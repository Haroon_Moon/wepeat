import 'package:wepeat/model/tags_model.dart';

class HomeModel {
  String? name;
  String? description;
  List<Tag>? homeTags = [];
  List<Member>? members = [];

  HomeModel({this.name, this.description});

  HomeModel.fromJson(dynamic json) {
    name = json['name'];
    description = json['description'];
    if (json['data'] != null) {
      // members = <Member>[];
      json['data'].forEach((v) {
        members!.add(Member.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['description'] = description;
    return data;
  }
}

class Member {
  int? id;
  String? name;
  String? birthdate;
  List<Tag>? tags = [];
  MediaData? media;

  Member({this.id, this.name, this.birthdate});

  Member.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    birthdate = json['birthdate'];
    media = json['image'] != null ? MediaData.fromJson(json['image']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['birthdate'] = birthdate;
    if (media != null) {
      data['image'] = media!.toJson();
    }
    return data;
  }
}

class MediaData {
  String? collectionName;
  String? fullUrl;
  Conversions? conversions;

  MediaData({this.collectionName, this.fullUrl, this.conversions});

  MediaData.fromJson(Map<String, dynamic> json) {
    collectionName = json['collection_name'];
    fullUrl = json['full_url'];
    conversions = json['conversions'] != null
        ? Conversions.fromJson(json['conversions'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['collection_name'] = collectionName;
    data['full_url'] = fullUrl;
    if (conversions != null) {
      data['conversions'] = conversions!.toJson();
    }
    return data;
  }
}

class Conversions {
  List<String>? miniature;

  Conversions({this.miniature});

  Conversions.fromJson(Map<String, dynamic> json) {
    miniature = json['miniature'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['miniature'] = miniature;
    return data;
  }
}
