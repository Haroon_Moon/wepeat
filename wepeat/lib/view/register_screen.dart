import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';

import '../res/images.dart';
import '../res/widgets.dart';
import '../viewmodel/register_viewmodel.dart';
import 'base.dart';

class RegisterScreen extends PageProvideNode<RegisterViewModel> {
  @override
  Widget buildContent(BuildContext context) {
    return _RegisterContentScreen(mProvider);
  }
}

/// View
class _RegisterContentScreen extends StatefulWidget {
  final RegisterViewModel provide;

  const _RegisterContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _RegisterContentState();
  }
}

class _RegisterContentState extends State<_RegisterContentScreen>
    with TickerProviderStateMixin<_RegisterContentScreen>
    implements Presenter {
  RegisterViewModel? mProvide;

  TextEditingController etName = TextEditingController();
  bool isErrorName = false;
  String? strErrorName = "";

  TextEditingController etEmail = TextEditingController();
  bool isErrorEmail = false;
  String? strErrorEmail = "";

  TextEditingController etPassword = TextEditingController();
  bool isErrorPassword = false;
  String? strErrorPassword = "";

  TextEditingController etRePassword = TextEditingController();
  bool isErrorRePassword = false;
  String? strErrorRePassword = "";

  bool isPasswordVisible = false;
  bool canRegister = true;

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    setListeners();
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    mProvide!.isApiResponseError.addListener(() {
      if (mProvide!.isApiResponseError.value) {
        // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
        setState(() {
          if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
            isErrorEmail = true;
            strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
          }
          if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
            isErrorRePassword = true;
            strErrorRePassword =
                mProvide!.loginErrorsResponse.errors!.password![0];
          }
        });
        mProvide!.isApiResponseError.value = false;
      }
    });
  }

  void checkValidations() {
    setState(() {
      if (etName.text.isNotEmpty && isErrorPassword) isErrorName = false;
      if (etEmail.text.isNotEmpty &&
          checkValidateEmail(etEmail.text.toString().trim()) &&
          isErrorEmail) isErrorEmail = false;
      if (etPassword.text.isNotEmpty && isErrorPassword) {
        isErrorPassword = false;
      }
      if (etRePassword.text.isNotEmpty && isErrorPassword) {
        isErrorRePassword = false;
      }

      if (etName.text.toString().trim().isEmpty) {
        isErrorName = true;
        strErrorName = string_error_name.tr;
      } else if (etEmail.text.toString().trim().isEmpty) {
        isErrorEmail = true;
        strErrorEmail = string_error_email.tr;
      } else if (!checkValidateEmail(etEmail.text.toString().trim())) {
        isErrorEmail = true;
        strErrorEmail = string_error_email_invalid.tr;
      } else if (etPassword.text.toString().trim().isEmpty) {
        isErrorPassword = true;
        strErrorPassword = string_error_password.tr;
      } else if (etRePassword.text.toString().trim().isEmpty) {
        isErrorRePassword = true;
        strErrorRePassword = string_error_password.tr;
      } else if (etRePassword.text.toString().trim() !=
          etPassword.text.toString().trim()) {
        isErrorRePassword = true;
        strErrorRePassword = string_error_password_not_match.tr;
      } else {
        mProvide!.register(
            etName.text.toString().trim(),
            etEmail.text.toString().trim(),
            etPassword.text.toString().trim(),
            etRePassword.text.toString().trim(),
            this);
      }
    });
  }

  @override
  void onClick(String? action) {
    print("onClick: " + action!);
    switch (action) {
      case ACTION_PASSWORD_VISIBILITY:
        setState(() {
          isPasswordVisible = !isPasswordVisible;
        });
        break;
      case ACTION_ON_GOOGLE:
        break;
      case ACTION_ON_FACEBOOK:
        break;
      case ACTION_ON_NAME_CHANGE:
        setState(() {
          isErrorName = false;
        });
        break;
      case ACTION_ON_EMAIL_CHANGE:
        setState(() {
          isErrorEmail = false;
        });
        break;
      case ACTION_ON_PASSWORD_CHANGE:
        setState(() {
          isErrorPassword = false;
          isErrorRePassword = false;
        });
        break;
      case ACTION_ON_REFRESH:
        setState(() {});
        break;
      case ACTION_LOG_IN:
        Navigator.pop(context);
        break;
      case ACTION_REGISTER:
        checkValidations();
        break;
      case ACTION_ON_SUCCESS:
        GeneralUtils.navigationPushReplacement(context, Routes.planWidget,
            routeName: Routes.plan);
        // Navigator.pushAndRemoveUntil(
        //   context,
        //   MaterialPageRoute(
        //     builder: (BuildContext context) => GlobalScreen(),
        //   ),
        //       (route) => false,
        // );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Register Screen --------");
    // spInit(context);

    return Scaffold(
        backgroundColor: color_primary,
        body: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Stack(
            children: [
              getWidgetBackgroundLayer(context),
              SingleChildScrollView(
                  child: Center(
                child: Column(
                  children: [
                    SizedBox(height: 150.sp),
                    widgetLogo(),
                    SizedBox(height: 70.sp),
                    getWidgetTextField(etName, string_your_name.tr,
                        string_enter_your_name.tr, isErrorName, strErrorName,
                        presenter: this,
                        action: ACTION_ON_NAME_CHANGE,
                        isMarginTop: false),
                    getWidgetTextField(etEmail, string_your_email.tr,
                        string_enter_email_acc.tr, isErrorEmail, strErrorEmail,
                        presenter: this, action: ACTION_ON_EMAIL_CHANGE),
                    getWidgetTextField(
                        etPassword,
                        string_your_password.tr,
                        string_enter_password.tr,
                        isErrorPassword,
                        strErrorPassword,
                        presenter: this,
                        action: ACTION_ON_PASSWORD_CHANGE,
                        isPasswordField: true,
                        passwordVisible: isPasswordVisible),
                    getWidgetTextField(
                        etRePassword,
                        string_your_password_again.tr,
                        string_enter_password_again.tr,
                        isErrorRePassword,
                        strErrorRePassword,
                        presenter: this,
                        action: ACTION_ON_PASSWORD_CHANGE,
                        isPasswordField: true,
                        passwordVisible: isPasswordVisible),
                    Container(
                      margin: EdgeInsets.only(
                          left: 60.sp, right: 60.sp, top: 60.sp, bottom: 26.sp),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                              child: Container(
                            height: 1.sp,
                            color: color_white,
                          )),
                          Padding(
                            padding: EdgeInsets.only(left: 20.sp, right: 20.sp),
                            child: Text(
                              string_also_register_with.tr,
                              style: Style_18_Reg_White,
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Expanded(
                              child: Container(
                            height: 1.sp,
                            color: color_white,
                          )),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            onClick(ACTION_ON_GOOGLE);
                          },
                          child: SvgPicture.asset(
                            image_google,
                            height: 80.sp,
                            width: 80.sp,
                          ),
                        ),
                        SizedBox(
                          width: 30.sp,
                        ),
                        InkWell(
                          onTap: () {
                            onClick(ACTION_ON_FACEBOOK);
                          },
                          child: SvgPicture.asset(
                            image_facebook,
                            height: 80.sp,
                            width: 80.sp,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 30.sp,
                    ),
                    getWidgetButton(
                        context,
                        string_create_account.tr,
                        ACTION_REGISTER,
                        this,
                        mProvide!.btnWidth,
                        mProvide!.loading,
                        isEnable: canRegister),
                    SizedBox(
                      height: 20.sp,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(string_already_have_account.tr,
                            style: Style_16_Reg_White),
                        SizedBox(width: 5.sp),
                        getWidgetFlatButton(
                          this,
                          ACTION_LOG_IN,
                          Text(string_connect_now.tr,
                              style: Style_16_Bold_White),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 40.sp,
                    ),
                  ],
                ),
              )),
              // getWidgetBackgroundLayer(context)
            ],
          ),
        ));
  }
}
