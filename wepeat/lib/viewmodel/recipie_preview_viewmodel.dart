import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:rxdart/transformers.dart';
import 'package:wepeat/model/calendar_items_model.dart';
import 'package:wepeat/model/recipies/food_model.dart';

import '../di/repository.dart';
import '../helper/constants.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../view/base.dart';

class RecipiePreviewViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();
  ItemData? recipie;

  RecipiePreviewViewModel(this._repo);
  bool get loading => _loading;
  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  Future<Stream?> getItemDetail(
      Presenter presenter, int id, int recipieId) async {
    if (await checkInternet()) {
      _repo.getCalenderItemDetail(id).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);
        recipie = ItemData.fromJson(r['data']);
        if (recipieId != -1) {
          getRecipieSteps(presenter, recipieId);
          getRecipieAllFoods(presenter, recipieId);
        }
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getRecipieSteps(Presenter presenter, int recipieId) async {
    if (await checkInternet()) {
      _repo.getRecipieSteps(recipieId).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);

        List<StepsModel> steps = <StepsModel>[];
        if (r['data'] != null) {
          r['data'].forEach((v) {
            steps.add(StepsModel.fromJson(v));
          });
        }

        recipie?.steps = steps;
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getRecipieAllFoods(Presenter presenter, int recipieId) async {
    if (await checkInternet()) {
      _repo.getRecipieFoods(recipieId).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) async {
        //success
        print(r);

        List<FoodModel> newFoods = <FoodModel>[];
        if (r['data'] != null) {
          r['data'].forEach((v) {
            newFoods.add(FoodModel.fromJson(v));
          });
        }

        recipie?.foods = newFoods;

        for (int i = 0; i < (recipie?.foods?.length ?? 0); i++) {
          await getFoodDetail(presenter, (recipie?.foods?[i].foodId ?? -1), i);
          await getMeasureDetail(
              presenter, (recipie?.foods?[i].measureId ?? -1), i);
        }
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getFoodDetail(
      Presenter presenter, int foodId, int index) async {
    if (await checkInternet()) {
      _repo.getFoodDetail(foodId).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);

        FoodData res = FoodData.fromJson(r);

        recipie?.foods?[index].foodData = res;

        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getMeasureDetail(
      Presenter presenter, int measureId, int index) async {
    if (await checkInternet()) {
      _repo.getMeasureDetail(measureId).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);

        MeasuresData res = MeasuresData.fromJson(r['data']);

        recipie?.foods?[index].measureData = res;

        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }
}
