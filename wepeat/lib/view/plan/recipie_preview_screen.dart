import 'package:flutter/foundation.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/widgets/bottom_nav_widget.dart';
import 'package:wepeat/widgets/network_image_widget.dart';

import '../../helper/constants.dart';
import '../../helper/device_utils.dart';
import '../../model/calendar_items_model.dart';
import '../../viewmodel/recipie_preview_viewmodel.dart';

class RecipiePreviewScreen extends PageProvideNode<RecipiePreviewViewModel> {
  int? itemId;
  int? recipieId;

  RecipiePreviewScreen({this.itemId, this.recipieId});

  @override
  Widget buildContent(BuildContext context) {
    return _RecipiePreviewContentScreen(mProvider, itemId, recipieId);
  }
}

/// View
class _RecipiePreviewContentScreen extends StatefulWidget {
  final RecipiePreviewViewModel provide;
  int? itemId;
  int? recipieId;

  _RecipiePreviewContentScreen(this.provide, this.itemId, this.recipieId);

  @override
  State<StatefulWidget> createState() {
    return _RecipiePreviewContentState();
  }
}

class _RecipiePreviewContentState extends State<_RecipiePreviewContentScreen>
    with TickerProviderStateMixin<_RecipiePreviewContentScreen>
    implements Presenter {
  RecipiePreviewViewModel? mProvide;
  double _animatedHeight = 500;

  @override
  void onClick(String? action) {
    // TODO: implement onClick
    print(action);
    switch (action) {
      case ACTION_ON_SUCCESS:
        if (mounted) setState(() {});
        break;
      case actionOpenLink:
        launchUrl(Uri.parse(mProvide!.recipie!.link!.url!));
        break;
    }
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    // mProvide!.isApiResponseError.addListener(() {
    //   if (mProvide!.isApiResponseError.value) {
    //     // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
    //     setState(() {
    //       if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
    //         isErrorEmail = true;
    //         strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
    //       }
    //       if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
    //         isErrorPassword = true;
    //         strErrorPassword =
    //             mProvide!.loginErrorsResponse.errors!.password![0];
    //       }
    //     });
    //     mProvide!.isApiResponseError.value = false;
    //   }
    // });
  }

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    setListeners();
    if (widget.itemId != null) {
      mProvide?.getItemDetail(this, widget.itemId!, widget.recipieId!);
    }
    // if (widget.recipieId != -1) {
    //   //Get Steps and ingredients here from api
    //   mProvide?.getRecipieSteps(this, widget.recipieId!);
    // }
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Recipie Preview Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: color_white,
      bottomNavigationBar: const BottomNavWidget(),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Stack(
          children: [
            (mProvide?.recipie == null) ? Container() : _buildHeaderView(),
            (mProvide?.recipie == null) ? Container() : _buildBody(),
            Consumer<RecipiePreviewViewModel>(builder: (context, value, child) {
              return getWidgetScreenLoader(context, value.loading);
            }),
          ],
        ),
      ),
    );
  }

  Widget _buildHeaderView() {
    return Stack(
      children: [
        NetworkImageWidget(
          url: mProvide?.recipie?.link?.url != null
              ? mProvide?.recipie?.link?.favIcon ?? noImgPH
              : mProvide?.recipie?.medias?.first.fullUrl ?? noImgPH,
          height: 660.sp,
          width: MediaQuery.of(context).size.width,
        ),
        Positioned(
          top: MediaQuery.of(context).padding.top + 5,
          right: 5,
          child: InkWell(
            onTap: () {
              GeneralUtils.navigationPop(context);
            },
            child: const CircleAvatar(
              foregroundColor: black03,
              child: Icon(
                Icons.close_rounded,
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildBody() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: GestureDetector(
        onVerticalDragStart: (DragStartDetails details) {
          if (kDebugMode) {
            print(details);
          }
          setState(() {
            if (details.globalPosition > Offset.zero) {
              _animatedHeight = _animatedHeight != 500.0 ? 500.0 : 750.0;
            }
          });
        },
        // onTap: () {
        //   setState(() {
        //     _animatedHeight = _animatedHeight != 500.0 ? 500.0 : 750.0;
        //   });
        // },
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          height: _animatedHeight,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(
            color: color_white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.all(40.sp),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    width: DeviceUtils.getScaledWidth(context, 40, 40),
                    height: DeviceUtils.getScaledHeight(context, 5, 5),
                    decoration: const BoxDecoration(
                      color: color_grey,
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                  ),
                ),
                spacerView(context, h: 24.sp),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // spacerView(context, h: 44.sp),
                        // Row(
                        //   children: [
                        //     NetworkImageWidget(
                        //       url: imgPH,
                        //       height: 64.sp,
                        //       width: 64.sp,
                        //       shape: BoxShape.circle,
                        //     ),
                        //     spacerView(context, w: 16.sp),
                        //     Text(
                        //       "@Alimentamente1",
                        //       style: Style_14_Reg_Black,
                        //     ),
                        //   ],
                        // ),

                        (mProvide?.recipie?.link?.url != null)
                            ? InkWell(
                                onTap: () {
                                  launchUrl(
                                      Uri.parse(mProvide!.recipie!.link!.url!));
                                },
                                child: Text(
                                  mProvide!.recipie!.link!.url!,
                                  style: Style_32_Bold_Black03,
                                ),
                              )
                            : Text(
                                mProvide?.recipie?.title ?? "",
                                style: Style_32_Bold_Black03,
                              ),
                        spacerView(context, h: 24.sp),
                        Row(
                          children: [
                            if (mProvide?.recipie?.defaultMinutes != null &&
                                mProvide?.recipie?.defaultMinutes != 0)
                              const Icon(
                                Icons.timer_outlined,
                                color: color_primary,
                              ),
                            if (mProvide?.recipie?.defaultMinutes != null &&
                                mProvide?.recipie?.defaultMinutes != 0)
                              Text(
                                (mProvide?.recipie?.defaultMinutes.toString() ??
                                        "") +
                                    stringMinutes.tr,
                              ),
                            spacerView(context, w: 32.sp),
                            if (mProvide?.recipie?.defaultPortions != null &&
                                mProvide?.recipie?.defaultPortions != 0)
                              const Icon(
                                Icons.person,
                                color: color_primary,
                              ),
                            if (mProvide?.recipie?.defaultPortions != null &&
                                mProvide?.recipie?.defaultPortions != 0)
                              Text(
                                (mProvide?.recipie?.defaultPortions
                                            .toString() ??
                                        "") +
                                    stringPortions.tr,
                              ),
                          ],
                        ),
                        spacerView(context, h: 24.sp),
                        const Divider(),
                        spacerView(context, h: 24.sp),
                        // if (mProvide?.recipie?.content?.isNotEmpty ?? false)
                        Text(
                          stringDescription.tr,
                          style: Style_24_Bold_Black03,
                        ),
                        spacerView(context, h: 24.sp),
                        // if (mProvide?.recipie?.content?.isNotEmpty ?? false)
                        Text(
                          mProvide?.recipie?.content ?? "N/A",
                          style: Style_18_Reg_Black03,
                        ),
                        spacerView(context, h: 24.sp),
                        if (mProvide?.recipie?.link?.url != null)
                          getWidgetNonButton(
                            context,
                            btnLblLinkedPage.tr,
                            actionOpenLink,
                            this,
                            isFilled: true,
                            height:
                                DeviceUtils.getScaledHeight(context, 50, 50),
                            radius: BorderRadius.circular(30),
                          ),
                        if (mProvide?.recipie?.foods != null &&
                            mProvide!.recipie!.foods!.isNotEmpty)
                          _buildFoodsView(),

                        if (mProvide?.recipie?.steps != null &&
                            mProvide!.recipie!.steps!.isNotEmpty)
                          _buildStepsView(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildFoodsView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        spacerView(context, h: 24.sp),
        Text(
          stringIngredientes.tr,
          style: Style_24_Bold_Black03,
        ),
        spacerView(context, h: 24.sp),
        ListView.separated(
          shrinkWrap: true,
          padding: EdgeInsets.zero,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  child: Text(
                    mProvide?.recipie?.foods?[index].foodData?.data?.name ?? "",
                    textAlign: TextAlign.left,
                    style: Style_18_Reg_Black03,
                  ),
                ),
                if (mProvide?.recipie?.foods?[index].measureData?.name !=
                        null &&
                    mProvide?.recipie?.foods?[index].measureData?.name != "")
                  Text(
                    (mProvide?.recipie?.foods?[index].quantity ?? "") +
                        " " +
                        (mProvide?.recipie?.foods?[index].measureData?.name ??
                            ""),
                    textAlign: TextAlign.left,
                    style: Style_18_Reg_Black03,
                  ),
              ],
            );
          },
          separatorBuilder: (context, index) {
            return spacerView(context, h: 20.h);
          },
          itemCount: mProvide?.recipie?.foods?.length ?? 0,
        )
      ],
    );
  }

  Widget _buildStepsView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        spacerView(context, h: 24.sp),
        Text(
          stringRecipieSteps.tr,
          style: Style_24_Bold_Black03,
        ),
        spacerView(context, h: 24.sp),
        ListView.separated(
          shrinkWrap: true,
          padding: EdgeInsets.zero,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(20.sp),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: color_primary, width: 2),
                  ),
                  child: Text(
                    "${index + 1}",
                    style: Style_18_Bold_Primary,
                  ),
                ),
                spacerView(context, w: 20.sp),
                Flexible(
                  child:
                      //  Html(
                      //   data: mProvide?.recipie?.steps?[index].content ?? "",
                      // ),
                      Text(
                    mProvide?.recipie?.steps?[index].content ?? "",
                    textAlign: TextAlign.left,
                    style: Style_18_Reg_Black03,
                  ),
                )
              ],
            );
          },
          separatorBuilder: (context, index) {
            return spacerView(context, h: 20.h);
          },
          itemCount: mProvide?.recipie?.steps?.length ?? 0,
        )
      ],
    );
  }
}
