import 'dart:async';

import 'package:wepeat/helper/net_utils.dart';
import 'package:dartin/dartin.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

/// normal click event
abstract class Presenter {
  void onClick(String? action);
}

abstract class PresenterWithValue {
  void onClickWithValue(String action, int? value);
}

abstract class PresenterWithValueAndMsg {
  void onClickWithValueAndMsg(String action, int? value, String? message);
}

abstract class PresenterWithContext {
  void onClickWithContext(String action, BuildContext context);
}

abstract class PresenterWithValueContext {
  void onClickWithValueContext(String action, int value, BuildContext context);
}

/// BaseViewModel
class BaseViewModel with ChangeNotifier {
  ValueNotifier<bool> isInternetError = ValueNotifier(false);
  ValueNotifier<bool> isApiResponseError = ValueNotifier(false);
  String errorMsg = "";

  Future<bool> checkInternet() async {
    bool connection = await isInternetConnected();
    isInternetError.value = !connection;
    isInternetError.notifyListeners();
    return connection;
  }

  void onApiResponseError() {
    isApiResponseError.value = true;
    isApiResponseError.notifyListeners();
  }

  CompositeSubscription compositeSubscription = CompositeSubscription();

  addSubscription(StreamSubscription subscription) {
    compositeSubscription.add(subscription);
  }

  @override
  void dispose() {
    if (!compositeSubscription.isDisposed) {
      compositeSubscription.dispose();
    }
    super.dispose();
  }
}

abstract class PageProvideNode<T extends ChangeNotifier> extends StatelessWidget
    implements Presenter {
  final T mProvider;

  PageProvideNode({List<dynamic>? params})
      : mProvider = inject<T>(params: params);

  Widget buildContent(BuildContext context);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      // BoxConstraints(
      //     maxWidth: MediaQuery.of(context).size.width,
      //     maxHeight: MediaQuery.of(context).size.height),
      designSize: const Size(750, 1334),
      minTextAdapt: true,
      // orientation: Orientation.portrait,
    );
    // ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: false); // Iphone 6
    // ScreenUtil.init(context, width: 375, height: 812, allowFontScaling: false); // Iphone x
    // ScreenUtil.init(context, width: 1242, height: 2688, allowFontScaling: false); // Iphone 11 max
    // ScreenUtil.init(context, width: 360, height: 740, allowFontScaling: false);
    return ChangeNotifierProvider<T>.value(
      value: mProvider,
      child: buildContent(context),
    );
  }

  @override
  void onClick(String? action) {}
}
