import 'dart:async';

import 'package:get/get.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:wepeat/helper/widgets/fade_route_builder.dart';
import 'package:wepeat/helper/widgets/nested_navigator.dart';
import 'package:wepeat/model/user_profile.dart';
import 'package:wepeat/res/fonts.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/viewmodel/global_viewmodel.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import '../../main.dart';
import '../../res/widgets.dart';
import '../base.dart';

class GlobalScreen extends PageProvideNode<GlobalViewModel> {
  GlobalScreen() : super();

  @override
  Widget buildContent(BuildContext context) {
    return _GlobalContentScreen(mProvider);
  }
}

/// View
class _GlobalContentScreen extends StatefulWidget {
  final GlobalViewModel provide;

  const _GlobalContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _GlobalContentState();
  }
}

class _GlobalContentState extends State<_GlobalContentScreen>
    with TickerProviderStateMixin<_GlobalContentScreen>
    implements Presenter, PresenterWithValue {
  late GlobalViewModel mProvide;

  bool isOpenDrawer = false;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool isBackBtnEnable = false;
  int screenPageCount = 0;
  bool firstLaunch = true;
  String toolBarTitle = "";
  int bottomNavigationIndex = 0;
  bool showToolBar = true;
  bool showBotomBar = true;

  late List<String> homeMenuList = [
    "Home",
    "Create Post",
    "My Profile",
    "Logout"
  ];

  final GlobalKey<NavigatorState> _navigationStackKey =
      GlobalKey<NavigatorState>();

  final Map _source = {ConnectivityResult.wifi: true};
  String strNoInternet = "";
  static GlobalKey previewContainer = GlobalKey();

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    contextGlobal = context;
    setListeners();

    Timer(const Duration(seconds: 1), () {
      showCreatePostPickerSheet(context);
    });
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  @override
  void onClick(String? action) {
    print("onClick: " + action!);
    switch (action) {
      // case ACTION_TOOLBAR_LOGO:
      //   setState(() {
      //     rootParentIndex = -1;
      //     _selectedIndex = 0;
      //     _navigationStackKey.currentState!
      //         .pushNamedAndRemoveUntil("", (route) => false);
      //   });
      //   break;
      case ACTION_BACK:
        _navigationStackKey.currentState!.pop();
        _onBackPress();
        screenPageCount--;

        setState(() {
          if (bottomNavigationIndex != 1) toolBarTitle = "";

          isBackBtnEnable = false;
          showBotomBar = true;
          showToolBar = true;
        });

        break;
      case ACTION_MENU:
        if (isOpenDrawer) {
          Navigator.pop(context);
        } else {
          scaffoldKey.currentState!.openDrawer();
        }
        break;
      case ACTION_MY_HOME:
        setState(() => bottomNavigationIndex = 0);
        _navigationStackKey.currentState!
            .pushNamedAndRemoveUntil("", (route) => false);
        break;
      case ACTION_MY_PLAN:
        setState(() => bottomNavigationIndex = 1);
        _navigationStackKey.currentState!
            .pushNamedAndRemoveUntil("", (route) => false);
        break;
      case ACTION_MY_PROFILE:
        setState(() => bottomNavigationIndex = 2);
        _navigationStackKey.currentState!
            .pushNamedAndRemoveUntil("", (route) => false);
        break;
      case 'Home':
        onSideMenuViewsCallingHandler();
        setState(() => bottomNavigationIndex = 0);
        _navigationStackKey.currentState!
            .pushNamedAndRemoveUntil("", (route) => false);
        break;
      case 'My Profile':
        // onSideMenuViewsCallingHandler();
        // _navigationStackKey.currentState!.pushNamed(
        //   HomeScreen.routeViewer,
        //   arguments: _HomeViewerArguments(spUtil.user),
        // );
        break;
      case 'Logout':
        // showAlert(context, this,
        //     positiveText: string_confirm,
        //     positiveAction: ACTION_ON_LOGOUT,
        //     title: string_logout_msg);
        break;
      case ACTION_ON_LOGOUT:
        onSideMenuViewsCallingHandler();
        logoutUser(context);
        break;
      case ACTION_EDIT_HOME:
        setState(() => bottomNavigationIndex = 0);
        _navigationStackKey.currentState!
            .pushNamedAndRemoveUntil("", (route) => false);
        Navigator.of(context).pop();
        break;
      case ACTION_MAYBE_LATER:
        setState(() => bottomNavigationIndex = 1);
        _navigationStackKey.currentState!
            .pushNamedAndRemoveUntil("", (route) => false);
        Navigator.of(context).pop();
        break;
    }
  }

  void onSideMenuViewsCallingHandler() {
    if (isOpenDrawer) {
      Navigator.pop(context);
    }
  }

  @override
  void onClickWithValue(String action, int? value) {
    switch (action) {
      case ACTION_IS_SHOW_BOTTOM_NAV:
        setState(() {
          // showBottomNav = false;
        });
        break;
    }
  }

  void setListeners() {
    mProvide.isInternetError.addListener(() {
      if (mProvide.isInternetError.value) {
        showInternetAlert(context);
        mProvide.isInternetError.value = false;
      }
    });

    mProvide.isApiResponseError.addListener(() {
      if (mProvide.isApiResponseError.value) {
        onErrorHandler(context, 422, "Error");
        mProvide.isApiResponseError.value = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Global Screen --------");

    switch (_source.keys.toList()[0]) {
      case ConnectivityResult.none:
        setState(() {
          strNoInternet = "No Internet Connection";
        });
        break;
      case ConnectivityResult.mobile:
        setState(() {
          strNoInternet = "";
        });
        break;
      case ConnectivityResult.wifi:
        setState(() {
          strNoInternet = "";
        });
    }

    return Scaffold(
        resizeToAvoidBottomInset: false,
        extendBody: true,
        // appBar: (showToolBar)? getWidgetAppBar(context, this, bottomNavigationIndex,
        //     isOpenDrawer: isOpenDrawer,
        //     isBackBtnEnable: isBackBtnEnable,
        //     title: toolBarTitle) as PreferredSizeWidget? : null,
        body: Scaffold(
          key: scaffoldKey,
          onDrawerChanged: (isOpen) {
            print("Drawer Open: $isOpen");
            setState(() {
              isOpenDrawer = isOpen;
            });
          },
          drawerEnableOpenDragGesture: false,
          backgroundColor: color_background,
          // body: _getNavItemWidget(_selectedIndex),
          body: Stack(
            children: [
              Column(
                children: [
                  Visibility(
                    visible: strNoInternet.isNotEmpty,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      color: Colors.red,
                      padding: EdgeInsets.all(15.sp),
                      child: Center(
                        child: Text(
                          strNoInternet,
                          style: Style_20_Reg_White,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: NestedNavigator(
                      //Need to assign a key to our navigator, so we can pop/push it later
                      navKey: _navigationStackKey,
                      routeBuilder: _buildPageRoute,
                      //When nested navigator has been popped, reverse the star anim back to start
                      onBackPop: _onBackPress,
                    ),
                  )
                ],
              ),
            ],
          ),

          bottomNavigationBar: (showBotomBar)
              ? BottomNavigationBar(
                  type: BottomNavigationBarType.fixed,
                  currentIndex: bottomNavigationIndex,
                  backgroundColor: color_white,
                  selectedItemColor: color_black,
                  unselectedItemColor: color_placeholder,
                  selectedFontSize: font_14,
                  unselectedFontSize: font_14,
                  selectedLabelStyle: Style_14_Bold_Black,
                  unselectedLabelStyle: Style_14_SemiBold_Grey,
                  onTap: (value) {
                    // Respond to item press.
                    print("Value: " + value.toString());
                    setState(() => bottomNavigationIndex = value);
                    switch (bottomNavigationIndex) {
                      case 0:
                        onClick(ACTION_MY_HOME);
                        break;
                      case 1:
                        onClick(ACTION_MY_PLAN);
                        break;
                      case 2:
                        onClick(ACTION_MY_PROFILE);
                        break;
                    }
                  },
                  items: [
                    BottomNavigationBarItem(
                      label: string_my_home.tr,
                      icon: Padding(
                        padding: EdgeInsets.only(bottom: 10.sp),
                        child: SvgPicture.asset(
                          image_home,
                          height: 60.sp,
                          width: 60.sp,
                          color: (bottomNavigationIndex == 0)
                              ? color_primary
                              : color_black,
                        ),
                      ),
                    ),
                    BottomNavigationBarItem(
                      label: string_my_plan.tr,
                      icon: Padding(
                        padding: EdgeInsets.only(bottom: 10.sp),
                        child: SvgPicture.asset(
                          image_plan,
                          height: 60.sp,
                          width: 60.sp,
                          color: (bottomNavigationIndex == 1)
                              ? color_primary
                              : color_black,
                        ),
                      ),
                    ),
                    BottomNavigationBarItem(
                        label: string_you.tr,
                        icon: Padding(
                          padding: EdgeInsets.only(bottom: 10.sp),
                          child: SvgPicture.asset(
                            image_user,
                            height: 60.sp,
                            width: 60.sp,
                            color: (bottomNavigationIndex == 2)
                                ? color_primary
                                : color_black,
                          ),
                        )),
                  ],
                )
              : null,
        ));
  }

  Route _buildPageRoute(RouteSettings route) {
    Widget? page;
    screenPageCount++;
    print("ScreenPageCount " + screenPageCount.toString());

    if (firstLaunch) {
      Timer.periodic(const Duration(seconds: 1), (time) {
        firstLaunch = false;
      });
    }

    switch (bottomNavigationIndex) {
      case 0:
        switch (route.name) {
          default:
            screenPageCount = 0;
            page = Center(
              child: Text(
                "Milestone 2\nWork Inprogress",
                style: Style_20_Bold_Black,
                textAlign: TextAlign.center,
              ),
            );
            // var args = route.arguments as _RouteArguments;
            // page = HomeScreen(null, (userProfile) {
            //   _navigationStackKey.currentState!.pushNamed(
            //     HomeScreen.routeViewer,
            //     arguments: _HomeViewerArguments(userProfile),
            //   );
            // });
            if (!firstLaunch) {
              setState(() {
                isBackBtnEnable = false;
                toolBarTitle = "";
              });
            }
        }
        break;
      case 1:
        switch (route.name) {
          default:
            screenPageCount = 0;
            page = Center(
              child: Text(
                "Milestone 3\nWork Inprogress",
                style: Style_20_Bold_Black,
                textAlign: TextAlign.center,
              ),
            );
            setState(() {
              toolBarTitle = "";
            });
        }
        break;
      case 2:
        switch (route.name) {
          default:
            screenPageCount = 0;
            page = Center(
              child: Text(
                "Milestone 4\nWork Inprogress",
                style: Style_20_Bold_Black,
                textAlign: TextAlign.center,
              ),
            );
            setState(() {
              toolBarTitle = "";
            });
        }
        break;
    }

    //Use a FadeRouteBuilder which fades the new view in, while fading the old page out. Necessary as the content pages have transparent backgrounds.
    return FadeRouteBuilder(page: page);
  }

  void _onBackPress() {
    // print("WillPopScope 2");
  }

  void showCreatePostPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Container(
              padding: EdgeInsets.all(40.sp),
              decoration: BoxDecoration(
                color: color_white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40.sp),
                    topRight: Radius.circular(40.sp)),
              ),
              child: Wrap(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 80.sp,
                        height: 10.sp,
                        decoration: BoxDecoration(
                          color: color_grey,
                          borderRadius: BorderRadius.all(Radius.circular(5.sp)),
                        ),
                      ),
                      SizedBox(
                        height: 30.sp,
                      ),
                      Text(
                        string_edit_home_info.tr,
                        style: Style_24_Bold_Dark_Blue,
                        textAlign: TextAlign.center,
                      ),
                      Container(
                        margin: EdgeInsets.all(15.sp),
                        child: Wrap(
                          children: const <Widget>[],
                        ),
                      ),
                      SizedBox(
                        height: 30.sp,
                      ),
                      SvgPicture.asset(
                        image_edit_home_info,
                        width: 380.sp,
                        height: 160,
                      ),
                      SizedBox(
                        height: 50.sp,
                      ),
                      RichText(
                        text: TextSpan(children: <TextSpan>[
                          TextSpan(
                            text:
                                "For a better experience on Wepeat, we recommend that you ",
                            style: Style_18_Reg_Black,
                          ),
                          TextSpan(
                              text: "edit your household data. ",
                              style: Style_18_Bold_Black),
                          TextSpan(text: "You can ", style: Style_18_Reg_Black),
                          TextSpan(
                              text: "configure the members ",
                              style: Style_18_Bold_Black),
                          TextSpan(
                              text: "that live with you, the ",
                              style: Style_18_Reg_Black),
                          TextSpan(
                              text: "allergies ", style: Style_18_Bold_Black),
                          TextSpan(
                              text: "of each one, their ",
                              style: Style_18_Reg_Black),
                          TextSpan(text: "tastes ", style: Style_18_Bold_Black),
                          TextSpan(text: ", the ", style: Style_18_Reg_Black),
                          TextSpan(
                              text: "equipment ", style: Style_18_Bold_Black),
                          TextSpan(
                              text: "you have and many other things.",
                              style: Style_18_Reg_Black),
                        ]),
                      ),
                      SizedBox(
                        height: 60.sp,
                      ),
                      getWidgetNonButton(context, string_edit_home_now.tr,
                          ACTION_EDIT_HOME, this),
                      SizedBox(
                        height: 20.sp,
                      ),
                      getWidgetNonButton(context, string_maybe_later.tr,
                          ACTION_MAYBE_LATER, this,
                          isFilled: false),
                      SizedBox(
                        height: 40.sp,
                      ),
                    ],
                  )
                ],
              ));
        });
  }
}

class _RouteArguments {
  int id = -1;

  _RouteArguments(this.id);
}

class _HomeViewerArguments {
  UserProfile? userProfile;

  _HomeViewerArguments(this.userProfile);
}
