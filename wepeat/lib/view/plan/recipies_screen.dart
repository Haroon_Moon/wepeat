import 'dart:async';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/viewmodel/recipies_viewmodel.dart';
import 'package:wepeat/widgets/network_image_widget.dart';
import '../../helper/device_utils.dart';

class RecipieScreen extends PageProvideNode<RecipiesViewModel> {
  String? date;
  int? mealTypeId;
  RecipieScreen({this.date, this.mealTypeId});

  @override
  Widget buildContent(BuildContext context) {
    return _RecipieContentScreen(mProvider, date, mealTypeId);
  }
}

/// View
class _RecipieContentScreen extends StatefulWidget {
  final RecipiesViewModel provide;
  String? date;
  int? mealTypeId;

  _RecipieContentScreen(this.provide, this.date, this.mealTypeId);

  @override
  State<StatefulWidget> createState() {
    return _RecipieContentState();
  }
}

class _RecipieContentState extends State<_RecipieContentScreen>
    with TickerProviderStateMixin<_RecipieContentScreen>
    implements Presenter {
  RecipiesViewModel? mProvide;
  TextEditingController etSearch = TextEditingController();
  TextEditingController etPortions = TextEditingController();
  List<bool> portions = List<bool>.generate(6, (i) => false);
  Timer? timer;

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;

    mProvide?.getRecipes(this, etSearch.text.trim());
    mProvide?.getTagsType(this);
    // etSearch.addListener(() {
    //   _startSearchTimer();
    // });
    setListeners();
  }

  void _startSearchTimer() {
    if (timer != null && timer!.isActive) timer?.cancel();
    timer = Timer.periodic(const Duration(seconds: 1), (time) {
      mProvide?.getRecipes(this, etSearch.text.trim());
      time.cancel();
    });
  }

  @override
  void dispose() {
    print('-------dispose-------');
    if (timer != null) timer?.cancel();
    etPortions.dispose();
    etSearch.dispose();
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    // mProvide!.isApiResponseError.addListener(() {
    //   if (mProvide!.isApiResponseError.value) {
    //     // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
    //     setState(() {
    //       if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
    //         isErrorEmail = true;
    //         strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
    //       }
    //       if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
    //         isErrorPassword = true;
    //         strErrorPassword =
    //             mProvide!.loginErrorsResponse.errors!.password![0];
    //       }
    //     });
    //     mProvide!.isApiResponseError.value = false;
    //   }
    // });
  }

  @override
  void onClick(String? action) {
    print("onClick: " + action!);
    switch (action) {
      case ACTION_ON_CHANGE:
        _startSearchTimer();
        break;
      case actionLoading:
        setState(() {
          mProvide?.loading = true;
        });
        break;
      case actionStopLoading:
        setState(() {
          mProvide?.loading = false;
        });
        break;
      case actionSuccess:
        setState(() {});
        break;
      case actionAddRecipiePlan:
        GeneralUtils.navigationPop(context);
        int? recipieId = mProvide?.recipes?.recipes
            ?.firstWhere((element) => element.isSelected == true)
            .id;
        mProvide?.addRecipie(this, recipieId!, widget.date!, widget.mealTypeId!,
            etPortions.text.trim());
        break;
      case actionSuccessAddRecipie:
        GeneralUtils.navigationPushReplacement(context, Routes.planWidget,
            routeName: Routes.plan);
        break;
      case ACTION_MAYBE_LATER:
        GeneralUtils.navigationPop(context);
        break;
      case actionApplyFilters:
        GeneralUtils.navigationPop(context);
        mProvide?.getRecipes(this, etSearch.text.trim());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Recipies Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: transparent,
      body: InkWell(
        onTap: () {
          GeneralUtils.navigationPop(context);
        },
        child: Container(
          color: color_grey.withOpacity(0.7),
          child: Container(
            margin: EdgeInsets.only(top: 200.sp),
            child: Stack(
              children: [
                GestureDetector(
                  onTap: () {
                    FocusScopeNode currentFocus = FocusScope.of(context);
                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                  },
                  onPanUpdate: (details) {
                    print(details.delta.dy);
                    if (details.delta.dy > 0.0) {
                      GeneralUtils.navigationPop(context);
                    }
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: color_grey),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(54.sp),
                            topRight: Radius.circular(54.sp))),
                    child: Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.all(40.sp),
                          child: NotificationListener<ScrollNotification>(
                            onNotification: (ScrollNotification scrollInfo) {
                              if (scrollInfo is ScrollEndNotification &&
                                  scrollInfo.metrics.extentAfter == 0 &&
                                  scrollInfo.metrics.axis == Axis.vertical &&
                                  !(mProvide!.loading)) {
                                print("Scroll End");
                                if (mProvide?.recipes?.links?.next != null) {
                                  mProvide?.getRecipes(
                                      this, etSearch.text.trim(),
                                      nextUrl: mProvide?.recipes?.links?.next);
                                }
                                return true;
                              }
                              return false;
                            },
                            child: SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      width: DeviceUtils.getScaledWidth(
                                          context, 40, 40),
                                      height: DeviceUtils.getScaledHeight(
                                          context, 5, 5),
                                      decoration: const BoxDecoration(
                                        color: color_grey,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(4)),
                                      ),
                                    ),
                                  ),
                                  spacerView(context, h: 40.sp),
                                  Stack(
                                    children: [
                                      getTextField(
                                          etSearch, "", stringSearch.tr,
                                          presenter: this,
                                          action: ACTION_ON_CHANGE),
                                      Positioned(
                                        top: 10,
                                        right: 10,
                                        child: Row(
                                          children: [
                                            InkWell(
                                              onTap: () {
                                                mProvide
                                                    ?.recipieTagsType?.tagtypes
                                                    ?.forEach((type) {
                                                  type.tagsData?.data
                                                      ?.forEach((element) {
                                                    if (element.isSelected) {
                                                      element.isSelected =
                                                          false;
                                                    }
                                                  });
                                                });
                                                mProvide?.getRecipes(
                                                    this, etSearch.text.trim());
                                              },
                                              child: Icon(
                                                Icons.close,
                                                color: color_primary,
                                              ),
                                            ),
                                            spacerView(context, w: 20.w),
                                            InkWell(
                                              onTap: () {
                                                showFilterPickerSheet(context);
                                              },
                                              child: Container(
                                                padding: EdgeInsets.all(20.sp),
                                                decoration: BoxDecoration(
                                                    color: greyC2,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                40.sp))),
                                                child: Center(
                                                  child: Text(
                                                    stringFilters.tr,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  spacerView(context, h: 64.sp),
                                  Text(
                                    mProvide?.recipes?.recipes?.length == 0
                                        ? stringNoMeals.tr
                                        : stringAddRecipieHeading.tr,
                                    style: Style_24_Bold_Black03,
                                  ),
                                  // spacerView(context, h: 12.sp),
                                  GridView.builder(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount:
                                        mProvide?.recipes?.recipes?.length ?? 0,
                                    gridDelegate:
                                        const SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      crossAxisSpacing: 16.0,
                                      mainAxisSpacing: 24.0,
                                      mainAxisExtent: 300,
                                    ),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return InkWell(
                                        onTap: () {
                                          setState(() {
                                            mProvide?.recipes?.recipes?[index]
                                                .isSelected = !(mProvide
                                                    ?.recipes
                                                    ?.recipes?[index]
                                                    .isSelected ??
                                                false);
                                            mProvide?.recipes?.recipes
                                                ?.forEach((element) {
                                              if (mProvide?.recipes
                                                      ?.recipes?[index].id !=
                                                  element.id)
                                                element.isSelected = false;
                                            });
                                          });
                                        },
                                        child: Column(
                                          children: [
                                            Stack(
                                              children: [
                                                NetworkImageWidget(
                                                  height: 332.sp,
                                                  width: 364.sp,
                                                  url: (mProvide
                                                              ?.recipes
                                                              ?.recipes?[index]
                                                              .medias !=
                                                          null)
                                                      ? (mProvide
                                                              ?.recipes
                                                              ?.recipes?[index]
                                                              .medias
                                                              ?.first
                                                              .fullUrl ??
                                                          imgPH)
                                                      : imgPH,
                                                  radius: BorderRadius.all(
                                                    Radius.circular(28.sp),
                                                  ),
                                                ),
                                                if (mProvide
                                                        ?.recipes
                                                        ?.recipes?[index]
                                                        .isSelected ??
                                                    false)
                                                  Positioned(
                                                    top: 10,
                                                    right: 10,
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(22.sp),
                                                      decoration:
                                                          const BoxDecoration(
                                                              color:
                                                                  color_primary,
                                                              shape: BoxShape
                                                                  .circle),
                                                      child: const Icon(
                                                        Icons.check,
                                                        color: color_white,
                                                      ),
                                                    ),
                                                  ),
                                              ],
                                            ),
                                            spacerView(context, h: 24.sp),
                                            Row(
                                              children: [
                                                NetworkImageWidget(
                                                  url: (mProvide
                                                              ?.recipes
                                                              ?.recipes?[index]
                                                              .userProfile
                                                              ?.iconUrl !=
                                                          null)
                                                      ? (mProvide
                                                              ?.recipes
                                                              ?.recipes?[index]
                                                              .userProfile
                                                              ?.iconUrl ??
                                                          imgPH)
                                                      : imgPH,
                                                  height: 64.sp,
                                                  width: 64.sp,
                                                  shape: BoxShape.circle,
                                                ),
                                                spacerView(context, w: 16.sp),
                                                Flexible(
                                                  child: Text(
                                                    (mProvide
                                                            ?.recipes
                                                            ?.recipes?[index]
                                                            .userProfile
                                                            ?.name ??
                                                        ""),
                                                    style: Style_14_Reg_Black,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            spacerView(context, h: 16.sp),
                                            Flexible(
                                              child: Text(
                                                (mProvide
                                                        ?.recipes
                                                        ?.recipes?[index]
                                                        .title ??
                                                    ""),
                                                style: Style_18_Bold_Black,
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        if (mProvide?.recipes?.recipes?.any(
                                (element) => element.isSelected == true) ??
                            false)
                          Positioned(
                            bottom: 40,
                            left: 0,
                            right: 0,
                            child: InkWell(
                              onTap: () {
                                print("Tapped Go");
                                optionsPickerSheet(context);
                              },
                              child: Container(
                                margin:
                                    EdgeInsets.only(left: 96.sp, right: 96.sp),
                                padding:
                                    EdgeInsets.only(top: 32.sp, bottom: 32.sp),
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  color: color_primary,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(100.sp)),
                                ),
                                child: Center(
                                  child: Text(
                                    stringGoSelectOptions.tr,
                                    style: Style_18_Bold_White,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        Consumer<RecipiesViewModel>(
                            builder: (context, value, child) {
                          return getWidgetScreenLoader(context, value.loading);
                        }),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void optionsPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
                padding: EdgeInsets.all(40.sp) +
                    EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                decoration: const BoxDecoration(
                  color: color_white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            width: DeviceUtils.getScaledWidth(context, 40, 40),
                            height: DeviceUtils.getScaledHeight(context, 5, 5),
                            decoration: const BoxDecoration(
                              color: color_grey,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                          ),
                        ),
                        spacerView(context, h: 60.h),
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            stringMealDealHeading.tr,
                            style: Style_24_Bold_Black03,
                          ),
                        ),
                        spacerView(context, h: 32.h),
                        Text(
                          stringHowManyPeople.tr,
                          style: Style_18_Bold_Black,
                        ),
                        spacerView(context, h: 12.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[0] = true;
                                  etPortions.text = "1";
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color:
                                      portions[0] ? color_primary : color_white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: portions[0] ? transparent : greyC2,
                                  ),
                                ),
                                child: Text(
                                  "1",
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[1] = true;
                                  etPortions.text = "2";
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color:
                                      portions[1] ? color_primary : color_white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: portions[1] ? transparent : greyC2,
                                  ),
                                ),
                                child: Text(
                                  "2",
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[2] = true;
                                  etPortions.text = "3";
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color:
                                      portions[2] ? color_primary : color_white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: portions[2] ? transparent : greyC2,
                                  ),
                                ),
                                child: Text(
                                  "3",
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[3] = true;
                                  etPortions.text = "4";
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color:
                                      portions[3] ? color_primary : color_white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: portions[3] ? transparent : greyC2,
                                  ),
                                ),
                                child: Text(
                                  "4",
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[4] = true;
                                  etPortions.text = "5";
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color:
                                      portions[4] ? color_primary : color_white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: portions[4] ? transparent : greyC2,
                                  ),
                                ),
                                child: Text(
                                  "5",
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[5] = true;
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color: color_primary.withOpacity(0.4),
                                  shape: BoxShape.circle,
                                ),
                                child: Text(
                                  "+",
                                ),
                              ),
                            ),
                          ],
                        ),
                        spacerView(context, h: 12.h),
                        if (portions[5])
                          getTextField(etPortions, "", stringEnterPortions.tr,
                              presenter: this,
                              action: ACTION_ON_NAME_CHANGE,
                              isMarginTop: false,
                              keyboardType: TextInputType.number),
                        spacerView(context, h: 65.h),
                        Row(
                          children: [
                            getButtonPadding(
                              context,
                              btnLblCancel.tr,
                              actionCancel,
                              this,
                              color: greyF3,
                              textStyle: Style_14_Bold_Black,
                              padding: EdgeInsets.only(
                                  top: 32.sp,
                                  bottom: 32.sp,
                                  left: 48.sp,
                                  right: 48.sp),
                            ),
                            spacerView(context, w: 20.sp),
                            Expanded(
                              child: getButtonPadding(
                                context,
                                stringAddRecipie.tr,
                                actionAddRecipiePlan,
                                this,
                                textStyle: Style_14_Bold_WHITE,
                                padding: EdgeInsets.only(
                                  top: 32.sp,
                                  bottom: 32.sp,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ));
          });
        }).whenComplete(() {
      setState(() {});
    });
  }

  void showFilterPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
                padding:
                    EdgeInsets.all(DeviceUtils.getScaledX(context, 20, 20)),
                height: MediaQuery.of(context).size.height - 200.sp,
                decoration: BoxDecoration(
                  color: color_white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40.sp),
                      topRight: Radius.circular(40.sp)),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          width: 80.sp,
                          height: 10.sp,
                          decoration: BoxDecoration(
                            color: color_grey,
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.sp)),
                          ),
                        ),
                      ),
                      SizedBox(height: 30.sp),
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          stringRecipieFiltersHeading.tr,
                          style: Style_24_Bold_Dark_Blue,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      spacerView(context, h: 64.sp),
                      _buildTagsViewList(),
                      spacerView(context, h: 64.sp),
                      Divider(),
                      spacerView(context, h: 64.sp),
                      Row(
                        children: [
                          getButtonPadding(context, btnLblCancel.tr,
                              ACTION_MAYBE_LATER, this,
                              isFilled: true,
                              color: greyF3,
                              textStyle: Style_18_Bold_Black,
                              padding: EdgeInsets.only(
                                  left: 48.sp,
                                  right: 48.sp,
                                  top: 32.sp,
                                  bottom: 32.sp)),
                          spacerView(context, w: 20.sp),
                          Expanded(
                            child: getButtonPadding(context,
                                stringApplyFilters.tr, actionApplyFilters, this,
                                isFilled: true,
                                padding: EdgeInsets.only(
                                    // left: 32.sp,
                                    // right: 32.sp,
                                    top: 32.sp,
                                    bottom: 32.sp)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ));
          });
        }).whenComplete(() {});
  }

  Widget _buildTagsViewList() {
    return ListView.separated(
      padding: EdgeInsets.zero,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return _buildTagTile(index);
      },
      separatorBuilder: (context, index) {
        return spacerView(context, h: 64.sp);
      },
      itemCount: mProvide?.recipieTagsType?.tagtypes?.length ?? 0,
    );
  }

  Widget _buildTagTile(i) {
    final GlobalKey<TagsState> _stateKey = GlobalKey<TagsState>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          mProvide?.recipieTagsType?.tagtypes?[i].name ?? "",
          style: Style_18_Bold_Black,
        ),
        spacerView(context, h: 24.sp),
        Tags(
          key: _stateKey,
          columns: 3,
          alignment: WrapAlignment.start,
          itemCount:
              mProvide?.recipieTagsType?.tagtypes?[i].tagsData?.data?.length ??
                  0,
          itemBuilder: (int index) {
            return ItemTags(
              key: Key(index.toString()),
              alignment: MainAxisAlignment.center,
              index: index,
              title: mProvide
                  ?.recipieTagsType?.tagtypes?[i].tagsData?.data?[index].name,
              textStyle: TextStyle(fontSize: 32.sp),
              activeColor: color_primary,
              active: mProvide?.recipieTagsType?.tagtypes?[i].tagsData
                  ?.data?[index].isSelected,
              borderRadius: BorderRadius.all(Radius.circular(16.sp)),
              border: Border.all(
                  color: (mProvide?.recipieTagsType?.tagtypes?[i].tagsData
                              ?.data?[index].isSelected ??
                          false)
                      ? color_primary
                      : black03),
              padding: EdgeInsets.only(
                  left: 22.sp, right: 22.sp, top: 20.sp, bottom: 20.sp),
              textColor: color_black,
              textActiveColor: color_white,
              combine: ItemTagsCombine.withTextBefore,
              onPressed: (item) {
                setState(() {
                  mProvide?.recipieTagsType?.tagtypes?[i].tagsData?.data?[index]
                      .isSelected = !(mProvide?.recipieTagsType?.tagtypes?[i]
                          .tagsData?.data?[index].isSelected ??
                      false);
                });
              },
              onLongPressed: (item) => print(item),
            );
          },
        ),
      ],
    );
  }
}
