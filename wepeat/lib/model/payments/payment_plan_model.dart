class PaymentPlansModel {
  List<Data>? data;

  PaymentPlansModel({this.data});

  PaymentPlansModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int? id;
  String? title;
  String? content;
  String? internalKey;
  int? trialDays;

  Data({this.id, this.title, this.content, this.internalKey, this.trialDays});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    content = json['content'];
    internalKey = json['internal_key'];
    trialDays = json['trial_days'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['content'] = this.content;
    data['internal_key'] = this.internalKey;
    data['trial_days'] = this.trialDays;
    return data;
  }
}
