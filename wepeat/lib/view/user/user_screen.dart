import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/dialog.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/viewmodel/user_viewmodel.dart';
import 'package:wepeat/widgets/bottom_nav_widget.dart';
import 'package:wepeat/widgets/network_image_widget.dart';

import '../../helper/navigation/routes.dart';

class UserScreen extends PageProvideNode<UserViewModel> {
  @override
  Widget buildContent(BuildContext context) {
    return _UserContentScreen(mProvider);
  }
}

/// View
class _UserContentScreen extends StatefulWidget {
  final UserViewModel provide;

  const _UserContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _UserContentState();
  }
}

class _UserContentState extends State<_UserContentScreen>
    with TickerProviderStateMixin<_UserContentScreen>
    implements Presenter {
  UserViewModel? mProvide;

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    setListeners();
    spUtil.putInt(keyBottomSelected, 2);
    mProvide?.getProfile(this);
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    // mProvide!.isApiResponseError.addListener(() {
    //   if (mProvide!.isApiResponseError.value) {
    //     // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
    //     setState(() {
    //       if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
    //         isErrorEmail = true;
    //         strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
    //       }
    //       if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
    //         isErrorPassword = true;
    //         strErrorPassword =
    //             mProvide!.loginErrorsResponse.errors!.password![0];
    //       }
    //     });
    //     mProvide!.isApiResponseError.value = false;
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build User Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: greyE5,
      bottomNavigationBar: const BottomNavWidget(),
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Stack(
            children: [
              _buildBody(),
              Consumer<UserViewModel>(builder: (context, value, child) {
                return getWidgetScreenLoader(context, value.loading);
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      // padding: EdgeInsets.only(top: 40.sp, bottom: 40.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildProfileView(),
          _buildPrefrencesView(),
          _buildPersonalInfoView(),
          _buildLogout(),
          spacerView(context, h: 20.sp)
        ],
      ),
    );
  }

  Widget _buildProfileView() {
    return Container(
      color: color_white,
      padding:
          EdgeInsets.only(top: 30.sp, bottom: 30.sp, left: 40.sp, right: 40.sp),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            child: Row(
              children: [
                NetworkImageWidget(
                  url: (mProvide?.userProfile?.iconUrl != null &&
                          mProvide?.userProfile?.iconUrl != "")
                      ? mProvide?.userProfile?.iconUrl
                      : imgPH,
                  height: 156.sp,
                  width: 156.sp,
                  shape: BoxShape.circle,
                ),
                spacerView(context, w: 24.sp),
                Container(
                  width: MediaQuery.of(context).size.width / 2,
                  child: Text(
                    mProvide?.userProfile?.name ?? "",
                    style: Style_28_Bold_Black03,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
          ),
          SvgPicture.asset(bellIcon)
        ],
      ),
    );
  }

  Widget _buildPrefrencesView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(left: 40.sp, top: 48.sp, bottom: 16.sp),
          child: Text(
            stringPreferences.tr,
            style: Style_24_Bold_Black03,
          ),
        ),
        InkWell(
          onTap: () => GeneralUtils.navigationPushAndRemoveUntil(
              context, Routes.homeWidget,
              routeName: Routes.home),
          child: Container(
            color: color_white,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(40.sp),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SvgPicture.asset(
                      image_home,
                      height: 56.sp,
                      width: 56.sp,
                      color: color_primary,
                    ),
                    spacerView(context, w: 48.sp),
                    Text(
                      stringMyHome.tr,
                      style: Style_20_Reg_B03,
                    ),
                  ],
                ),
                SvgPicture.asset(forwardIcon),
              ],
            ),
          ),
        ),
        InkWell(
          onTap: () => GeneralUtils.navigationPushAndRemoveUntil(
              context, Routes.planWidget,
              routeName: Routes.plan),
          child: Container(
              color: color_white,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(40.sp),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SvgPicture.asset(
                        image_plan,
                        height: 56.sp,
                        width: 56.sp,
                        color: color_primary,
                      ),
                      spacerView(context, w: 48.sp),
                      Text(
                        string_my_plan.tr,
                        style: Style_20_Reg_B03,
                      ),
                    ],
                  ),
                  SvgPicture.asset(forwardIcon)
                ],
              )),
        ),
        InkWell(
          onTap: () {
            mProvide?.getSites(this);
          },
          child: Container(
              color: color_white,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(40.sp),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.language,
                        size: 60.sp,
                        color: color_primary,
                      ),
                      spacerView(context, w: 48.sp),
                      Text(
                        stringChooseLanguage.tr,
                        style: Style_20_Reg_B03,
                      ),
                    ],
                  ),
                  SvgPicture.asset(forwardIcon)
                ],
              )),
        ),
      ],
    );
  }

  Widget _buildPersonalInfoView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(left: 40.sp, top: 48.sp, bottom: 16.sp),
          child: Text(
            stringPersonalInfo.tr,
            style: Style_24_Bold_Black03,
          ),
        ),
        InkWell(
          onTap: () {
            GeneralUtils.navigationPushTo(context, Routes.myDataWidget,
                routeName: Routes.myData);
          },
          child: Container(
            color: color_white,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(40.sp),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SvgPicture.asset(
                      image_user,
                      height: 56.sp,
                      width: 56.sp,
                      color: color_primary,
                    ),
                    spacerView(context, w: 48.sp),
                    Text(
                      stringMyData.tr,
                      style: Style_20_Reg_B03,
                    ),
                  ],
                ),
                SvgPicture.asset(forwardIcon)
              ],
            ),
          ),
        ),
        InkWell(
          onTap: () {
            GeneralUtils.navigationPushTo(context, Routes.subscriptionWidget,
                routeName: Routes.subscription);
          },
          child: Container(
            color: color_white,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(40.sp),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SvgPicture.asset(
                      dollorIcon,
                      height: 56.sp,
                      width: 56.sp,
                      color: color_primary,
                    ),
                    spacerView(context, w: 48.sp),
                    Text(
                      stringSubscriptions.tr,
                      style: Style_20_Reg_B03,
                    ),
                  ],
                ),
                SvgPicture.asset(forwardIcon)
              ],
            ),
          ),
        ),
        InkWell(
          onTap: () {
            GeneralUtils.navigationPushTo(context, Routes.billingDataWidget,
                routeName: Routes.billingData);
          },
          child: Container(
            color: color_white,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(40.sp),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SvgPicture.asset(
                      cardIcon,
                      height: 56.sp,
                      width: 56.sp,
                      color: color_primary,
                    ),
                    spacerView(context, w: 48.sp),
                    Text(
                      stringBillingData.tr,
                      style: Style_20_Reg_B03,
                    ),
                  ],
                ),
                SvgPicture.asset(forwardIcon)
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLogout() {
    return InkWell(
      onTap: () {
        showAlert(context, this,
            positiveText: string_confirm.tr,
            positiveAction: ACTION_ON_LOGOUT,
            title: string_logout_msg.tr);
      },
      child: Container(
        color: color_white,
        margin: EdgeInsets.only(top: 40.sp),
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(40.sp),
        child: Row(
          children: [
            SvgPicture.asset(
              image_user,
              height: 56.sp,
              width: 56.sp,
              color: color_primary,
            ),
            spacerView(context, w: 48.sp),
            Text(
              stringLogout.tr,
              style: Style_20_Reg_B03,
            ),
          ],
        ),
      ),
    );
  }

  void showLanguagePicker(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
              padding: EdgeInsets.all(40.sp),
              height: MediaQuery.of(context).size.height / 1.5,
              decoration: BoxDecoration(
                color: color_white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40.sp),
                    topRight: Radius.circular(40.sp)),
              ),
              child: ListView.separated(
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      spUtil.putString(
                          keySites, mProvide?.sitesRes?.data?[index].locale);
                      spUtil.putInt(
                          keySiteId, mProvide?.sitesRes?.data?[index].id ?? 1);
                      var locale = Locale(
                          mProvide?.sitesRes?.data?[index].locale ?? 'en');
                      Get.updateLocale(locale);
                      GeneralUtils.navigationPop(context);
                    },
                    child: Container(
                      padding: EdgeInsets.only(top: 20.sp, bottom: 20.sp),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            mProvide?.sitesRes?.data?[index].name ?? "",
                            style: Style_16_Reg_Black,
                          ),
                          if (mProvide?.sitesRes?.data?[index].locale ==
                              spUtil.getString(keySites))
                            const Icon(
                              Icons.check,
                              color: color_primary,
                            )
                        ],
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return spacerView(context, h: 20.sp);
                },
                itemCount: mProvide?.sitesRes?.data?.length ?? 0,
              ),
            );
          });
        }).whenComplete(() {});
  }

  @override
  void onClick(String? action) {
    switch (action) {
      case actionSuccess:
        setState(() {});
        break;
      case actionSuccessSites:
        // setState(() {
        //   showSites = true;
        // });
        showLanguagePicker(context);
        break;
      case ACTION_ON_LOGOUT:
        // logoutUser(context);
        mProvide?.logout(this, context);
        break;
    }
  }
}
