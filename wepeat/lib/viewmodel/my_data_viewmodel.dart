import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/transformers.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/dialog.dart';
import 'package:wepeat/model/user_profile.dart';

import '../di/repository.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../view/base.dart';

class MyDataViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();

  MyDataViewModel(this._repo);

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  Future<Stream?> updateProfile(
    Presenter presenter,
    String name,
    String email,
    String insta,
    String fb,
    String twitter,
    String pinterest,
    String youtube,
  ) async {
    if (await checkInternet()) {
      _repo
          .updateProfile(name, email, insta, fb, twitter, pinterest, youtube)
          .doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        UserProfile? userProfile = UserProfile.fromJson(r['data']);
        spUtil.user = userProfile;
        presenter.onClick(actionUpdatedSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> updatePassword(Presenter presenter, String email,
      String password, String confirmPassword) async {
    if (await checkInternet()) {
      _repo.updatePassword(email, password, confirmPassword).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        if (r == "true") presenter.onClick(actionUpdatedSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> updateUserImage(
      Presenter presenter, BuildContext context, img) async {
    if (await checkInternet()) {
      _repo.updateUserImage(img).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        UserProfile? userProfile = UserProfile.fromJson(r['data']);
        spUtil.user = userProfile;
        presenter.onClick(actionUpdatedSuccess);
      }, onError: (e) {
        //error
        loading = false;
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
            showToastDialog(context, body["message"]);
          }
        }
      });
    }
  }
}
