import 'package:wepeat/model/recipies/food_model.dart';
import 'package:wepeat/model/user_profile.dart';

import 'home_model.dart';

class CalendarItemsModel {
  List<CalendarItem>? items;
  String? data = "";

  CalendarItemsModel({this.data, this.items});

  CalendarItemsModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      items = <CalendarItem>[];
      json['data'].forEach((v) {
        items!.add(CalendarItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (items != null) {
      data['data'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CalendarItem {
  int? id;
  String? type;
  String? date;
  int? order;
  int? mealTypeId;
  String? mealTypeName;
  Options? options;
  ItemData? data;

  CalendarItem(
      {this.id,
      this.type,
      this.date,
      this.order,
      this.mealTypeId,
      this.mealTypeName,
      this.options,
      this.data});

  CalendarItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    date = json['date'];
    order = json['order'];
    mealTypeId = json['meal_type_id'];
    options =
        json['options'] != null ? Options.fromJson(json['options']) : null;
    data = json['data'] != null ? ItemData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['type'] = type;
    data['date'] = date;
    data['order'] = order;
    data['meal_type_id'] = mealTypeId;
    if (options != null) {
      data['options'] = options!.toJson();
    }
    // if (data != null) {
    //   data['data'] = data!.toJson();
    // }
    return data;
  }
}

class Options {
  int? portions;

  Options({this.portions});

  Options.fromJson(Map<String, dynamic> json) {
    portions = json['portions'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['portions'] = portions;
    return data;
  }
}

class ItemData {
  bool? isSelected;
  UserProfile? userProfile;
  String? content;
  Link? link;
  int? id;
  int? userId;
  bool? admin;
  String? title;
  String? excerpt;
  int? defaultMinutes;
  int? defaultPortions;
  bool? isBatchcooking;
  List<dynamic>? permissions;
  List<MediaData>? medias;
  List<StepsModel>? steps;
  List<FoodModel>? foods;

  String? source;
  String? sourceLink;

  ItemData({
    this.isSelected = false,
    this.userProfile,
    this.content,
    this.link,
    this.id,
    this.userId,
    this.admin,
    this.title,
    this.excerpt,
    this.defaultMinutes,
    this.defaultPortions,
    this.isBatchcooking,
    this.permissions,
    this.medias,
    this.source,
    this.sourceLink,
    this.steps,
    this.foods,
  });

  ItemData.fromJson(Map<String, dynamic> json) {
    content = json['content'];
    link = json['link'] != null ? Link.fromJson(json['link']) : null;
    id = json['id'] ?? -1;
    userId = json['user_id'] ?? -1;
    admin = json['admin'] ?? false;
    title = json['title'] ?? "";
    excerpt = json['excerpt'] ?? "";
    defaultMinutes = json['default_minutes'];
    defaultPortions = json['default_portions'];
    isBatchcooking =
        json['is_batchcooking'] ?? json["has_batchcooking_recipes"] ?? false;
    permissions = json['permissions'] ?? [];
    source = json['source'];
    sourceLink = json['source_link'];
    if (json['medias'] != null) {
      medias = <MediaData>[];
      json['medias'].forEach((v) {
        medias!.add(MediaData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['content'] = content;
    if (link != null) {
      data['link'] = link!.toJson();
    }
    data['id'] = id;
    data['user_id'] = userId;
    data['admin'] = admin;
    data['title'] = title;
    data['excerpt'] = excerpt;
    data['default_minutes'] = defaultMinutes;
    data['default_portions'] = defaultPortions;
    data['is_batchcooking'] = isBatchcooking;
    data['permissions'] = permissions;
    data['source'] = source;
    data['source_link'] = sourceLink;
    if (medias != null) {
      data['medias'] = medias!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Link {
  String? title;
  String? description;
  String? providerName;
  String? providerUrl;
  String? image;
  String? icon;
  String? favIcon;
  String? url;

  Link(
      {this.title,
      this.description,
      this.providerName,
      this.providerUrl,
      this.image,
      this.icon,
      this.favIcon,
      this.url});

  Link.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    description = json['description'];
    providerName = json['provider_name'];
    providerUrl = json['provider_url'];
    image = json['image'];
    icon = json['icon'];
    favIcon = json['fav_icon'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    data['description'] = description;
    data['provider_name'] = providerName;
    data['provider_url'] = providerUrl;
    data['image'] = image;
    data['icon'] = icon;
    data['fav_icon'] = favIcon;
    data['url'] = url;
    return data;
  }
}

class Conversions {
  List<String>? miniature;

  Conversions({this.miniature});

  Conversions.fromJson(Map<String, dynamic> json) {
    miniature = json['miniature'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['miniature'] = miniature;
    return data;
  }
}

class StepsModel {
  int? id;
  int? order;
  String? content;

  StepsModel({this.id, this.order, this.content});

  StepsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    order = json['order'];
    content = json['content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['order'] = order;
    data['content'] = content;
    return data;
  }
}
