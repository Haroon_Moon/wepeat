import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wepeat/di/app_module.dart';

import '../di/repository.dart';
import '../helper/constants.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../view/base.dart';

class RegisterViewModel extends BaseViewModel {
  final GithubRepo _repo;
  RegisterViewModel(this._repo);

  bool _loading = false;
  double _btnWidth = 200.0;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  double get btnWidth => _btnWidth;

  set btnWidth(double btnWidth) {
    _btnWidth = btnWidth;
    notifyListeners();
  }

  Future<Stream?> register(String name, String email, String password,
      String passwordConfirm, Presenter presenter) async {
    if (await checkInternet()) {
      _repo.register(name, email, password, passwordConfirm).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print("Token: " + r['token']);
        spUtil.putString(KEY_TOKEN, r['token']);
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }
}
