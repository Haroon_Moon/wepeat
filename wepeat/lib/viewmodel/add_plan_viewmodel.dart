import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/model/planners_model.dart';
import 'package:wepeat/model/tags/type_tags_model.dart';
import 'package:wepeat/model/tags_model.dart';

import '../di/repository.dart';
import '../model/calendar_items_model.dart';
import '../model/recipes_model.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../model/user_profile.dart';
import '../view/base.dart';

class AddPlanViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  bool isApiCalled = false;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();
  PlannersModel? planners;
  List<ItemData>? batchCookingMenu = [];
  List<ItemData>? normalMenu = [];
  TypeTagsModel? planTagsType;

  AddPlanViewModel(this._repo);

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  int noOfApiCalls = 0;
  Future<Stream?> getTagsType(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getTagsTypes("planners").doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) async {
        //success
        planTagsType = TypeTagsModel.fromJson(r);

        noOfApiCalls = 0;
        for (int i = 0; i < (planTagsType?.tagtypes?.length ?? 0); i++) {
          await getTagsId(presenter, (planTagsType?.tagtypes?[i].id ?? -1), i);
        }
        print(planTagsType);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getTagsId(Presenter presenter, int id, int index) async {
    if (await checkInternet()) {
      presenter.onClick(actionLoading);
      _repo.getTagsId(id).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        // loading = false;
      }).listen((r) {
        //success
        print(r);
        TagsModel tags = TagsModel.fromJson(r);
        planTagsType?.tagtypes?[index].tagsData = tags;
        print("$index done");
        noOfApiCalls++;

        if (noOfApiCalls == (planTagsType?.tagtypes?.length ?? 0)) {
          presenter.onClick(actionStopLoading);
        } else {
          presenter.onClick(actionLoading);
        }
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getPlanners(Presenter presenter, String? search,
      {required bool isBatchCooking, String? nextUrl}) async {
    batchCookingMenu?.clear();
    normalMenu?.clear();
    List<int> tags = [];
    planTagsType?.tagtypes?.forEach((type) {
      type.tagsData?.data?.forEach((element) {
        if (element.isSelected) {
          tags.add(element.id!);
        }
      });
    });
    if (await checkInternet()) {
      isApiCalled = false;
      _repo
          .getPlanners(search,
              nextUrl: nextUrl, tags: tags, isBatchCooking: isBatchCooking)
          .doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);

        PlannersModel mApiResponse = PlannersModel.fromJson(r);

        if (mApiResponse.meta?.currentPage != 1) {
          planners?.links = mApiResponse.links;
          planners?.meta = mApiResponse.meta;
          planners?.data?.addAll(mApiResponse.data!);
        } else {
          planners = mApiResponse;
        }

        planners?.data?.forEach((element) {
          if (element.isBatchcooking ?? false) {
            batchCookingMenu?.add(element);
          } else {
            normalMenu?.add(element);
          }
        });

        isApiCalled = true;
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getUser(
      Presenter presenter, int userId, int index, bool isBatchCooking) async {
    if (await checkInternet()) {
      _repo.getUser(userId).doOnListen(() {
        // loading = true;
      }).doOnDone(() {
        // loading = false;
      }).listen((r) {
        //success
        print(r);
        UserProfile userProfile = UserProfile.fromJson(r['data']);
        isBatchCooking
            ? batchCookingMenu![index].userProfile = userProfile
            : normalMenu![index].userProfile = userProfile;
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> addPlannerMeal(
      Presenter presenter, int plannerId, String date, String portions) async {
    if (await checkInternet()) {
      _repo.addPlannerMeal(plannerId, date, portions, "planner").doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }
}
