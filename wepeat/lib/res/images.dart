const imgPH =
    "https://antique-collector-app.s3.eu-west-2.amazonaws.com/project/media/default/44181__2021_6_21_824196.png";
const noImgPH =
    "https://firebasestorage.googleapis.com/v0/b/wingman-37a7a.appspot.com/o/Placeholders%2FNo-Image-Placeholder.svg.png?alt=media&token=920d900e-e172-4037-b059-07830cade443";

const basePath = "assets/img/";

const imageNoImgPH = basePath + "noImgPH.png";
const image_logo = basePath + "logo.svg";
const image_google = basePath + "ic_google.svg";
const image_facebook = basePath + "ic_facebook.svg";
const image_splash_bg = basePath + "splash_bg.jpg";
const image_home = basePath + "ic_home.svg";
const image_plan = basePath + "ic_plan.svg";
const image_user = basePath + "ic_user.svg";
const image_edit_home_info = basePath + "edit_home_info.svg";
const addUserIcon = basePath + "addUser.svg";
const deleteIcon = basePath + "deleteIcon.svg";
const syncIcon = basePath + "syncIcon.svg";
const forwardIcon = basePath + "forwardIcon.svg";
const bellIcon = basePath + "bellIcon.svg";
const dollorIcon = basePath + "dollorIcon.svg";
const cardIcon = basePath + "cardIcon.svg";
const eyeIcon = basePath + "eyeIcon.svg";
const statsIcon = basePath + "statsIcon.svg";
const addIcon = basePath + "addIcon.svg";
const dragIcon = basePath + "dragIcon.svg";
const menuIcon = basePath + "menuIcon.svg";
const chart = basePath + "chart.svg";
const details = basePath + "details.svg";
