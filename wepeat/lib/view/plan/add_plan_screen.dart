import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/device_utils.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/viewmodel/recipies_viewmodel.dart';
import 'package:wepeat/widgets/network_image_widget.dart';

import '../../viewmodel/add_plan_viewmodel.dart';

class AddPlanScreen extends PageProvideNode<AddPlanViewModel> {
  bool? isBatchCooking = false;

  AddPlanScreen({this.isBatchCooking = false});

  @override
  Widget buildContent(BuildContext context) {
    return _AddPlanContentScreen(mProvider, isBatchCooking);
  }
}

/// View
class _AddPlanContentScreen extends StatefulWidget {
  final AddPlanViewModel provide;
  bool? isBatchCooking;

  _AddPlanContentScreen(this.provide, this.isBatchCooking);

  @override
  State<StatefulWidget> createState() {
    return _AddPlanContentState();
  }
}

class _AddPlanContentState extends State<_AddPlanContentScreen>
    with TickerProviderStateMixin<_AddPlanContentScreen>
    implements Presenter {
  AddPlanViewModel? mProvide;
  TextEditingController etSearch = TextEditingController();
  TextEditingController etPortions = TextEditingController();
  DateTime? startDate;
  List<bool> portions = List<bool>.generate(6, (i) => false);
  Timer? timer;

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;

    mProvide?.getPlanners(this, etSearch.text.trim(),
        isBatchCooking: widget.isBatchCooking ?? false);
    mProvide?.getTagsType(this);
    // etSearch.addListener(() {
    //   _startSearchTimer();
    // });
    setListeners();
  }

  void _startSearchTimer() {
    if (timer != null && timer!.isActive) timer?.cancel();
    timer = Timer.periodic(const Duration(seconds: 1), (time) {
      mProvide?.getPlanners(this, etSearch.text.trim(),
          isBatchCooking: widget.isBatchCooking ?? false);
      time.cancel();
    });
  }

  @override
  void dispose() {
    print('-------dispose-------');
    if (timer != null) timer?.cancel();
    etPortions.dispose();
    etSearch.dispose();
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    // mProvide!.isApiResponseError.addListener(() {
    //   if (mProvide!.isApiResponseError.value) {
    //     // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
    //     setState(() {
    //       if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
    //         isErrorEmail = true;
    //         strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
    //       }
    //       if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
    //         isErrorPassword = true;
    //         strErrorPassword =
    //             mProvide!.loginErrorsResponse.errors!.password![0];
    //       }
    //     });
    //     mProvide!.isApiResponseError.value = false;
    //   }
    // });
  }

  @override
  void onClick(String? action) {
    print("onClick: " + action!);
    switch (action) {
      case ACTION_ON_CHANGE:
        _startSearchTimer();
        break;
      case actionLoading:
        setState(() {
          mProvide?.loading = true;
        });
        break;
      case actionStopLoading:
        setState(() {
          mProvide?.loading = false;
        });
        break;
      case actionSuccess:
        setState(() {});
        break;
      case actionCancel:
        GeneralUtils.navigationPop(context);
        break;
      case actionSaveDate:
        GeneralUtils.navigationPop(context);
        optionsPickerSheet(context);

        break;
      case actionAddMeanPlan:
        GeneralUtils.navigationPop(context);
        int? plannerId = mProvide?.planners?.data
            ?.firstWhere((element) => element.isSelected == true)
            .id;
        mProvide?.addPlannerMeal(
            this,
            plannerId!,
            DateFormat('yyyy-MM-dd').format(startDate!),
            etPortions.text.trim());

        break;
      case ACTION_ON_SUCCESS:
        GeneralUtils.navigationPushAndRemoveUntil(context, Routes.planWidget,
            routeName: Routes.plan);
        break;
      case ACTION_MAYBE_LATER:
        GeneralUtils.navigationPop(context);
        break;
      case actionApplyFilters:
        GeneralUtils.navigationPop(context);
        mProvide?.getPlanners(this, etSearch.text.trim(),
            isBatchCooking: widget.isBatchCooking ?? false);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Add Plan Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: transparent,
      resizeToAvoidBottomInset: true,
      body: InkWell(
        onTap: () {
          GeneralUtils.navigationPop(context);
        },
        child: Container(
          color: color_grey.withOpacity(0.7),
          child: Container(
            margin: EdgeInsets.only(top: 200.sp),
            child: Stack(
              children: [
                GestureDetector(
                  onTap: () {
                    FocusScopeNode currentFocus = FocusScope.of(context);
                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                  },
                  onPanUpdate: (details) {
                    print(details.delta.dy);
                    if (details.delta.dy > 0.0) {
                      GeneralUtils.navigationPop(context);
                    }
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: color_grey),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(54.sp),
                            topRight: Radius.circular(54.sp))),
                    child: Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.all(40.sp),
                          child: NotificationListener<ScrollNotification>(
                            onNotification: (ScrollNotification scrollInfo) {
                              if (scrollInfo is ScrollEndNotification &&
                                  scrollInfo.metrics.extentAfter == 0 &&
                                  scrollInfo.metrics.axis == Axis.vertical &&
                                  !(mProvide!.loading)) {
                                print("Scroll End");
                                if (mProvide?.planners?.links?.next != null) {
                                  mProvide?.getPlanners(
                                      this, etSearch.text.trim(),
                                      isBatchCooking:
                                          widget.isBatchCooking ?? false,
                                      nextUrl: mProvide?.planners?.links?.next);
                                }
                                return true;
                              }
                              return false;
                            },
                            child: SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      width: DeviceUtils.getScaledWidth(
                                          context, 40, 40),
                                      height: DeviceUtils.getScaledHeight(
                                          context, 5, 5),
                                      decoration: const BoxDecoration(
                                        color: color_grey,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(4)),
                                      ),
                                    ),
                                  ),
                                  spacerView(context, h: 40.sp),
                                  Stack(
                                    children: [
                                      getTextField(
                                          etSearch, "", stringSearch.tr,
                                          presenter: this,
                                          action: ACTION_ON_CHANGE),
                                      Positioned(
                                        top: 10,
                                        right: 10,
                                        child: Row(
                                          children: [
                                            InkWell(
                                              onTap: () {
                                                mProvide?.planTagsType?.tagtypes
                                                    ?.forEach((type) {
                                                  type.tagsData?.data
                                                      ?.forEach((element) {
                                                    if (element.isSelected) {
                                                      element.isSelected =
                                                          false;
                                                    }
                                                  });
                                                });
                                                mProvide?.getPlanners(
                                                    this, etSearch.text.trim(),
                                                    isBatchCooking:
                                                        widget.isBatchCooking ??
                                                            false);
                                              },
                                              child: Icon(
                                                Icons.close,
                                                color: color_primary,
                                              ),
                                            ),
                                            spacerView(context, w: 20.w),
                                            InkWell(
                                              onTap: () {
                                                showFilterPickerSheet(context);
                                              },
                                              child: Container(
                                                padding: EdgeInsets.all(20.sp),
                                                decoration: BoxDecoration(
                                                    color: greyC2,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                40.sp))),
                                                child: Center(
                                                  child: Text(
                                                    stringFilters.tr,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  spacerView(context, h: 64.sp),
                                  if (mProvide!.isApiCalled)
                                    Text(
                                      (((widget.isBatchCooking ?? false) &&
                                                  (mProvide!.batchCookingMenu!
                                                      .isEmpty)) ||
                                              ((!(widget.isBatchCooking ??
                                                      false) &&
                                                  mProvide!
                                                      .normalMenu!.isEmpty)))
                                          ? stringNoPlanners.tr
                                          : stringAddMealHeading.tr,
                                      style: Style_24_Bold_Black03,
                                    ),
                                  // spacerView(context, h: 12.sp),
                                  GridView.builder(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount: (widget.isBatchCooking ?? false)
                                        ? (mProvide?.batchCookingMenu?.length ??
                                            0)
                                        : (mProvide?.normalMenu?.length ?? 0),
                                    gridDelegate:
                                        const SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      crossAxisSpacing: 16.0,
                                      mainAxisSpacing: 24.0,
                                      mainAxisExtent: 250,
                                    ),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return InkWell(
                                        onTap: () {
                                          setState(() {
                                            if (widget.isBatchCooking ??
                                                false) {
                                              mProvide?.batchCookingMenu?[index]
                                                  .isSelected = !(mProvide!
                                                      .batchCookingMenu?[index]
                                                      .isSelected ??
                                                  false);
                                              mProvide?.batchCookingMenu
                                                  ?.forEach((element) {
                                                if (mProvide!
                                                        .batchCookingMenu![
                                                            index]
                                                        .id !=
                                                    element.id)
                                                  element.isSelected = false;
                                              });
                                            } else {
                                              mProvide?.normalMenu?[index]
                                                  .isSelected = !(mProvide!
                                                      .normalMenu?[index]
                                                      .isSelected ??
                                                  false);
                                              mProvide?.normalMenu
                                                  ?.forEach((element) {
                                                if (mProvide!.normalMenu![index]
                                                        .id !=
                                                    element.id)
                                                  element.isSelected = false;
                                              });
                                            }
                                          });
                                        },
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Stack(
                                              children: [
                                                NetworkImageWidget(
                                                  height: 332.sp,
                                                  width: 364.sp,
                                                  url: (widget.isBatchCooking ??
                                                          false)
                                                      ? (mProvide
                                                                  ?.batchCookingMenu?[
                                                                      index]
                                                                  .medias !=
                                                              null)
                                                          ? (mProvide
                                                                  ?.batchCookingMenu?[
                                                                      index]
                                                                  .medias
                                                                  ?.first
                                                                  .fullUrl ??
                                                              imgPH)
                                                          : imgPH
                                                      : (mProvide
                                                                  ?.normalMenu?[
                                                                      index]
                                                                  .medias !=
                                                              null)
                                                          ? (mProvide
                                                                  ?.normalMenu?[
                                                                      index]
                                                                  .medias
                                                                  ?.first
                                                                  .fullUrl ??
                                                              imgPH)
                                                          : imgPH,
                                                  radius: BorderRadius.all(
                                                    Radius.circular(28.sp),
                                                  ),
                                                ),
                                                Positioned(
                                                  top: 10,
                                                  left: 10,
                                                  child: InkWell(
                                                    onTap: () {
                                                      GeneralUtils
                                                          .navigationPushTo(
                                                              context,
                                                              Routes
                                                                  .planDetailWidget(
                                                                data: (widget
                                                                            .isBatchCooking ??
                                                                        false)
                                                                    ? (mProvide
                                                                            ?.batchCookingMenu?[
                                                                        index])
                                                                    : (mProvide
                                                                            ?.normalMenu?[
                                                                        index]),
                                                              ),
                                                              routeName: Routes
                                                                  .planDetail);
                                                    },
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(22.sp),
                                                      decoration:
                                                          const BoxDecoration(
                                                              color:
                                                                  color_white,
                                                              shape: BoxShape
                                                                  .circle),
                                                      child: const Icon(
                                                        Icons
                                                            .remove_red_eye_outlined,
                                                        color: black03,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                if ((widget.isBatchCooking ??
                                                        false)
                                                    ? (mProvide
                                                            ?.batchCookingMenu?[
                                                                index]
                                                            .isSelected ??
                                                        false)
                                                    : (mProvide
                                                            ?.normalMenu?[index]
                                                            .isSelected ??
                                                        false))
                                                  Positioned(
                                                    top: 10,
                                                    right: 10,
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(22.sp),
                                                      decoration:
                                                          const BoxDecoration(
                                                              color:
                                                                  color_primary,
                                                              shape: BoxShape
                                                                  .circle),
                                                      child: const Icon(
                                                        Icons.check,
                                                        color: color_white,
                                                      ),
                                                    ),
                                                  ),
                                              ],
                                            ),
                                            spacerView(context, h: 24.sp),
                                            Flexible(
                                              child: Text(
                                                (widget.isBatchCooking ?? false)
                                                    ? (mProvide
                                                            ?.batchCookingMenu?[
                                                                index]
                                                            .title ??
                                                        "")
                                                    : (mProvide
                                                            ?.normalMenu?[index]
                                                            .title ??
                                                        ""),
                                                style: Style_18_Bold_Black,
                                                maxLines: 2,
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        if ((widget.isBatchCooking ?? false)
                            ? (mProvide?.batchCookingMenu?.any(
                                    (element) => element.isSelected == true) ??
                                false)
                            : (mProvide?.normalMenu?.any(
                                    (element) => element.isSelected == true) ??
                                false))
                          Positioned(
                            bottom: 40,
                            left: 0,
                            right: 0,
                            child: InkWell(
                              onTap: () {
                                print("Tapped Go");
                                optionsPickerSheet(context);
                              },
                              child: Container(
                                margin:
                                    EdgeInsets.only(left: 96.sp, right: 96.sp),
                                padding:
                                    EdgeInsets.only(top: 32.sp, bottom: 32.sp),
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  color: color_primary,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(100.sp)),
                                ),
                                child: Center(
                                  child: Text(
                                    stringGoSelectOptions.tr,
                                    style: Style_18_Bold_White,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        Consumer<AddPlanViewModel>(
                            builder: (context, value, child) {
                          return getWidgetScreenLoader(context, value.loading);
                        }),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void showFilterPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
                padding:
                    EdgeInsets.all(DeviceUtils.getScaledX(context, 20, 20)),
                height: MediaQuery.of(context).size.height - 200.sp,
                decoration: BoxDecoration(
                  color: color_white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40.sp),
                      topRight: Radius.circular(40.sp)),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          width: 80.sp,
                          height: 10.sp,
                          decoration: BoxDecoration(
                            color: color_grey,
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.sp)),
                          ),
                        ),
                      ),
                      SizedBox(height: 30.sp),
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          stringRecipieFiltersHeading.tr,
                          style: Style_24_Bold_Dark_Blue,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      spacerView(context, h: 64.sp),
                      _buildTagsViewList(),
                      spacerView(context, h: 64.sp),
                      Divider(),
                      spacerView(context, h: 64.sp),
                      Row(
                        children: [
                          getButtonPadding(context, btnLblCancel.tr,
                              ACTION_MAYBE_LATER, this,
                              isFilled: true,
                              color: greyF3,
                              textStyle: Style_18_Bold_Black,
                              padding: EdgeInsets.only(
                                  left: 48.sp,
                                  right: 48.sp,
                                  top: 32.sp,
                                  bottom: 32.sp)),
                          spacerView(context, w: 20.sp),
                          Expanded(
                            child: getButtonPadding(context,
                                stringApplyFilters.tr, actionApplyFilters, this,
                                isFilled: true,
                                padding: EdgeInsets.only(
                                    // left: 32.sp,
                                    // right: 32.sp,
                                    top: 32.sp,
                                    bottom: 32.sp)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ));
          });
        }).whenComplete(() {});
  }

  Widget _buildTagsViewList() {
    return ListView.separated(
      padding: EdgeInsets.zero,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return _buildTagTile(index);
      },
      separatorBuilder: (context, index) {
        return spacerView(context, h: 64.sp);
      },
      itemCount: mProvide?.planTagsType?.tagtypes?.length ?? 0,
    );
  }

  Widget _buildTagTile(i) {
    final GlobalKey<TagsState> _stateKey = GlobalKey<TagsState>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          mProvide?.planTagsType?.tagtypes?[i].name ?? "",
          style: Style_18_Bold_Black,
        ),
        spacerView(context, h: 24.sp),
        Tags(
          key: _stateKey,
          columns: 3,
          alignment: WrapAlignment.start,
          itemCount:
              mProvide?.planTagsType?.tagtypes?[i].tagsData?.data?.length ?? 0,
          itemBuilder: (int index) {
            return ItemTags(
              key: Key(index.toString()),
              alignment: MainAxisAlignment.center,
              index: index,
              title: mProvide
                  ?.planTagsType?.tagtypes?[i].tagsData?.data?[index].name,
              textStyle: TextStyle(fontSize: 32.sp),
              activeColor: color_primary,
              active: mProvide?.planTagsType?.tagtypes?[i].tagsData
                  ?.data?[index].isSelected,
              borderRadius: BorderRadius.all(Radius.circular(16.sp)),
              border: Border.all(
                  color: (mProvide?.planTagsType?.tagtypes?[i].tagsData
                              ?.data?[index].isSelected ??
                          false)
                      ? color_primary
                      : black03),
              padding: EdgeInsets.only(
                  left: 22.sp, right: 22.sp, top: 20.sp, bottom: 20.sp),
              textColor: color_black,
              textActiveColor: color_white,
              combine: ItemTagsCombine.withTextBefore,
              onPressed: (item) {
                setState(() {
                  mProvide?.planTagsType?.tagtypes?[i].tagsData?.data?[index]
                      .isSelected = !(mProvide?.planTagsType?.tagtypes?[i]
                          .tagsData?.data?[index].isSelected ??
                      false);
                });
              },
              onLongPressed: (item) => print(item),
            );
          },
        ),
      ],
    );
  }

  void optionsPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
                padding: EdgeInsets.all(40.sp) +
                    EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                decoration: const BoxDecoration(
                  color: color_white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            width: DeviceUtils.getScaledWidth(context, 40, 40),
                            height: DeviceUtils.getScaledHeight(context, 5, 5),
                            decoration: const BoxDecoration(
                              color: color_grey,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                          ),
                        ),
                        spacerView(context, h: 60.h),
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            stringMealDealHeading.tr,
                            style: Style_24_Bold_Black03,
                          ),
                        ),
                        spacerView(context, h: 32.h),
                        Text(
                          stringFromWhatday.tr,
                          style: Style_18_Bold_Black,
                        ),
                        spacerView(context, h: 12.h),
                        InkWell(
                          onTap: () {
                            GeneralUtils.navigationPop(context);
                            datePickerSheet(context);
                          },
                          child: Container(
                            width: ScreenUtil().screenWidth,
                            padding: EdgeInsets.only(
                                top: 32.sp, bottom: 32.sp, left: 32.sp),
                            decoration: BoxDecoration(
                              border: Border.all(color: greyC2),
                              borderRadius: BorderRadius.all(
                                Radius.circular(4.r),
                              ),
                            ),
                            child: Text(
                              startDate != null
                                  ? DateFormat('yyyy-MM-dd').format(startDate!)
                                  : stringSelectdate.tr,
                              style: Style_16_Reg_Black,
                            ),
                          ),
                        ),
                        spacerView(context, h: 32.h),
                        Text(
                          stringHowManyPeople.tr,
                          style: Style_18_Bold_Black,
                        ),
                        spacerView(context, h: 12.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[0] = true;
                                  etPortions.text = "1";
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color:
                                      portions[0] ? color_primary : color_white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: portions[0] ? transparent : greyC2,
                                  ),
                                ),
                                child: Text(
                                  "1",
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[1] = true;
                                  etPortions.text = "2";
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color:
                                      portions[1] ? color_primary : color_white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: portions[1] ? transparent : greyC2,
                                  ),
                                ),
                                child: Text(
                                  "2",
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[2] = true;
                                  etPortions.text = "3";
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color:
                                      portions[2] ? color_primary : color_white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: portions[2] ? transparent : greyC2,
                                  ),
                                ),
                                child: Text(
                                  "3",
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[3] = true;
                                  etPortions.text = "4";
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color:
                                      portions[3] ? color_primary : color_white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: portions[3] ? transparent : greyC2,
                                  ),
                                ),
                                child: Text(
                                  "4",
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[4] = true;
                                  etPortions.text = "5";
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color:
                                      portions[4] ? color_primary : color_white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: portions[4] ? transparent : greyC2,
                                  ),
                                ),
                                child: Text(
                                  "5",
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                mystate(() {
                                  for (int i = 0; i < portions.length; i++) {
                                    portions[i] = false;
                                  }
                                  portions[5] = true;
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(30.r),
                                decoration: BoxDecoration(
                                  color: color_primary.withOpacity(0.4),
                                  shape: BoxShape.circle,
                                ),
                                child: Text(
                                  "+",
                                ),
                              ),
                            ),
                          ],
                        ),
                        spacerView(context, h: 12.h),
                        if (portions[5])
                          getTextField(etPortions, "", stringEnterPortions.tr,
                              presenter: this,
                              action: ACTION_ON_NAME_CHANGE,
                              isMarginTop: false,
                              keyboardType: TextInputType.number),
                        spacerView(context, h: 65.h),
                        Row(
                          children: [
                            getButtonPadding(
                              context,
                              btnLblCancel.tr,
                              actionCancel,
                              this,
                              color: greyF3,
                              textStyle: Style_14_Bold_Black,
                              padding: EdgeInsets.only(
                                  top: 32.sp,
                                  bottom: 32.sp,
                                  left: 48.sp,
                                  right: 48.sp),
                            ),
                            spacerView(context, w: 20.sp),
                            Expanded(
                              child: getButtonPadding(
                                context,
                                stringAddMealPlan.tr,
                                actionAddMeanPlan,
                                this,
                                textStyle: Style_14_Bold_WHITE,
                                padding: EdgeInsets.only(
                                  top: 32.sp,
                                  bottom: 32.sp,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ));
          });
        }).whenComplete(() {
      setState(() {});
    });
  }

  void datePickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
                padding: EdgeInsets.all(40.sp),
                decoration: const BoxDecoration(
                  color: color_white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: DeviceUtils.getScaledWidth(context, 40, 40),
                          height: DeviceUtils.getScaledHeight(context, 5, 5),
                          decoration: const BoxDecoration(
                            color: color_grey,
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                          ),
                        ),
                        spacerView(context, h: 92.h),
                        Text(
                          stringSelectDate.tr,
                          style: Style_24_Reg_Black03,
                        ),
                        spacerView(context, h: 32.h),
                        Container(
                          height: 500.sp,
                          child: SfDateRangePicker(
                            startRangeSelectionColor: color_primary,
                            endRangeSelectionColor: color_primary,
                            selectionColor: color_primary.withOpacity(0.5),
                            rangeSelectionColor: color_primary.withOpacity(0.5),
                            onSelectionChanged: _onSelectionChanged,
                            selectionMode: DateRangePickerSelectionMode.single,
                            initialSelectedRange:
                                PickerDateRange(startDate, startDate),
                          ),
                        ),
                        spacerView(context, h: 32.h),
                        Row(
                          children: [
                            getButtonPadding(
                              context,
                              btnLblCancel.tr,
                              actionCancel,
                              this,
                              color: greyF3,
                              textStyle: Style_14_Bold_Black,
                              padding: EdgeInsets.only(
                                  top: 32.sp,
                                  bottom: 32.sp,
                                  left: 48.sp,
                                  right: 48.sp),
                            ),
                            spacerView(context, w: 20.sp),
                            Expanded(
                              child: getButtonPadding(
                                context,
                                btnLblConfirmDate.tr,
                                actionSaveDate,
                                this,
                                textStyle: Style_14_Bold_WHITE,
                                padding: EdgeInsets.only(
                                  top: 32.sp,
                                  bottom: 32.sp,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ));
          });
        }).whenComplete(() {
      setState(() {});
    });
  }

  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    print(args);
    setState(() {
      if (args.value is PickerDateRange) {
        startDate = args.value.startDate;
      } else if (args.value is DateTime) {
        startDate = args.value;
      } else if (args.value is List<DateTime>) {
        // _dateCount = args.value.length.toString();
      } else {
        // _rangeCount = args.value.length.toString();
      }
    });
    print(startDate);
  }
}
