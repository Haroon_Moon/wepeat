import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/fonts.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/view/base.dart';

class BottomNavWidget extends StatefulWidget {
  const BottomNavWidget({Key? key}) : super(key: key);

  @override
  State<BottomNavWidget> createState() => _BottomNavWidgetState();
}

class _BottomNavWidgetState extends State<BottomNavWidget>
    implements Presenter {
  int bottomNavigationIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: spUtil.getInt(keyBottomSelected) ?? 1,
      backgroundColor: color_white,
      selectedItemColor: color_black,
      unselectedItemColor: color_placeholder,
      selectedFontSize: font_14,
      unselectedFontSize: font_14,
      selectedLabelStyle: Style_14_Bold_Black,
      unselectedLabelStyle: Style_14_SemiBold_Grey,
      onTap: (value) {
        // Respond to item press.
        print("Value: " + value.toString());
        setState(() => bottomNavigationIndex = value);
        switch (bottomNavigationIndex) {
          case 0:
            onClick(ACTION_MY_HOME);
            break;
          case 1:
            onClick(ACTION_MY_PLAN);
            break;
          case 2:
            onClick(ACTION_MY_PROFILE);
            break;
        }
      },
      items: [
        BottomNavigationBarItem(
          label: string_my_home.tr,
          icon: Padding(
            padding: EdgeInsets.only(bottom: 10.sp),
            child: SvgPicture.asset(
              image_home,
              height: 60.sp,
              width: 60.sp,
              color: spUtil.getInt(keyBottomSelected) == 0
                  ? color_primary
                  : color_black,
            ),
          ),
        ),
        BottomNavigationBarItem(
          label: string_my_plan.tr,
          icon: Padding(
            padding: EdgeInsets.only(bottom: 10.sp),
            child: SvgPicture.asset(
              image_plan,
              height: 60.sp,
              width: 60.sp,
              color: spUtil.getInt(keyBottomSelected) == 1
                  ? color_primary
                  : color_black,
            ),
          ),
        ),
        BottomNavigationBarItem(
            label: string_you.tr,
            icon: Padding(
              padding: EdgeInsets.only(bottom: 10.sp),
              child: SvgPicture.asset(
                image_user,
                height: 60.sp,
                width: 60.sp,
                color: spUtil.getInt(keyBottomSelected) == 2
                    ? color_primary
                    : color_black,
              ),
            )),
      ],
    );
  }

  @override
  void onClick(String? action) {
    switch (action) {
      case ACTION_MY_HOME:
        GeneralUtils.navigationPushReplacement(context, Routes.homeWidget,
            routeName: Routes.home);
        break;
      case ACTION_MY_PLAN:
        GeneralUtils.navigationPushReplacement(context, Routes.planWidget,
            routeName: Routes.plan);
        break;
      case ACTION_MY_PROFILE:
        GeneralUtils.navigationPushReplacement(context, Routes.userWidget,
            routeName: Routes.user);
        break;
    }
  }
}
