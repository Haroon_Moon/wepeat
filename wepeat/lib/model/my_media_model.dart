class MyMedia {
  int? mediaType;
  int? productMediaId;
  String? mediaPath;
  String? thumbnail;
  int? order;
  bool isPlay = false;

  MyMedia({
    this.mediaType,
    this.productMediaId,
    this.mediaPath,
    this.thumbnail,
    this.order,
  });

  MyMedia.fromJson(Map<String, dynamic> json) {
    mediaType = json['media_type'];
    productMediaId = json['product_media_id'];
    mediaPath = json['media_path'];
    thumbnail = json['thumbnail'];
    order = json['order'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['media_type'] = mediaType;
    data['product_media_id'] = productMediaId;
    data['media_path'] = mediaPath;
    data['thumbnail'] = thumbnail;
    data['order'] = order;
    return data;
  }

  // Map<String, dynamic> toFileJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   File file = File(this.mediaPath!);
  //   String fileName = file.path.split('/').last;
  //   File fileThumbnail = File(this.thumbnail!);
  //   String fileThumbnailName = file.path.split('/').last;
  //   data['media_type'] = this.mediaType;
  //   data['media_file'] =
  //       MultipartFile.fromFileSync(file.path, filename: fileName);
  //   data['thumbnail'] = "";
  //   // MultipartFile.fromFileSync(fileThumbnail.path, filename: fileThumbnailName);
  //   return data;
  // }
}
