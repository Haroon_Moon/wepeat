import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/widgets/bottom_nav_widget.dart';
import '../../viewmodel/billing_data_viewmodel.dart';
import '../../viewmodel/subscription_viewmodel.dart';

class SubscriptionScreen extends PageProvideNode<SubscriptionViewModel> {
  @override
  Widget buildContent(BuildContext context) {
    return _SubscriptionContentScreen(mProvider);
  }
}

/// View
class _SubscriptionContentScreen extends StatefulWidget {
  final SubscriptionViewModel provide;

  const _SubscriptionContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _SubscriptionContentState();
  }
}

class _SubscriptionContentState extends State<_SubscriptionContentScreen>
    with TickerProviderStateMixin<_SubscriptionContentScreen>
    implements Presenter {
  SubscriptionViewModel? mProvide;
  TextEditingController etName = TextEditingController();
  TextEditingController etEmail = TextEditingController();
  TextEditingController etPhone = TextEditingController();
  TextEditingController etCountry = TextEditingController();
  TextEditingController etState = TextEditingController();
  TextEditingController etCity = TextEditingController();
  TextEditingController etAddress = TextEditingController();
  TextEditingController etZip = TextEditingController();

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    setListeners();
    mProvide?.getPlans(this);
    mProvide?.getUserSubscription(this);
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    // mProvide!.isApiResponseError.addListener(() {
    //   if (mProvide!.isApiResponseError.value) {
    //     // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
    //     setState(() {
    //       if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
    //         isErrorEmail = true;
    //         strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
    //       }
    //       if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
    //         isErrorPassword = true;
    //         strErrorPassword =
    //             mProvide!.loginErrorsResponse.errors!.password![0];
    //       }
    //     });
    //     mProvide!.isApiResponseError.value = false;
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Subscription Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: color_white,
      bottomNavigationBar: const BottomNavWidget(),
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Stack(
            children: [
              _buildBody(),
              Consumer<SubscriptionViewModel>(builder: (context, value, child) {
                return getWidgetScreenLoader(context, value.loading);
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(40.sp),
          child: Row(
            children: [
              InkWell(
                onTap: () {
                  GeneralUtils.navigationPop(context);
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                ),
              ),
              spacerView(context, w: 32.sp),
              Text(
                stringMySubs.tr,
                style: Style_30_Bold_Black05,
              ),
            ],
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                _buildTopView(),
                spacerView(context, h: 64.sp),
                _buildBottomView(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildTopView() {
    return Padding(
      padding: EdgeInsets.all(40.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            stringChoosePlan.tr,
            style: Style_18_Bold_Black,
          ),
          Text(
            stringSubscribeTrial.tr,
            style: Style_18_Reg_Black,
          ),
          spacerView(context, h: 64.sp),
          Row(
            children: [
              Expanded(
                child: Container(
                  // height: 280.sp,
                  padding: EdgeInsets.only(
                      left: 30.sp, right: 30.sp, top: 50.sp, bottom: 50.sp),
                  decoration: BoxDecoration(
                    border: Border.all(color: greyC2),
                    borderRadius: BorderRadius.all(Radius.circular(20.sp)),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Text(
                      //   "4,95€",
                      //   style: Style_32_Bold_Black03,
                      // ),
                      // spacerView(context, h: 16.sp),
                      Text(
                        (mProvide?.plans?.data?.length ?? 0) >= 1
                            ? (mProvide?.plans?.data?[0].title ?? "")
                            : "",
                        // "Monthly",
                        style: Style_16_Bold_Black,
                      ),
                      spacerView(context, h: 16.sp),
                      Text(
                        (mProvide?.plans?.data?.length ?? 0) >= 1
                            ? (mProvide?.plans?.data?[0].content ?? "")
                            : "",
                        // "Starter pack, pay monthly",
                        style: Style_14_Reg_Black,
                      ),
                    ],
                  ),
                ),
              ),
              spacerView(context, w: 20.sp),
              Expanded(
                child: Container(
                  // height: 280.sp,
                  padding: EdgeInsets.only(
                      left: 30.sp, right: 30.sp, top: 50.sp, bottom: 50.sp),
                  decoration: BoxDecoration(
                    border: Border.all(color: greyC2),
                    borderRadius: BorderRadius.all(Radius.circular(20.sp)),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Text(
                      //   "39,95€",
                      //   style: Style_32_Bold_Black03,
                      // ),
                      // spacerView(context, h: 16.sp),
                      Text(
                        (mProvide?.plans?.data?.length ?? 0) > 1
                            ? (mProvide?.plans?.data?[1].title ?? "")
                            : "",
                        style: Style_16_Bold_Black,
                      ),
                      spacerView(context, h: 16.sp),
                      Text(
                        (mProvide?.plans?.data?.length ?? 0) > 1
                            ? (mProvide?.plans?.data?[1].content ?? "")
                            : "",
                        // "Starter pack, pay monthly",
                        style: Style_14_Reg_Black,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          spacerView(context, h: 20.sp),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 34.sp, bottom: 34.sp),
            decoration: BoxDecoration(
              color: greyF3,
              borderRadius: BorderRadius.all(Radius.circular(20.sp)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  stringYouAreNow.tr,
                  style: Style_16_Reg_Black,
                ),
                Text(
                  stringFreeUser.tr,
                  style: Style_16_Bold_Black,
                ),
              ],
            ),
          ),
          spacerView(context, h: 64.sp),
          getButtonPadding(
            context,
            btnLblGotoSubscription.tr,
            actionOpenSubscription,
            this,
            color: goldenCE,
            textStyle: Style_14_Bold_WHITE,
            padding: EdgeInsets.only(
              top: 32.sp,
              bottom: 32.sp,
            ),
          ),
          spacerView(context, h: 24.sp),
          getButtonPadding(
            context,
            btnLblHistory.tr,
            actionOpenhistory,
            this,
            isFilled: false,
            textStyle: Style_14_Bold_Black,
            padding: EdgeInsets.only(
              top: 32.sp,
              bottom: 32.sp,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBottomView() {
    return Container(
      padding: EdgeInsets.all(48.sp),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: goldenCE.withOpacity(0.3),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(60.sp),
          topRight: Radius.circular(60.sp),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            stringOfferTitle.tr,
            style: Style_20_Bold_Black,
          ),
          spacerView(context, h: 48.sp),
          Row(
            children: [
              Icon(
                Icons.check_circle,
                color: goldenCE,
              ),
              spacerView(context, w: 12.sp),
              Text(
                stringOffer1.tr,
                style: Style_18_Reg_Black03,
              ),
            ],
          ),
          spacerView(context, h: 56.sp),
          Row(
            children: [
              Icon(
                Icons.check_circle,
                color: goldenCE,
              ),
              spacerView(context, w: 12.sp),
              Text(
                stringOffer2.tr,
                style: Style_18_Reg_Black03,
              ),
            ],
          ),
          spacerView(context, h: 56.sp),
          Row(
            children: [
              Icon(
                Icons.check_circle,
                color: goldenCE,
              ),
              spacerView(context, w: 12.sp),
              Text(
                stringOffer3.tr,
                style: Style_18_Reg_Black03,
              ),
            ],
          ),
          spacerView(context, h: 56.sp),
          Row(
            children: [
              Icon(
                Icons.check_circle,
                color: goldenCE,
              ),
              spacerView(context, w: 12.sp),
              Text(
                stringOffer4.tr,
                style: Style_18_Reg_Black03,
              ),
            ],
          ),
          spacerView(context, h: 56.sp),
          Row(
            children: [
              Icon(
                Icons.check_circle,
                color: goldenCE,
              ),
              spacerView(context, w: 12.sp),
              Text(
                stringOffer5.tr,
                style: Style_18_Reg_Black03,
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void onClick(String? action) {
    switch (action) {
      // case actionCancel:
      //   GeneralUtils.navigationPop(context);
      //   break;
      // case actionSaveChanges:
      //   //Tapped Apply changes button
      //   break;
      case actionSuccess:
        setState(() {});
        break;
      case actionOpenSubscription:
        // GeneralUtils.navigationPushTo(context, Routes.addCardWidget(),
        //     routeName: Routes.addCard);
        mProvide?.initStripePaymentSheet();
        break;
    }
  }
}
