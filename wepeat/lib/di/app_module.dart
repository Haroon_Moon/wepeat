// @dart=2.9
import 'dart:io';

import 'package:dartin/dartin.dart';
import 'package:dio/dio.dart';
import 'package:wepeat/di/repository.dart';
import 'package:wepeat/view/plan/recipie_preview_screen.dart';
import 'package:wepeat/viewmodel/add_plan_viewmodel.dart';
import 'package:wepeat/viewmodel/edit_home_mem_viewmodel.dart';
import 'package:wepeat/viewmodel/home_viewmodel.dart';
import 'package:wepeat/viewmodel/my_data_viewmodel.dart';
import 'package:wepeat/viewmodel/plan_detail_viewmodel.dart';
import 'package:wepeat/viewmodel/plan_viewmodel.dart';
import 'package:wepeat/viewmodel/recipie_preview_viewmodel.dart';
import 'package:wepeat/viewmodel/recipies_viewmodel.dart';
import 'package:wepeat/viewmodel/subscription_viewmodel.dart';
import 'package:wepeat/viewmodel/user_viewmodel.dart';

import '../helper/constants.dart';
import '../helper/shared_preferences.dart';
import '../viewmodel/billing_data_viewmodel.dart';
import '../viewmodel/global_viewmodel.dart';
import '../viewmodel/login_viewmodel.dart';
import '../viewmodel/register_viewmodel.dart';
import '../viewmodel/splash_viewmodel.dart';

const testScope = DartInScope('test');

/// ViewModel
final viewModelModule = Module([
  factory<SplashViewModel>(({params}) => SplashViewModel(get())),
  factory<LoginViewModel>(({params}) => LoginViewModel(get())),
  factory<RegisterViewModel>(({params}) => RegisterViewModel(get())),
  factory<GlobalViewModel>(({params}) => GlobalViewModel()),
  factory<HomeViewModel>(({params}) => HomeViewModel(get())),
  factory<EditHomeMemViewModel>(({params}) => EditHomeMemViewModel(get())),
  factory<PlanViewModel>(({params}) => PlanViewModel(get())),
  factory<RecipiesViewModel>(({params}) => RecipiesViewModel(get())),
  factory<UserViewModel>(({params}) => UserViewModel(get())),
  factory<MyDataViewModel>(({params}) => MyDataViewModel(get())),
  factory<BillingDataViewModel>(({params}) => BillingDataViewModel(get())),
  factory<SubscriptionViewModel>(({params}) => SubscriptionViewModel(get())),
  factory<RecipiePreviewViewModel>(
      ({params}) => RecipiePreviewViewModel(get())),
  factory<AddPlanViewModel>(({params}) => AddPlanViewModel(get())),
  factory<PlanDetailViewModel>(({params}) => PlanDetailViewModel(get())),
])
  ..withScope(testScope, [
    ///other scope
//  factory<HomeViewModel>(({params}) => HomeViewModel(params.get(0), get<GithubRepo>())),
  ]);

/// Repository
final repoModule = Module([
  factory<GithubRepo>(({params}) => GithubRepo(get())),
]);

/// Remote
final remoteModule = Module([
  factory<GithubService>(({params}) => GithubService()),
]);

/// Local
final localModule = Module([
  single<SpUtil>(({params}) => spUtil),
]);

final appModule = [viewModelModule, repoModule, remoteModule, localModule];

/// AuthInterceptor
class AuthInterceptor extends Interceptor {
  @override
  onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    final token = spUtil.getString(KEY_TOKEN);
    if (token != null && token != "") {
      options.headers.update("token", (_) => token, ifAbsent: () => token);
      options.headers[HttpHeaders.authorizationHeader] = 'Bearer ' + token;
    }
    // options.headers.update("Authorization", (_) => "Bearer " + token, ifAbsent: () => "Bearer " + token);

    return super.onRequest(options, handler);
  }
}

Dio dio = Dio()
  ..options = BaseOptions(
      baseUrl: BASE_URL, connectTimeout: 900000, receiveTimeout: 900000)
  ..interceptors.add(AuthInterceptor())
  ..options.headers["Content-Type"] = "application/json"
  ..options.headers["Accept"] = "application/json"
  ..interceptors.add(LogInterceptor(responseBody: true, requestBody: true));

SpUtil spUtil;

/// init
init() async {
  spUtil = await SpUtil.getInstance();

// DartIn start
  startDartIn(appModule);
}
