import 'package:flutter/material.dart';
import 'package:wepeat/view/home/edit_home_mem_screen.dart';
import 'package:wepeat/view/home/home_screen.dart';
import 'package:wepeat/view/login_screen.dart';
import 'package:wepeat/view/payments/add_card_screen.dart';
import 'package:wepeat/view/plan/add_plan_screen.dart';
import 'package:wepeat/view/plan/plan_detail_screen.dart';
import 'package:wepeat/view/plan/plan_screen.dart';
import 'package:wepeat/view/plan/recipie_preview_screen.dart';
import 'package:wepeat/view/plan/recipies_screen.dart';
import 'package:wepeat/view/splash_screen.dart';
import 'package:wepeat/view/user/billing_data_screen.dart';
import 'package:wepeat/view/user/my_data_screen.dart';
import 'package:wepeat/view/user/subscription_screen.dart';

import '../../model/home_model.dart';
import '../../view/user/user_screen.dart';

class Routes {
  Routes._();

  //static variables
  static const String splash = '/splash';
  static const String tutorial = '/tutorial';
  static const String welcome = '/welcome';
  static const String login = '/login';
  static const String register = '/register';
  static const String home = '/home';
  static const String plan = '/plan';
  static const String user = '/user';
  static const String myData = '/myData';
  static const String billingData = '/billingData';
  static const String subscription = '/subscription';
  static const String editHomeMem = '/editHomeMem';
  static const String recipies = '/recipies';
  static const String recipiesPreview = '/recipiesPreview';
  static const String addPlan = '/addPlan';
  static const String planDetail = '/planDetail';

  static const String addCard = '/addCard';

  static final routes = <String, WidgetBuilder>{
    splash: (BuildContext context) => SplashScreen(),
    login: (BuildContext context) => LoginScreen(),
    home: (BuildContext context) => HomeScreen(),
    plan: (BuildContext context) => PlanScreen(),
    user: (BuildContext context) => UserScreen(),
    editHomeMem: (BuildContext context) => EditHomeMemScreen(),
    recipies: (BuildContext context) => RecipieScreen(),
    billingData: (BuildContext context) => BillingDataScreen(),
    subscription: (BuildContext context) => SubscriptionScreen(),
    recipiesPreview: (BuildContext context) => RecipiePreviewScreen(),
    addPlan: (BuildContext context) => AddPlanScreen(),
    planDetail: (BuildContext context) => PlanDetailScreen(),
    addCard: (BuildContext context) => AddCardScreen(),
  };

  static Widget splashWidget = SplashScreen();
  static Widget loginWidget = LoginScreen();
  static Widget homeWidget = HomeScreen();
  static Widget planWidget = PlanScreen();
  static Widget userWidget = UserScreen();
  static Widget myDataWidget = MyDataScreen();
  static Widget billingDataWidget = BillingDataScreen();
  static Widget subscriptionWidget = SubscriptionScreen();
  static Widget editHomeMemWidget({bool? isEdit, Member? member}) =>
      EditHomeMemScreen(isEdit: isEdit, homeMem: member);
  static Widget recipieWidget(date, mealTypeId) =>
      RecipieScreen(date: date, mealTypeId: mealTypeId);
  static Widget recipiesPreviewWidget(itemId, recipieId) =>
      RecipiePreviewScreen(itemId: itemId, recipieId: recipieId);
  static Widget addPlanWidget({bool? isBatchCooking}) =>
      AddPlanScreen(isBatchCooking: isBatchCooking);
  static Widget planDetailWidget({data}) => PlanDetailScreen(planner: data);

  static Widget addCardWidget({data}) => AddCardScreen();
}
