// Share Preferences Keys

const BASE_URL = "https://api.wepeat.desarrollo.systems/v1/clients";

const testKey =
    "pk_test_51IlubZIpSGIJs4U9DeMWzl4pZBo435AnuK2PoyFzCAk1Dy0tSLrVpBFwrrurDCn9OS1e2a8CHOWwobvk9iaAA1CC006JlWjevz";

const secretKey =
    "sk_test_51IlubZIpSGIJs4U9KKbglyY2EzhNoFaJiBHTAvTUiWVBvFvufgfmqRjETkUwyMhQgRQ4T0N5rzneHuhE72Nuf67600I1S6gyWk";

// EndPoints Auth
const ENDPOINT_LOGIN = "/auth";
const ENDPOINT_REGISTER = "/signup";
const ENDPOINT_SOCIAL = "/auth/provider";
const ENDPOINT_MY_PROFILE = "/me/profile";
const ENDPOINT_MY_IMAGE = "/me/profile/image";
const ENDPOINT_MY_CREDENTIALS = "/me/auth/credentials";

const ENDPOINT_MY_BILLING = "/me/billing";
const ENDPOINT_Logout = "/me/auth";
const ENDPOINT_MY_HOME = "/me/home";
const ENDPOINT_MY_HOME_TAGS = "/me/home/tags";
const ENDPOINT_MY_HOME_MEMBERS = "/me/home/members";
const ENDPOINT_ALL_TAGS = "/tags";
const ENDPOINT_ALL_TAG_TYPES = "/tagtypes";
const ENDPOINT_PLANNERS = "/planners";
const ENDPOINT_ADD_MEMBER = "/me/home/members";
const ENDPOINT_UPDATE_MEMBER = "/me/home/members/";
const ENDPOINT_RECIPES = "/recipes";
const ENDPOINT_MEAL_TYPES = "/mealtypes";
const ENDPOINT_CALENDAR = "/me/calendar";
const ENDPOINT_CALENDAR_ITEMS = "/me/calendar/items";
const ENDPOINT_USER = "/users/";

const ENDPOINT_FOODS = "/foods";
const ENDPOINT_MEASURES = "/measures";

const ENDPOINT_USER_SUBSCRIPTION = "/me/subscriptions";
const ENDPOINT_PLANS = "/plans";

const ENDPOINT_SITES = "/sites";

// Preferences
const KEY_TOKEN = "token";
const KEY_FCM_TOKEN = "fcm_token";
const KEY_USER = "user";
const keyBottomSelected = "bottomSelectedIndex";
const keyShowEditHomTutorial = "keyShowEditHomTutorial";
const keySites = "keySites";
const keySiteId = "keySiteId";

// Actions
const ACTION_BACK = "back";
const ACTION_MENU = "menu";
const ACTION_SEARCH = "search";
const ACTION_IS_SHOW_BOTTOM_NAV = "isShowBottomNav";
const ACTION_NEXT = "next";
const ACTION_PREVIOUS = "previous";
const ACTION_SKIP = "skip";
const ACTION_GET_START = "getStarted";
const ACTION_LOG_IN = "login";
const ACTION_FORGOT = "forgot";
const ACTION_REGISTER = "register";
const ACTION_CONFIRM = "confirm";
const ACTION_RESET = "reset";
const ACTION_FINISH = "finish";
const ACTION_PASSWORD_VISIBILITY = "passwordVisible";
const ACTION_ON_GO = "onGO";
const ACTION_LOGIN_SUCCESS = "loginSuccess";
const ACTION_ON_EMAIL_CHANGE = "onEmailChange";
const ACTION_ON_PLAN_TITLE_CHANGE = "onPlanTitleChange";
const ACTION_ON_PLAN_DESC_CHANGE = "onPlanDescChange";
const ACTION_ON_NAME_CHANGE = "onNameChange";
const ACTION_ON_CHANGE = "onNameChange";
const ACTION_ON_PASSWORD_CHANGE = "onPasswordChange";
const ACTION_ON_NEW_PASSOWRD_CHANGE = "onNewPasswordChange";
const ACTION_ON_GET_VALIDATION = "onGetValidation";
const ACTION_REGISTER_SUCCESS = "registerSuccess";
const ACTION_ON_SUCCESS = "onSuccess";
const ACTION_ON_USERNAME_CHANGE = "onUsernameChange";
const ACTION_ON_VALIDATION_SUCCESS = "onValidationSuccess";
const ACTION_EDIT_PROFILE_IMAGE = "editProfileImage";
const ACTION_ON_LOGOUT = "onLogout";
const ACTION_ON_GOOGLE = "onGoogle";
const ACTION_ON_FACEBOOK = "onFacebook";
const ACTION_MY_HOME = "myHome";
const ACTION_MY_PLAN = "myPlan";
const ACTION_MY_PROFILE = "myProfile";
const ACTION_ON_REFRESH = "onRefresh";

const ACTION_EDIT_HOME = "editHome";
const ACTION_MAYBE_LATER = "maybeLater";
const actionEditMember = "editMember";
const actionEditHome = "editHome";
const ACTION_CONFIRM_ADD_MEMBER = "confirmAddMember";
const ACTION_CONFIRM_UPDATE_MEMBER = "confirmUpdateMember";

const actionUpdateHome = "updateHome";
const actionAddMember = "addMember";
const actionDelMember = "delMember";
const actionSuccess = "success";
const actionSuccessSites = "successSites";
const actionUpdatedSuccess = "actionUpdatedSuccess";
const actionLoading = "loading";
const actionStopLoading = "stopLoading";
const actionSuccessDelItem = "actionSuccessDelItem";
const actionOpenHomeEditPicker = "actionOpenHomeEditPicker";
const actionSuccessRecipesBatch = "actionSuccessRecipesBatch";
const actionSuccessRecipesNormal = "actionSuccessRecipesNormal";
const actionApplyFilters = "actionApplyFilters";
const actionOpenEditMember = "openEditMember";
const actionOpenEditHome = "openEditHome";
const actionAddMenu = "actionAddMenu";
const actionOpenAddRecipie = "actionOpenAddRecipie";
const actionAddRecipie = "actionAddRecipie";
const actionRecipieMenu = "actionRecipieMenu";
const actionAddFav = "actionAddFav";
const actionBatchcookingMenu = "actionBatchcookingMenu";
const actionRemoveRecipie = "actionRemoveRecipie";
const actionNormalMenu = "actionNormalMenu";
const actionMoreOptions = "actionMoreOptions";
const actionGenrate = "actionGenrateShoppingList";
const actionPrintPlan = "actionPrintPlan";
const actionEditPlan = "actionEditPlan";
const actionCancel = "actionCancel";
const actionSaveChanges = "actionSaveChanges";
const actionSaveDate = "actionSaveDate";
const actionAddNote = "actionAddNote";
const actionRecipiePrecview = "actionRecipiePrecview";
const actionUserTagsSuccess = "actionUserTagsSuccess";
const actionOpenSubscription = "actionOpenSubscription";
const actionOpenhistory = "actionOpenhistory";
const actionAddMeanPlan = "actionAddMeanPlan";
const actionAddRecipiePlan = "actionAddRecipiePlan";
const actionAddNoteDone = "actionAddNoteDone";
const actionOpenLink = "actionOpenLink";
const actionPay = "actionPay";

const actionFilterBreakfast = "actionFilterBreakfast";
const actionFilterBrunch = "actionFilterBrunch";
const actionFilterLunch = "actionFilterLunch";
const actionFilterSnacks = "actionFilterSnacks";
const actionFilterDinner = "actionFilterDinner";

const actionSuccessAddNote = "actionSuccessAddNote";
const actionSuccessAddRecipie = "actionSuccessAddRecipie";

// Errors
const ERROR_INVALID = "invalid";

const TYPE_FACEBOOK = "facebook";
const TYPE_GOOGLE = "google";

const TYPE_RECIPIE = "recipe";
const TYPE_RECIPIE_MEAL = "recipe_meal";
