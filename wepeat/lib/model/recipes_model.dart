import 'package:wepeat/model/pagination_model.dart';
import 'package:wepeat/model/user_profile.dart';

import 'home_model.dart';

class RecipesModel {
  List<Recipe>? recipes;
  Links? links;
  Meta? meta;

  RecipesModel({this.recipes, this.links, this.meta});

  RecipesModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      recipes = <Recipe>[];
      json['data'].forEach((v) {
        recipes!.add(Recipe.fromJson(v));
      });
    }
    links = json['links'] != null ? Links.fromJson(json['links']) : null;
    meta = json['meta'] != null ? Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (recipes != null) {
      data['data'] = recipes!.map((v) => v.toJson()).toList();
    }
    if (links != null) {
      data['links'] = links!.toJson();
    }
    if (meta != null) {
      data['meta'] = meta!.toJson();
    }
    return data;
  }
}

class Recipe {
  int? id;
  int? userId;
  UserProfile? userProfile;
  bool? admin;
  String? title;
  String? excerpt;
  int? defaultMinutes;
  int? defaultPortions;
  bool? isBatchcooking;
  List<dynamic>? permissions;
  List<MediaData>? medias;
  String? content;
  dynamic? source;
  String? sourceLink;
  bool isSelected = false;

  Recipe(
      {this.isSelected = false,
      this.id,
      this.userId,
      this.userProfile,
      this.admin,
      this.title,
      this.excerpt,
      this.defaultMinutes,
      this.defaultPortions,
      this.isBatchcooking,
      this.permissions,
      this.medias,
      this.content,
      this.source,
      this.sourceLink});

  Recipe.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    admin = json['admin'];
    title = json['title'];
    excerpt = json['excerpt'];
    defaultMinutes = json['default_minutes'];
    defaultPortions = json['default_portions'];
    isBatchcooking = json['is_batchcooking'];
    content = json['content'];
    source = json['source'];
    sourceLink = json['source_link'];
    if (json['permissions'] != null) {
      permissions = <Null>[];
      json['permissions'].forEach((v) {
        // permissions!.add(new Null.fromJson(v));
      });
    }
    if (json['medias'] != null) {
      medias = <MediaData>[];
      json['medias'].forEach((v) {
        medias!.add(MediaData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['admin'] = admin;
    data['title'] = title;
    data['excerpt'] = excerpt;
    data['default_minutes'] = defaultMinutes;
    data['default_portions'] = defaultPortions;
    data['is_batchcooking'] = isBatchcooking;
    data['content'] = content;
    data['source'] = source;
    data['source_link'] = sourceLink;
    if (permissions != null) {
      data['permissions'] = permissions!.map((v) => v?.toJson()).toList();
    }
    if (medias != null) {
      data['medias'] = medias!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Meta {
  int? currentPage;
  int? from;
  int? lastPage;
  List<Links>? links;
  String? path;
  int? perPage;
  int? to;
  int? total;

  Meta(
      {this.currentPage,
      this.from,
      this.lastPage,
      this.links,
      this.path,
      this.perPage,
      this.to,
      this.total});

  Meta.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    from = json['from'];
    lastPage = json['last_page'];
    if (json['links'] != null) {
      links = <Links>[];
      json['links'].forEach((v) {
        links!.add(Links.fromJson(v));
      });
    }
    path = json['path'];
    perPage = json['per_page'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['current_page'] = currentPage;
    data['from'] = from;
    data['last_page'] = lastPage;
    if (links != null) {
      data['links'] = links!.map((v) => v.toJson()).toList();
    }
    data['path'] = path;
    data['per_page'] = perPage;
    data['to'] = to;
    data['total'] = total;
    return data;
  }
}
