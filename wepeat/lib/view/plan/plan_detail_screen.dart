import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/widgets/bottom_nav_widget.dart';
import 'package:wepeat/widgets/network_image_widget.dart';

import '../../helper/constants.dart';
import '../../helper/device_utils.dart';
import '../../model/calendar_items_model.dart';
import '../../viewmodel/plan_detail_viewmodel.dart';
import '../../viewmodel/recipie_preview_viewmodel.dart';

class PlanDetailScreen extends PageProvideNode<PlanDetailViewModel> {
  ItemData? planner;

  PlanDetailScreen({this.planner});

  @override
  Widget buildContent(BuildContext context) {
    return _PlanDetailContentScreen(mProvider, this.planner);
  }
}

/// View
class _PlanDetailContentScreen extends StatefulWidget {
  final PlanDetailViewModel provide;
  ItemData? planner;

  _PlanDetailContentScreen(this.provide, this.planner);

  @override
  State<StatefulWidget> createState() {
    return _PlanDetailContentState();
  }
}

class _PlanDetailContentState extends State<_PlanDetailContentScreen>
    with TickerProviderStateMixin<_PlanDetailContentScreen>
    implements Presenter {
  PlanDetailViewModel? mProvide;
  double _animatedHeight = 500;

  @override
  void onClick(String? action) {
    // TODO: implement onClick
    print(action);
    switch (action) {
      case ACTION_ON_SUCCESS:
        if (mounted) setState(() {});
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    mProvide?.getRecipies(this, widget.planner?.id ?? -1);
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Plan Detail Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: color_white,
      bottomNavigationBar: const BottomNavWidget(),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Stack(
          children: [
            _buildHeaderView(),
            _buildBody(),
            Consumer<PlanDetailViewModel>(builder: (context, value, child) {
              return getWidgetScreenLoader(context, value.loading);
            }),
          ],
        ),
      ),
    );
  }

  Widget _buildHeaderView() {
    return Stack(
      children: [
        NetworkImageWidget(
          url: widget.planner?.medias?.length != 0
              ? (widget.planner?.medias?.first.fullUrl ?? noImgPH)
              : noImgPH,
          height: 660.sp,
          width: MediaQuery.of(context).size.width,
        ),
        Positioned(
          top: MediaQuery.of(context).padding.top + 5,
          right: 5,
          child: InkWell(
            onTap: () {
              GeneralUtils.navigationPop(context);
            },
            child: CircleAvatar(
              foregroundColor: black03,
              child: Icon(
                Icons.close_rounded,
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildBody() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: GestureDetector(
        onVerticalDragStart: (DragStartDetails details) {
          print(details);
          setState(() {
            if (details.globalPosition > Offset.zero) {
              _animatedHeight = _animatedHeight != 500.0 ? 500.0 : 750.0;
            }
          });
        },
        onTap: () {
          setState(() {
            _animatedHeight = _animatedHeight != 500.0 ? 500.0 : 750.0;
          });
        },
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          height: _animatedHeight,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(
            color: color_white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          child: SingleChildScrollView(
            padding: EdgeInsets.all(40.sp),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    width: DeviceUtils.getScaledWidth(context, 40, 40),
                    height: DeviceUtils.getScaledHeight(context, 5, 5),
                    decoration: const BoxDecoration(
                      color: color_grey,
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                    ),
                  ),
                ),
                spacerView(context, h: 44.sp),
                Text(
                  widget.planner?.title ?? "",
                  style: Style_32_Bold_Black03,
                ),
                spacerView(context, h: 24.sp),
                // Row(
                //   children: [
                //     Icon(
                //       Icons.calendar_today_outlined,
                //       color: color_primary,
                //     ),
                //     // Text(
                //     //   (widget.planner. ?? "") +
                //     //       " minutes",
                //     // ),
                //   ],
                // ),
                // spacerView(context, h: 24.sp),
                Divider(),
                spacerView(context, h: 24.sp),
                if (widget.planner?.excerpt?.isNotEmpty ?? false)
                  Text(
                    stringDescription.tr,
                    style: Style_24_Bold_Black03,
                  ),
                spacerView(context, h: 24.sp),
                Text(
                  widget.planner?.excerpt ?? "",
                  style: Style_18_Reg_Black03,
                ),
                spacerView(context, h: 24.sp),
                if (mProvide?.recipies?.data != null)
                  Text(
                    stringRecipieIncluded.tr,
                    style: Style_24_Bold_Black03,
                  ),
                spacerView(context, h: 24.sp),
                _recipiesView(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _recipiesView() {
    return GridView.builder(
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: mProvide?.recipies?.data?.length ?? 0,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 16.0,
        mainAxisSpacing: 24.0,
        mainAxisExtent: 300,
      ),
      itemBuilder: (BuildContext context, int index) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            NetworkImageWidget(
              height: 332.sp,
              width: 364.sp,
              url: (mProvide?.recipies?.data?[index].recipe?.medias != null)
                  ? (mProvide?.recipies?.data?[index].recipe?.medias?.first
                          .fullUrl ??
                      noImgPH)
                  : noImgPH,
              radius: BorderRadius.all(
                Radius.circular(28.sp),
              ),
            ),
            spacerView(context, h: 24.sp),
            Text(
              (mProvide?.recipies?.data?[index].recipe?.title ?? ""),
              style: Style_18_Bold_Black,
              maxLines: 2,
            ),
            // spacerView(context, h: 12.sp),
            Row(
              children: [
                Icon(
                  Icons.timer_outlined,
                  color: color_primary,
                ),
                Text(
                  (mProvide?.recipies?.data?[index].recipe?.defaultMinutes
                              .toString() ??
                          "") +
                      stringMinutes.tr,
                  style: Style_14_Reg_Black,
                ),
              ],
            ),
            Row(
              children: [
                Icon(
                  Icons.person,
                  color: color_primary,
                ),
                Text(
                  (mProvide?.recipies?.data?[index].recipe?.defaultPortions
                              .toString() ??
                          "") +
                      stringPortions.tr,
                  style: Style_14_Reg_Black,
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
