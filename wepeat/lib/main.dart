// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/localization.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/view/splash_screen.dart';
import 'di/app_module.dart';

BuildContext contextGlobal;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Stripe.publishableKey = testKey;
  await Stripe.instance.applySettings();

  await init();
  if (spUtil.getString(keySites) == null) {
    spUtil.putString(keySites, "es");
    if (spUtil.getInt(keySiteId) == null) {
      spUtil.putInt(keySiteId, 1);
    }
  }

  runApp(MyApp());
}

/// View
class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //     statusBarIconBrightness: Brightness.dark,
    //     statusBarColor: color_app_bar
    // ));

    // var brightness = SchedulerBinding.instance.window.platformBrightness;

    return ScreenUtilInit(
        designSize: const Size(428, 940),
        // designSize: Size(750, 1334),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) => GetMaterialApp(
              debugShowCheckedModeBanner: false,
              builder: (BuildContext context, Widget child) {
                // ScreenUtil.setContext(context);
                return MediaQuery(
                  data: MediaQuery.of(context).copyWith(
                    textScaleFactor: 1.0,
                  ), //set desired text scale factor here
                  child: child,
                );
              },
              // theme: ThemeData(
              //   highlightColor: transparent,
              //   splashColor: transparent,
              //   focusColor: transparent,
              //   hoverColor: transparent,
              //   primarySwatch: materialColor,
              //   primaryColor: Colors.white,
              //   dialogBackgroundColor: Colors.black54,
              //   dialogTheme: const DialogTheme(backgroundColor: Colors.black54),
              //   platform: TargetPlatform.windows,
              //   appBarTheme: const AppBarTheme(
              //       systemOverlayStyle: SystemUiOverlayStyle.light),
              // ),
              // darkTheme: ThemeData(
              //   highlightColor: transparent,
              //   splashColor: transparent,
              //   focusColor: transparent,
              //   hoverColor: transparent,
              //   primarySwatch: materialColor,
              //   primaryColor: Colors.black,
              //   checkboxTheme: CheckboxThemeData(
              //     fillColor: MaterialStateProperty.all(Colors.white),
              //     overlayColor: MaterialStateProperty.all(Colors.green),
              //     checkColor: MaterialStateProperty.all(Colors.amberAccent),
              //     side: const BorderSide(color: Colors.white, width: 1),
              //   ),
              //   dialogBackgroundColor: Colors.black54,
              //   dialogTheme: const DialogTheme(backgroundColor: Colors.black54),
              //   platform: TargetPlatform.windows,
              //   appBarTheme: const AppBarTheme(
              //       systemOverlayStyle: SystemUiOverlayStyle.light),
              // ),
              translations: LocaleString(),
              locale: (spUtil.getString(keySites) != null
                  ? Locale(spUtil.getString(keySites))
                  : const Locale('en', 'US')),
              home: SplashScreen(),
            ));
  }
}
