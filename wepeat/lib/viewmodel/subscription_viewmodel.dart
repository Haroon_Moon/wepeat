import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:rxdart/transformers.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/model/payments/payment_plan_model.dart';
import 'package:wepeat/model/payments/user_subscription_model.dart';
import 'package:wepeat/model/user_profile.dart';
import 'package:http/http.dart' as http;
import '../di/repository.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../view/base.dart';

class SubscriptionViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();
  PaymentPlansModel? plans;

  SubscriptionViewModel(this._repo);

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  Future<Stream?> getPlans(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getPlans().doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        plans = PaymentPlansModel.fromJson(r);

        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getUserSubscription(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getUserSubscription().doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);

        if (r['data'] != null) {
          List<UserSubscriptionModel> data = [];
          r['data'].forEach((v) {
            data.add(UserSubscriptionModel.fromJson(v));
          });

          UserProfile user = spUtil.user;
          user.subscriptionData = data;
          spUtil.user = user;
        }

        // presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<void> initStripePaymentSheet() async {
    try {
      // 1. create payment intent on the server
      final paymentSheetData = await createPaymentIntent();
      print("payment intent created");

      // 2. initialize the payment sheet
      await Stripe.instance.initPaymentSheet(
          paymentSheetParameters: SetupPaymentSheetParameters(
        style: ThemeMode.dark,
        merchantDisplayName: 'Prospects',
        customerId: paymentSheetData!['customer'],
        paymentIntentClientSecret: paymentSheetData!['client_secret'],
        // customerEphemeralKeySecret: paymentSheetData['ephemeralKey'],
      ));
      print("payment sheet created");

      await Stripe.instance.presentPaymentSheet();

      print("Payment done");

      print("after payment sheet presented");
    } on Exception catch (e) {
      if (e is StripeException) {
        print("Error from Stripe: ${e.error.localizedMessage}");
        // loading(false);
      } else {
        print("Unforeseen error: ${e}");
        // loading(false);
      }
      rethrow;
    }
  }

  createPaymentIntent() async {
    try {
      var response = await http.post(
        Uri.parse("https://api.stripe.com/v1/payment_intents"),
        headers: {
          'Authorization': 'Bearer $secretKey',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: {
          'amount': '100',
          'currency': 'usd',
          'payment_method_types[]': 'card'
        },
      );
      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        print(json.decode(response.body));
        throw 'Failed to create PaymentIntents.';
      }
    } catch (e) {
      print(e);
    }
  }
}
