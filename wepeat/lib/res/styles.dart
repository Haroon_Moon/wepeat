import 'package:flutter/material.dart';

import 'colors.dart';
import 'fonts.dart';

TextStyle Style_10_Reg_White = TextStyle(
  color: color_white,
  fontSize: font_10,
  fontFamily: "DosisRegular",
);

TextStyle Style_12_Reg_White = TextStyle(
  color: color_white,
  fontSize: font_12,
  fontFamily: "DosisRegular",
);

TextStyle Style_12_Bold_Red = TextStyle(
  color: Colors.red,
  fontSize: font_12,
  fontFamily: "DosisBold",
);

TextStyle Style_14_Reg_White = TextStyle(
  color: color_white,
  fontSize: font_14,
  fontFamily: "DosisRegular",
);

TextStyle Style_16_Reg_White = TextStyle(
  color: color_white,
  fontSize: font_16,
  fontFamily: "DosisRegular",
);

TextStyle Style_18_Reg_White = TextStyle(
  color: color_white,
  fontSize: font_18,
  fontFamily: "DosisRegular",
);

TextStyle Style_20_Reg_White = TextStyle(
  color: color_white,
  fontSize: font_20,
  fontFamily: "DosisRegular",
);

TextStyle Style_20_Reg_B03 = TextStyle(
  color: black03,
  fontSize: font_20,
  fontFamily: "DosisRegular",
);

TextStyle Style_16_Reg_Placeholder = TextStyle(
  color: color_placeholder,
  fontSize: font_16,
  fontFamily: "DosisRegular",
);

TextStyle Style_14_Bold_Black = TextStyle(
  color: color_black,
  fontSize: font_14,
  fontFamily: "DosisBold",
);
TextStyle Style_14_Bold_WHITE = TextStyle(
  color: color_white,
  fontSize: font_14,
  fontFamily: "DosisBold",
);
TextStyle Style_20_Bold_Black = TextStyle(
  color: color_black,
  fontSize: font_20,
  fontFamily: "DosisBold",
);
TextStyle Style_20_Bold_Black03 = TextStyle(
  color: black03,
  fontSize: font_20,
  fontFamily: "DosisBold",
);

TextStyle Style_30_Bold_Black05 = TextStyle(
  color: black05,
  fontSize: font_30,
  fontFamily: "DosisBold",
);

TextStyle Style_16_Reg_Black05 = TextStyle(
  color: black05,
  fontSize: font_16,
  fontFamily: "DosisRegular",
);

TextStyle Style_24_Bold_Dark_Blue = TextStyle(
  color: color_dark_blue,
  fontSize: font_24,
  fontFamily: "DosisBold",
);

TextStyle Style_14_SemiBold_Grey = TextStyle(
  color: color_placeholder,
  fontSize: font_14,
  fontFamily: "DosisSemiBold",
);

TextStyle Style_16_Bold_White = TextStyle(
  color: color_white,
  fontSize: font_16,
  fontFamily: "DosisBold",
);

TextStyle Style_16_Bold_Black = TextStyle(
  color: color_black,
  fontSize: font_16,
  fontFamily: "DosisBold",
);

TextStyle Style_18_Bold_White = TextStyle(
  color: color_white,
  fontSize: font_18,
  fontFamily: "DosisBold",
);

TextStyle Style_18_Bold_Red92 = TextStyle(
  color: red92,
  fontSize: font_18,
  fontFamily: "DosisBold",
);

TextStyle Style_10_Reg_Black = TextStyle(
  color: color_black,
  fontSize: font_10,
  fontFamily: "DosisRegular",
);

TextStyle Style_12_Reg_Black = TextStyle(
  color: color_black,
  fontSize: font_12,
  fontFamily: "DosisRegular",
);
TextStyle Style_14_Reg_Black = TextStyle(
  color: color_black,
  fontSize: font_14,
  fontFamily: "DosisRegular",
);

TextStyle Style_16_Reg_Black = TextStyle(
  color: color_black,
  fontSize: font_16,
  fontFamily: "DosisRegular",
);

TextStyle Style_18_Reg_Black = TextStyle(
  color: color_black,
  fontSize: font_18,
  fontFamily: "DosisRegular",
);

TextStyle Style_18_Reg_Black03 = TextStyle(
  color: black03,
  fontSize: font_18,
  fontFamily: "DosisRegular",
);

TextStyle Style_18_Bold_Black = TextStyle(
  color: color_black,
  fontSize: font_18,
  fontFamily: "DosisBold",
);

TextStyle Style_24_Bold_Black03 = TextStyle(
  color: black03,
  fontSize: font_24,
  fontFamily: "DosisBold",
);
TextStyle Style_24_Med_Black03 = TextStyle(
  color: black03,
  fontSize: font_24,
  fontFamily: "DosisSemiBold",
);

TextStyle Style_24_Reg_Black03 = TextStyle(
  color: black03,
  fontSize: font_24,
  fontFamily: "DosisRegular",
);

TextStyle Style_28_Bold_Black03 = TextStyle(
  color: black03,
  fontSize: font_28,
  fontFamily: "DosisBold",
);
TextStyle Style_32_Bold_Black03 = TextStyle(
  color: black03,
  fontSize: font_32,
  fontFamily: "DosisBold",
);

TextStyle Style_18_Bold_Primary = TextStyle(
  color: color_primary,
  fontSize: font_18,
  fontFamily: "DosisBold",
);
