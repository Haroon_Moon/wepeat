import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:wepeat/model/sites/sites_model.dart';
import 'package:wepeat/model/user_profile.dart';

import '../di/repository.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../view/base.dart';

class UserViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();
  UserProfile? userProfile;
  SitesModel? sitesRes;

  UserViewModel(this._repo);

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  Future<Stream?> getProfile(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getProfile().doOnListen(() {
        // loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        userProfile = UserProfile.fromJson(r['data']);
        spUtil.user = userProfile!;
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getSites(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getSites().doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        sitesRes = SitesModel.fromJson(r);
        presenter.onClick(actionSuccessSites);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> logout(Presenter presenter, BuildContext context) async {
    if (await checkInternet()) {
      _repo.logout().doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        logoutUser(context);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }
}
