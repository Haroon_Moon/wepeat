import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:rxdart/transformers.dart';
import 'package:wepeat/model/calendar_items_model.dart';
import 'package:wepeat/model/planner_recipies_model.dart';
import 'package:wepeat/model/recipes_model.dart';

import '../di/repository.dart';
import '../helper/constants.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../view/base.dart';

class PlanDetailViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();
  PlannersRecipies? recipies;

  PlanDetailViewModel(this._repo);
  bool get loading => _loading;
  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  Future<Stream?> getRecipies(Presenter presenter, int id) async {
    if (await checkInternet()) {
      _repo.getPlanRecipies(id).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) async {
        //success
        print(r);
        recipies = PlannersRecipies.fromJson(r);
        for (int i = 0; i < (recipies?.data?.length ?? 0); i++) {
          await getRecipieDetail(
              presenter, (recipies?.data?[i].recipeId ?? -1), i);
        }
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getRecipieDetail(
      Presenter presenter, int id, int index) async {
    if (await checkInternet()) {
      _repo.getRecipieDetail(id).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);
        Recipe detail = Recipe.fromJson(r['data']);
        recipies?.data?[index].recipe = detail;
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }
}
