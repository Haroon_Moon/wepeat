import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wepeat/di/app_module.dart';

import '../di/repository.dart';
import '../helper/constants.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../view/base.dart';

class LoginViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  double _btnWidth = 200.0;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();

  LoginViewModel(this._repo);

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  double get btnWidth => _btnWidth;

  set btnWidth(double btnWidth) {
    _btnWidth = btnWidth;
    notifyListeners();
  }

  Future<Stream?> login(String email, String password, String deviceName,
      Presenter presenter) async {
    if (await checkInternet()) {
      _repo.login(email, password, deviceName).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print("Token: " + r['token']);
        spUtil.putString(KEY_TOKEN, r['token']);
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> socialLogin(
      String token, String type, Presenter presenter) async {
    if (await checkInternet()) {
      loading = true;
      _repo.socialLogin(token, type).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print("Token: " + r['token']);
        spUtil.putString(KEY_TOKEN, r['token']);
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }
}
