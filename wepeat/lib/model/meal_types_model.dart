class MealTypesModel {
  List<MealType>? type;

  MealTypesModel({this.type});

  MealTypesModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      type = <MealType>[];
      json['data'].forEach((v) {
        type!.add(MealType.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (type != null) {
      data['data'] = type!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MealType {
  int? id;
  String? key;
  String? name;
  String? description;

  MealType({this.id, this.key, this.name, this.description});

  MealType.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    key = json['key'];
    name = json['name'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['key'] = key;
    data['name'] = name;
    data['description'] = description;
    return data;
  }
}
