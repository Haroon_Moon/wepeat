import 'dart:async';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
// import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/device_utils.dart';
import 'package:wepeat/helper/dialog.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/viewmodel/plan_viewmodel.dart';
import 'package:wepeat/widgets/bottom_nav_widget.dart';
import 'package:wepeat/widgets/network_image_widget.dart';

import '../../model/calendar_items_model.dart';

class PlanScreen extends PageProvideNode<PlanViewModel> {
  @override
  Widget buildContent(BuildContext context) {
    return _PlanContentScreen(mProvider);
  }
}

/// View
class _PlanContentScreen extends StatefulWidget {
  final PlanViewModel provide;

  const _PlanContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _PlanContentState();
  }
}

class _PlanContentState extends State<_PlanContentScreen>
    with TickerProviderStateMixin<_PlanContentScreen>
    implements Presenter {
  PlanViewModel? mProvide;
  TextEditingController etTitle = TextEditingController();
  TextEditingController etDesc = TextEditingController();
  TextEditingController etSearch = TextEditingController();
  TextEditingController etEcternalLink = TextEditingController();
  TextEditingController etNoteDesc = TextEditingController();
  DateTime startDate = DateTime.now().subtract(const Duration(days: 7));
  DateTime endDate = DateTime.now();
  List<bool> filtersSelection = [true, true, true, true, true];
  bool noFiltersApplied = true;
  String? selectedDate;
  int? mealTypeId;
  int? itemId;
  String? itemType;
  int? recipieId;

  @override
  void onClick(String? action) {
    // TODO: implement onClick
    print(action);
    switch (action) {
      case actionOpenHomeEditPicker:
        showEditHomePickerSheet(context);
        break;
      case ACTION_MAYBE_LATER:
        GeneralUtils.navigationPop(context);
        break;
      case actionEditHome:
        GeneralUtils.navigationPushReplacement(context, Routes.homeWidget,
            routeName: Routes.home);
        break;
      case actionAddMenu:
        addMenuPickerSheet(context);
        break;
      case actionBatchcookingMenu:
        GeneralUtils.navigationPop(context);
        // mProvide?.getRecipes(this, isBatchCooking: true);
        // GeneralUtils.navigationPushTo(
        //     context, Routes.recipieWidget(isBatchCooking: true),
        //     routeName: Routes.recipies);
        GeneralUtils.navigationPushTo(
            context, Routes.addPlanWidget(isBatchCooking: true),
            routeName: Routes.addPlan);
        break;
      case actionNormalMenu:
        GeneralUtils.navigationPop(context);
        // mProvide?.getRecipes(this, isBatchCooking: false);
        // GeneralUtils.navigationPushTo(
        //     context, Routes.recipieWidget(isBatchCooking: false),
        //     routeName: Routes.recipies);
        GeneralUtils.navigationPushTo(
            context, Routes.addPlanWidget(isBatchCooking: false),
            routeName: Routes.addPlan);

        break;
      case actionSuccessRecipesBatch:
        // addMealPickerSheet(context, isBatchCooking: true);
        break;
      case actionSuccessRecipesNormal:
        // addMealPickerSheet(context, isBatchCooking: false);
        break;

      case actionMoreOptions:
        moreOptionsPickerSheet(context);
        break;
      case actionGenrate:
        GeneralUtils.navigationPop(context);
        break;
      case actionPrintPlan:
        GeneralUtils.navigationPop(context);
        break;
      case actionEditPlan:
        GeneralUtils.navigationPop(context);
        editPlanPickerSheet(context);
        break;
      case actionCancel:
        GeneralUtils.navigationPop(context);
        break;
      case actionSaveChanges:
        GeneralUtils.navigationPop(context);
        mProvide?.updateCalendar(this, etTitle.text.trim(), etDesc.text.trim());
        break;
      case actionOpenAddRecipie:
        addRecipiePickerSheet(context);
        break;
      case actionRecipieMenu:
        recipieMenuPickerSheet(context);
        break;
      case actionAddRecipie:
        GeneralUtils.navigationPop(context);
        GeneralUtils.navigationPushTo(
            context, Routes.recipieWidget(selectedDate!, mealTypeId!),
            routeName: Routes.recipies);
        break;
      case actionAddNote:
        GeneralUtils.navigationPop(context);
        addNotesPickerSheet(context);
        break;
      case actionRecipiePrecview:
        GeneralUtils.navigationPop(context);

        // if (itemId != null &&
        // (itemType == TYPE_RECIPIE || itemType == TYPE_RECIPIE_MEAL)) {
        GeneralUtils.navigationPushTo(
            context, Routes.recipiesPreviewWidget(itemId, recipieId),
            routeName: Routes.recipiesPreview);
        // } else {
        //   //Todo:
        //   showToastDialog(context, "Only Recipie Type Details available");
        // }
        break;
      case actionAddFav:
        GeneralUtils.navigationPop(context);

        showToastDialog(context, "Api missing");
        break;
      case actionRemoveRecipie:
        GeneralUtils.navigationPop(context);
        if (itemId != null) {
          mProvide?.delItem(this, itemId);
        }
        // showToastDialog(context, "Api Error");
        break;
      case ACTION_ON_SUCCESS:
        etTitle.clear();
        etDesc.clear();
        if (mounted) setState(() {});
        break;
      case actionSaveDate:
        GeneralUtils.navigationPop(context);
        final days = endDate.difference(startDate).inDays;
        mProvide?.getCalendarItems(
            this,
            DateFormat('yyyy-MM-dd').format(startDate),
            DateFormat('yyyy-MM-dd').format(endDate),
            days);
        break;
      case actionSuccess:
        if (mounted) setState(() {});
        break;

      case actionFilterBreakfast:
        setState(() {
          GeneralUtils.navigationPop(context);
          filtersSelection[0] = !filtersSelection[0];
          if (filtersSelection.any((element) => element == true)) {
            noFiltersApplied = false;
          } else {
            noFiltersApplied = true;
          }
        });
        break;
      case actionFilterBrunch:
        setState(() {
          GeneralUtils.navigationPop(context);
          filtersSelection[1] = !filtersSelection[1];
          if (filtersSelection.any((element) => element == true)) {
            noFiltersApplied = false;
          } else {
            noFiltersApplied = true;
          }
        });
        break;
      case actionFilterLunch:
        setState(() {
          GeneralUtils.navigationPop(context);
          filtersSelection[2] = !filtersSelection[2];
          if (filtersSelection.any((element) => element == true)) {
            noFiltersApplied = false;
          } else {
            noFiltersApplied = true;
          }
        });
        break;
      case actionFilterSnacks:
        setState(() {
          GeneralUtils.navigationPop(context);
          filtersSelection[3] = !filtersSelection[3];
          if (filtersSelection.any((element) => element == true)) {
            noFiltersApplied = false;
          } else {
            noFiltersApplied = true;
          }
        });
        break;
      case actionFilterDinner:
        setState(() {
          GeneralUtils.navigationPop(context);
          filtersSelection[4] = !filtersSelection[4];
          if (filtersSelection.any((element) => element == true)) {
            noFiltersApplied = false;
          } else {
            noFiltersApplied = true;
          }
        });
        break;
      case actionAddNoteDone:
        GeneralUtils.navigationPop(context);
        mProvide?.addNote(this, selectedDate!, mealTypeId!,
            etEcternalLink.text.trim(), etNoteDesc.text.trim());
        break;
      case actionSuccessAddNote:
        GeneralUtils.navigationPushReplacement(context, Routes.planWidget,
            routeName: Routes.plan);
        break;
      case actionSuccessDelItem:
        GeneralUtils.navigationPushReplacement(context, Routes.planWidget,
            routeName: Routes.plan);
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    setListeners();
    spUtil.putInt(keyBottomSelected, 1);
    mProvide?.getMealTypes(this);
    mProvide?.getCalendar(this);
    if (spUtil.getBool(keyShowEditHomTutorial) ?? true) {
      spUtil.putBool(keyShowEditHomTutorial, false);
      Timer(const Duration(seconds: 1), () {
        onClick(actionOpenHomeEditPicker);
      });
    }

    final days = endDate.difference(startDate).inDays;
    mProvide?.getCalendarItems(this, DateFormat('yyyy-MM-dd').format(startDate),
        DateFormat('yyyy-MM-dd').format(endDate), days);
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    // mProvide!.isApiResponseError.addListener(() {
    //   if (mProvide!.isApiResponseError.value) {
    //     // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
    //     setState(() {
    //       if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
    //         isErrorEmail = true;
    //         strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
    //       }
    //       if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
    //         isErrorPassword = true;
    //         strErrorPassword =
    //             mProvide!.loginErrorsResponse.errors!.password![0];
    //       }
    //     });
    //     mProvide!.isApiResponseError.value = false;
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Plan Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: color_white,
      bottomNavigationBar: const BottomNavWidget(),
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Stack(
            children: [
              _buildBody(),
              Visibility(
                visible: (mProvide!.filtredCalendarItems!.isEmpty &&
                    mProvide!.isApiCalled),
                child: Align(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        stringNoData.tr,
                        style: Style_16_Reg_Placeholder,
                      ),
                      Text(
                        stringSearchAnotherDates.tr,
                        style: Style_16_Reg_Placeholder,
                      ),
                    ],
                  ),
                ),
              ),
              Consumer<PlanViewModel>(builder: (context, value, child) {
                return getWidgetScreenLoader(context, value.loading);
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      // padding: EdgeInsets.all(40.sp),
      child: Column(
        children: [
          _buildHeaderView(),
          _buildContentView(),
        ],
      ),
    );
  }

  Widget _buildHeaderView() {
    return Container(
      color: color_white,
      padding: const EdgeInsets.all(15) + const EdgeInsets.only(left: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            mProvide?.calendarRes?.calendarData?.name ?? "",
            style: Style_30_Bold_Black05,
          ),
          spacerView(context, h: 48.sp),
          Text(mProvide?.calendarRes?.calendarData?.description ?? "",
              style: Style_16_Reg_Black05),
          spacerView(context, h: 40.sp),
          Row(
            children: [
              Expanded(
                  child: getButtonPadding(
                      context, btnLblAddMenu.tr, actionAddMenu, this,
                      textStyle: Style_14_Bold_WHITE,
                      padding: EdgeInsets.only(top: 24.sp, bottom: 24.sp))),
              spacerView(context, w: 8.sp),
              getButtonPadding(
                  context, btnLblMoreOptions.tr, actionMoreOptions, this,
                  color: greyF3,
                  textStyle: Style_14_Bold_Black,
                  padding: EdgeInsets.only(
                      top: 24.sp, bottom: 24.sp, left: 64.sp, right: 64.sp))
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildContentView() {
    return Container(
      padding: EdgeInsets.only(left: 48.sp, top: 48.sp, bottom: 48.sp),
      color: greyF3,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildOptionsView(),
          spacerView(context, h: 32.sp),
          // (mProvide!.filtredCalendarItems!.isNotEmpty)
          // ?
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (int i = 0; i < mProvide!.filtredCalendarItems!.length; i++)
                  _buildTileView(i),
              ],
            ),
          )
          // : Align(
          //     alignment: Alignment.center,
          //     child: Text(
          //       stringNoData,
          //       style: Style_16_Reg_Placeholder,
          //     ),
          //   ),
        ],
      ),
    );
  }

  Widget _buildOptionsView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(
          onTap: () {
            datePickerSheet(context);
          },
          child: Container(
            padding: EdgeInsets.all(20.sp),
            decoration: const BoxDecoration(
                color: color_white,
                boxShadow: [
                  BoxShadow(color: greyE5),
                ],
                borderRadius: BorderRadius.all(Radius.circular(9999))),
            child: Row(
              children: [
                const Icon(
                  Icons.arrow_back_ios,
                  color: color_primary,
                ),
                Text(
                  "${DateFormat('yyyy-MM-dd').format(startDate)} - ${DateFormat('yyyy-MM-dd').format(endDate ?? startDate)}",
                  style: Style_16_Reg_Black05,
                ),
                const Icon(
                  Icons.arrow_forward_ios,
                  color: color_primary,
                ),
              ],
            ),
          ),
        ),
        Row(
          children: [
            InkWell(
                onTap: () {
                  filtersPickerSheet(context);
                },
                child: SvgPicture.asset(eyeIcon)),
            spacerView(context, w: 10.w),
            // InkWell(
            //     onTap: () {
            //       statsPickerSheet(context);
            //     },
            //     child: SvgPicture.asset(statsIcon)),
            // spacerView(context, w: 30.sp),
          ],
        ),
      ],
    );
  }

  Widget _buildTileView(i) {
    List<CalendarItem>? breakfast = [];
    List<CalendarItem>? brunch = [];
    List<CalendarItem>? lunch = [];
    List<CalendarItem>? snacks = [];
    List<CalendarItem>? dinner = [];
    setState(() {
      mProvide!.filtredCalendarItems![i].items?.forEach((element) {
        if (element.mealTypeId == 1) {
          breakfast.add(element);
        } else if (element.mealTypeId == 2) {
          brunch.add(element);
        } else if (element.mealTypeId == 3) {
          lunch.add(element);
        } else if (element.mealTypeId == 4) {
          snacks.add(element);
        } else if (element.mealTypeId == 5) {
          dinner.add(element);
        }
      });
    });

    return Container(
      width: MediaQuery.of(context).size.width - 150.sp,
      margin: EdgeInsets.only(right: 16.sp),
      decoration:
          BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20.sp))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width - 150.sp,
            decoration: BoxDecoration(
              color: color_black,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.sp),
                topRight: Radius.circular(20.sp),
              ),
            ),
            padding: EdgeInsets.only(left: 30.sp, top: 20.sp, bottom: 20.sp),
            child: Text(
              mProvide!.filtredCalendarItems![i].data ?? "",
              style: Style_16_Bold_White,
            ),
          ),
          if (
          // breakfast.isNotEmpty &&
          (noFiltersApplied || filtersSelection[0]))
            Container(
              color: color_white,
              padding: EdgeInsets.all(30.sp),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Text(
                            mProvide?.mealTypesRes?.type?[0].description ??
                                stringBreakfast.tr,
                            style: Style_18_Bold_Black,
                          ),
                          spacerView(context, w: 10.sp),
                          if (breakfast.isNotEmpty)
                            Text(
                              "${breakfast.length} ${stringRecipies.tr}",
                              style: Style_14_Reg_Black,
                            ),
                        ],
                      ),
                      InkWell(
                          onTap: () {
                            selectedDate =
                                mProvide!.filtredCalendarItems![i].data ?? "";
                            mealTypeId = 1;

                            onClick(actionOpenAddRecipie);
                          },
                          child: SvgPicture.asset(addIcon)),
                    ],
                  ),
                  spacerView(context, h: 16.sp),
                  breakfast.isNotEmpty
                      ? ListView.separated(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return _buildTileRecipie(breakfast[index]);
                          },
                          separatorBuilder: (context, index) {
                            return spacerView(context, h: 2.sp, color: greyF3);
                          },
                          itemCount: breakfast.length,
                        )
                      : DottedBorder(
                          strokeWidth: 1,
                          borderType: BorderType.RRect,
                          radius: Radius.circular(10.sp),
                          dashPattern: [5],
                          color: color_grey,
                          child: InkWell(
                            onTap: () {
                              selectedDate =
                                  mProvide!.filtredCalendarItems![i].data ?? "";
                              mealTypeId = 1;

                              onClick(actionOpenAddRecipie);
                            },
                            child: Container(
                              height: 100.sp,
                              width: MediaQuery.of(context).size.width - 150.sp,
                              child: Center(
                                child: Text(
                                  stringNoItemAddNew.tr,
                                ),
                              ),
                            ),
                          ),
                        )
                ],
              ),
            ),
          if (
          // brunch.isNotEmpty &&
          (noFiltersApplied || filtersSelection[1]))
            Container(
              color: color_white,
              padding: EdgeInsets.all(30.sp),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Text(
                            mProvide?.mealTypesRes?.type?[1].description ??
                                stringBrunch.tr,
                            style: Style_18_Bold_Black,
                          ),
                          spacerView(context, w: 10.sp),
                          if (brunch.isNotEmpty)
                            Text(
                              "${brunch.length} ${stringRecipies.tr}",
                              style: Style_14_Reg_Black,
                            ),
                        ],
                      ),
                      InkWell(
                          onTap: () {
                            selectedDate =
                                mProvide!.filtredCalendarItems![i].data ?? "";
                            mealTypeId = 2;
                            onClick(actionOpenAddRecipie);
                          },
                          child: SvgPicture.asset(addIcon)),
                    ],
                  ),
                  spacerView(context, h: 16.sp),
                  brunch.isNotEmpty
                      ? ListView.separated(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return _buildTileRecipie(brunch[index]);
                          },
                          separatorBuilder: (context, index) {
                            return spacerView(context, h: 2.sp, color: greyF3);
                          },
                          itemCount: brunch.length,
                        )
                      : DottedBorder(
                          strokeWidth: 1,
                          borderType: BorderType.RRect,
                          radius: Radius.circular(10.sp),
                          dashPattern: [5],
                          color: color_grey,
                          child: InkWell(
                            onTap: () {
                              selectedDate =
                                  mProvide!.filtredCalendarItems![i].data ?? "";
                              mealTypeId = 2;
                              onClick(actionOpenAddRecipie);
                            },
                            child: Container(
                              height: 100.sp,
                              width: MediaQuery.of(context).size.width - 150.sp,
                              child: Center(
                                child: Text(
                                  stringNoItemAddNew.tr,
                                ),
                              ),
                            ),
                          ),
                        )
                ],
              ),
            ),
          if (
          // lunch.isNotEmpty &&
          (noFiltersApplied || filtersSelection[2]))
            Container(
              color: color_white,
              padding: EdgeInsets.all(30.sp),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Text(
                            mProvide?.mealTypesRes?.type?[2].description ??
                                stringLunch.tr,
                            style: Style_18_Bold_Black,
                          ),
                          spacerView(context, w: 10.sp),
                          if (lunch.isNotEmpty)
                            Text(
                              "${lunch.length} ${stringRecipies.tr}",
                              style: Style_14_Reg_Black,
                            ),
                        ],
                      ),
                      InkWell(
                          onTap: () {
                            selectedDate =
                                mProvide!.filtredCalendarItems![i].data ?? "";
                            mealTypeId = 3;
                            onClick(actionOpenAddRecipie);
                          },
                          child: SvgPicture.asset(addIcon)),
                    ],
                  ),
                  spacerView(context, h: 16.sp),
                  lunch.isNotEmpty
                      ? ListView.separated(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return _buildTileRecipie(lunch[index]);
                          },
                          separatorBuilder: (context, index) {
                            return spacerView(context, h: 2.sp, color: greyF3);
                          },
                          itemCount: lunch.length,
                        )
                      : DottedBorder(
                          strokeWidth: 1,
                          borderType: BorderType.RRect,
                          radius: Radius.circular(10.sp),
                          dashPattern: [5],
                          color: color_grey,
                          child: InkWell(
                            onTap: () {
                              selectedDate =
                                  mProvide!.filtredCalendarItems![i].data ?? "";
                              mealTypeId = 3;
                              onClick(actionOpenAddRecipie);
                            },
                            child: Container(
                              height: 100.sp,
                              width: MediaQuery.of(context).size.width - 150.sp,
                              child: Center(
                                child: Text(
                                  stringNoItemAddNew.tr,
                                ),
                              ),
                            ),
                          ),
                        ),
                ],
              ),
            ),
          if (
          // snacks.isNotEmpty &&
          (noFiltersApplied || filtersSelection[3]))
            Container(
              color: color_white,
              padding: EdgeInsets.all(30.sp),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Text(
                            mProvide?.mealTypesRes?.type?[3].description ??
                                stringSnacks.tr,
                            style: Style_18_Bold_Black,
                          ),
                          spacerView(context, w: 10.sp),
                          if (snacks.isNotEmpty)
                            Text(
                              "${snacks.length} ${stringRecipies.tr}",
                              style: Style_14_Reg_Black,
                            ),
                        ],
                      ),
                      InkWell(
                          onTap: () {
                            selectedDate =
                                mProvide!.filtredCalendarItems![i].data ?? "";
                            mealTypeId = 4;
                            onClick(actionOpenAddRecipie);
                          },
                          child: SvgPicture.asset(addIcon)),
                    ],
                  ),
                  spacerView(context, h: 16.sp),
                  snacks.isNotEmpty
                      ? ListView.separated(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return _buildTileRecipie(snacks[index]);
                          },
                          separatorBuilder: (context, index) {
                            return spacerView(context, h: 2.sp, color: greyF3);
                          },
                          itemCount: snacks.length,
                        )
                      : DottedBorder(
                          strokeWidth: 1,
                          borderType: BorderType.RRect,
                          radius: Radius.circular(10.sp),
                          dashPattern: [5],
                          color: color_grey,
                          child: InkWell(
                            onTap: () {
                              selectedDate =
                                  mProvide!.filtredCalendarItems![i].data ?? "";
                              mealTypeId = 4;
                              onClick(actionOpenAddRecipie);
                            },
                            child: Container(
                              height: 100.sp,
                              width: MediaQuery.of(context).size.width - 150.sp,
                              child: Center(
                                child: Text(
                                  stringNoItemAddNew.tr,
                                ),
                              ),
                            ),
                          ),
                        ),
                ],
              ),
            ),
          if (
          // dinner.isNotEmpty &&
          (noFiltersApplied || filtersSelection[4]))
            Container(
              color: color_white,
              padding: EdgeInsets.all(30.sp),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Text(
                            mProvide?.mealTypesRes?.type?[4].description ??
                                stringDinner.tr,
                            style: Style_18_Bold_Black,
                          ),
                          spacerView(context, w: 10.sp),
                          if (dinner.isNotEmpty)
                            Text(
                              "${dinner.length} ${stringRecipies.tr}",
                              style: Style_14_Reg_Black,
                            ),
                        ],
                      ),
                      InkWell(
                          onTap: () {
                            selectedDate =
                                mProvide!.filtredCalendarItems![i].data ?? "";
                            mealTypeId = 5;
                            onClick(actionOpenAddRecipie);
                          },
                          child: SvgPicture.asset(addIcon)),
                    ],
                  ),
                  spacerView(context, h: 16.sp),
                  dinner.isNotEmpty
                      ? ListView.separated(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return _buildTileRecipie(dinner[index]);
                          },
                          separatorBuilder: (context, index) {
                            return spacerView(context, h: 2.sp, color: greyF3);
                          },
                          itemCount: dinner.length,
                        )
                      : DottedBorder(
                          strokeWidth: 1,
                          borderType: BorderType.RRect,
                          radius: Radius.circular(10.sp),
                          dashPattern: [5],
                          color: color_grey,
                          child: InkWell(
                            onTap: () {
                              selectedDate =
                                  mProvide!.filtredCalendarItems![i].data ?? "";
                              mealTypeId = 5;
                              onClick(actionOpenAddRecipie);
                            },
                            child: Container(
                              height: 100.sp,
                              width: MediaQuery.of(context).size.width - 150.sp,
                              child: Center(
                                child: Text(
                                  stringNoItemAddNew.tr,
                                ),
                              ),
                            ),
                          ),
                        ),
                ],
              ),
            ),
          Container(
            width: MediaQuery.of(context).size.width - 150.sp,
            height: 20.sp,
            decoration: BoxDecoration(
              color: color_white,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20.sp),
                bottomRight: Radius.circular(20.sp),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTileRecipie(CalendarItem item) {
    return InkWell(
      onTap: () {
        itemId = null;
        itemType = null;
        recipieId = null;
        // if (item.type == TYPE_RECIPIE || item.type == TYPE_RECIPIE_MEAL) {
        itemId = item.id;
        itemType = item.type;
        recipieId = item.data?.id;
        // }
        onClick(actionRecipiePrecview);
      },
      child: Container(
        padding: EdgeInsets.only(top: 20.sp, bottom: 20.sp),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(dragIcon),
                spacerView(context, w: 8.sp),
                (item.type == TYPE_RECIPIE || item.type == TYPE_RECIPIE_MEAL)
                    ? NetworkImageWidget(
                        url: (item.type == TYPE_RECIPIE ||
                                item.type == TYPE_RECIPIE_MEAL)
                            ? (item.data!.medias!.isNotEmpty)
                                ? item.data?.medias?.first.fullUrl
                                : noImgPH
                            : item.data?.link?.image ?? noImgPH,
                        height: 100.sp,
                        width: 120.sp,
                      )
                    : item.data?.link?.favIcon != null &&
                            item.data?.link?.favIcon != ""
                        ? NetworkImageWidget(
                            url: item.data?.link?.favIcon ?? noImgPH,
                            height: 100.sp,
                            width: 120.sp,
                          )
                        : Container(
                            height: 100.sp,
                            width: 120.sp,
                            color: color_primary.withOpacity(0.5),
                            child: const Center(
                              child: const Icon(
                                Icons.bookmark,
                                color: color_primary,
                              ),
                            ),
                          ),
                spacerView(context, w: 10.sp),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (item.type == TYPE_RECIPIE ||
                        item.type == TYPE_RECIPIE_MEAL)
                      Row(
                        children: [
                          const Icon(Icons.timer_outlined),
                          Text(
                            "${item.data?.defaultMinutes} ${stringMinutes.tr}",
                            style: Style_18_Reg_Black03,
                          ),
                        ],
                      ),
                    Container(
                      width: 250.sp,
                      child: Text(
                        (item.type == TYPE_RECIPIE ||
                                item.type == TYPE_RECIPIE_MEAL)
                            ? item.data?.title ?? ""
                            : item.data?.content ?? "",
                        style: Style_16_Bold_Black,
                      ),
                    ),
                  ],
                )
              ],
            ),
            // if (item.type == TYPE_RECIPIE || item.type == TYPE_RECIPIE_MEAL)
            InkWell(
              onTap: () {
                itemId = null;
                itemType = null;
                recipieId = null;
                // if (item.type == TYPE_RECIPIE || item.type == TYPE_RECIPIE_MEAL) {
                itemId = item.id;
                itemType = item.type;
                recipieId = item.data?.id;
                // }
                onClick(actionRecipieMenu);
              },
              child: SvgPicture.asset(menuIcon),
            )
          ],
        ),
      ),
    );
  }

  void showEditHomePickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Container(
              padding: EdgeInsets.all(DeviceUtils.getScaledX(context, 20, 20)),
              decoration: const BoxDecoration(
                color: color_white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
              ),
              child: Wrap(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: DeviceUtils.getScaledWidth(context, 40, 40),
                        height: DeviceUtils.getScaledHeight(context, 5, 5),
                        decoration: const BoxDecoration(
                          color: color_grey,
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                        ),
                      ),
                      spacerView(context,
                          h: DeviceUtils.getScaledHeight(context, 15, 15)),
                      Text(
                        string_edit_home_info.tr,
                        style: Style_24_Bold_Dark_Blue,
                        textAlign: TextAlign.center,
                      ),
                      Container(
                        margin: EdgeInsets.all(
                            DeviceUtils.getScaledX(context, 7, 7)),
                        child: Wrap(
                          children: const <Widget>[],
                        ),
                      ),
                      spacerView(context,
                          h: DeviceUtils.getScaledHeight(context, 15, 15)),
                      SvgPicture.asset(
                        image_edit_home_info,
                        width: DeviceUtils.getScaledWidth(context, 190, 190),
                        height: DeviceUtils.getScaledHeight(context, 150, 150),
                      ),
                      spacerView(context,
                          h: DeviceUtils.getScaledHeight(context, 25, 25)),
                      RichText(
                        text: TextSpan(children: <TextSpan>[
                          TextSpan(
                            text: stringBetterExperience.tr,
                            style: Style_18_Reg_Black,
                          ),
                          TextSpan(
                              text: stringEdithouseData.tr,
                              style: Style_18_Bold_Black),
                          TextSpan(text: "You can ", style: Style_18_Reg_Black),
                          TextSpan(
                              text: stringConfigureMem.tr,
                              style: Style_18_Bold_Black),
                          TextSpan(
                              text: stringThatLives.tr,
                              style: Style_18_Reg_Black),
                          TextSpan(
                              text: stringAllergies.tr,
                              style: Style_18_Bold_Black),
                          TextSpan(
                              text: stringEachOne.tr,
                              style: Style_18_Reg_Black),
                          TextSpan(
                              text: stringTastes.tr,
                              style: Style_18_Bold_Black),
                          TextSpan(text: ", the ", style: Style_18_Reg_Black),
                          TextSpan(
                              text: stringEquipment.tr,
                              style: Style_18_Bold_Black),
                          TextSpan(
                              text: stringYouHaveManyOtherthings.tr,
                              style: Style_18_Reg_Black),
                        ]),
                      ),
                      spacerView(context,
                          h: DeviceUtils.getScaledHeight(context, 30, 30)),
                      getWidgetNonButton(
                        context,
                        string_edit_home_now.tr,
                        actionEditHome,
                        this,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context,
                          h: DeviceUtils.getScaledHeight(context, 10, 10)),
                      getWidgetNonButton(
                        context,
                        string_maybe_later.tr,
                        ACTION_MAYBE_LATER,
                        this,
                        isFilled: false,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context,
                          h: DeviceUtils.getScaledHeight(context, 20, 20)),
                    ],
                  )
                ],
              ));
        });
  }

  void addMenuPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Container(
              padding: EdgeInsets.all(40.sp) +
                  EdgeInsets.only(left: 56.sp, right: 56.sp),
              decoration: const BoxDecoration(
                color: color_white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
              ),
              child: Wrap(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: DeviceUtils.getScaledWidth(context, 40, 40),
                        height: DeviceUtils.getScaledHeight(context, 5, 5),
                        decoration: const BoxDecoration(
                          color: color_grey,
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                        ),
                      ),
                      spacerView(context, h: 197.sp),
                      Text(
                        stringWhatDoYouWantToAdd.tr,
                        style: Style_24_Bold_Dark_Blue,
                        textAlign: TextAlign.center,
                      ),
                      spacerView(context, h: 66.sp),
                      getWidgetNonButton(
                        context,
                        btnLblBatchcookingMenu.tr,
                        actionBatchcookingMenu,
                        this,
                        isFilled: false,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context,
                          h: DeviceUtils.getScaledHeight(context, 10, 10)),
                      getWidgetNonButton(
                        context,
                        btnLblNormalMenu.tr,
                        actionNormalMenu,
                        this,
                        isFilled: false,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context, h: 233.sp),
                    ],
                  )
                ],
              ));
        });
  }

  void moreOptionsPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Container(
              padding: EdgeInsets.all(40.sp) +
                  EdgeInsets.only(left: 56.sp, right: 56.sp),
              decoration: const BoxDecoration(
                color: color_white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
              ),
              child: Wrap(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: DeviceUtils.getScaledWidth(context, 40, 40),
                        height: DeviceUtils.getScaledHeight(context, 5, 5),
                        decoration: const BoxDecoration(
                          color: color_grey,
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                        ),
                      ),
                      spacerView(context, h: 224.sp),
                      Text(
                        stringGenralOptions.tr,
                        style: Style_24_Bold_Dark_Blue,
                        textAlign: TextAlign.center,
                      ),
                      spacerView(context, h: 66.sp),
                      // getWidgetNonButton(
                      //   context,
                      //   btnLblGenrateShoppingList,
                      //   actionGenrate,
                      //   this,
                      //   isFilled: false,
                      //   height: DeviceUtils.getScaledHeight(context, 50, 50),
                      //   radius: BorderRadius.circular(30),
                      // ),
                      // spacerView(context,
                      //     h: DeviceUtils.getScaledHeight(context, 10, 10)),
                      // getWidgetNonButton(
                      //   context,
                      //   btnLblPrintPlan,
                      //   actionPrintPlan,
                      //   this,
                      //   isFilled: false,
                      //   height: DeviceUtils.getScaledHeight(context, 50, 50),
                      //   radius: BorderRadius.circular(30),
                      // ),
                      // spacerView(context,
                      //     h: DeviceUtils.getScaledHeight(context, 10, 10)),
                      getWidgetNonButton(
                        context,
                        btnLblEditPlanInfo.tr,
                        actionEditPlan,
                        this,
                        isFilled: false,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context, h: 260.sp),
                    ],
                  )
                ],
              ));
        });
  }

  void editPlanPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Container(
              padding: EdgeInsets.all(40.sp),
              decoration: const BoxDecoration(
                color: color_white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
              ),
              child: Wrap(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          width: DeviceUtils.getScaledWidth(context, 40, 40),
                          height: DeviceUtils.getScaledHeight(context, 5, 5),
                          decoration: const BoxDecoration(
                            color: color_grey,
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                          ),
                        ),
                      ),
                      spacerView(context, h: 169.sp),
                      Text(
                        stringEditPlanInfo.tr,
                        style: Style_24_Bold_Dark_Blue,
                        textAlign: TextAlign.center,
                      ),
                      spacerView(context, h: 64.sp),
                      getWidgetTextField(etTitle, stringPlanTitle.tr,
                          string_enter_plan_title.tr, false, "",
                          presenter: this,
                          action: ACTION_ON_PLAN_TITLE_CHANGE,
                          isMarginTop: false,
                          lblStyle: Style_18_Bold_Black,
                          margin: EdgeInsets.zero,
                          lblMargin: EdgeInsets.only(bottom: 24.sp),
                          radius: BorderRadius.all(Radius.circular(8.sp)),
                          border: Border.all(
                            color: greyE5,
                          )),
                      spacerView(context, h: 65.sp),
                      getWidgetTextField(etDesc, stringPlanDesc.tr,
                          string_enter_plan_desc.tr, false, "",
                          presenter: this,
                          action: ACTION_ON_PLAN_DESC_CHANGE,
                          isMarginTop: false,
                          lblStyle: Style_18_Bold_Black,
                          margin: EdgeInsets.zero,
                          lblMargin: EdgeInsets.only(bottom: 24.sp),
                          radius: BorderRadius.all(Radius.circular(8.sp)),
                          border: Border.all(
                            color: greyE5,
                          )),
                      spacerView(context, h: 130.sp),
                      Row(
                        children: [
                          getButtonPadding(
                            context,
                            btnLblCancel.tr,
                            actionCancel,
                            this,
                            color: greyF3,
                            textStyle: Style_14_Bold_Black,
                            padding: EdgeInsets.only(
                                top: 32.sp,
                                bottom: 32.sp,
                                left: 48.sp,
                                right: 48.sp),
                          ),
                          spacerView(context, w: 20.sp),
                          Expanded(
                            child: getButtonPadding(
                              context,
                              btnLblSaveChanges.tr,
                              actionSaveChanges,
                              this,
                              textStyle: Style_14_Bold_WHITE,
                              padding: EdgeInsets.only(
                                top: 32.sp,
                                bottom: 32.sp,
                              ),
                            ),
                          ),
                        ],
                      ),
                      spacerView(context, h: 207.sp),
                    ],
                  )
                ],
              ));
        });
  }

  void statsPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Container(
              padding: EdgeInsets.all(40.sp),
              decoration: const BoxDecoration(
                color: color_white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
              ),
              child: Wrap(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          width: DeviceUtils.getScaledWidth(context, 40, 40),
                          height: DeviceUtils.getScaledHeight(context, 5, 5),
                          decoration: const BoxDecoration(
                            color: color_grey,
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                          ),
                        ),
                      ),
                      spacerView(context, h: 105.sp),
                      Text(
                        stringComposition.tr,
                        style: Style_24_Bold_Dark_Blue,
                        textAlign: TextAlign.center,
                      ),
                      spacerView(context, h: 64.sp),
                      SvgPicture.asset(chart),
                      spacerView(context, h: 64.sp),
                      SvgPicture.asset(details),
                      spacerView(context, h: 143.sp),
                    ],
                  )
                ],
              ));
        });
  }

  void filtersPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Container(
              padding: EdgeInsets.all(40.sp) +
                  EdgeInsets.only(left: 56.sp, right: 56.sp),
              decoration: const BoxDecoration(
                color: color_white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
              ),
              child: Wrap(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: DeviceUtils.getScaledWidth(context, 40, 40),
                        height: DeviceUtils.getScaledHeight(context, 5, 5),
                        decoration: const BoxDecoration(
                          color: color_grey,
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                        ),
                      ),
                      spacerView(context, h: 127.sp),
                      Text(
                        stringFiltersHeading.tr,
                        style: Style_24_Bold_Dark_Blue,
                        textAlign: TextAlign.center,
                      ),
                      spacerView(context, h: 66.sp),
                      getWidgetNonButton(
                        context,
                        btnLblBreakfast.tr,
                        actionFilterBreakfast,
                        this,
                        isFilled: filtersSelection[0] ? true : false,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context, h: 16.sp),
                      getWidgetNonButton(
                        context,
                        btnLblMidmorning.tr,
                        actionFilterBrunch,
                        this,
                        isFilled: filtersSelection[1] ? true : false,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context, h: 16.sp),
                      getWidgetNonButton(
                        context,
                        btnLblLaunch.tr,
                        actionFilterLunch,
                        this,
                        isFilled: filtersSelection[2] ? true : false,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context, h: 16.sp),
                      getWidgetNonButton(
                        context,
                        btnLblAfternoonSnack.tr,
                        actionFilterSnacks,
                        this,
                        isFilled: filtersSelection[3] ? true : false,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context, h: 16.sp),
                      getWidgetNonButton(
                        context,
                        btnLblDinner.tr,
                        actionFilterDinner,
                        this,
                        isFilled: filtersSelection[4] ? true : false,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context, h: 165.sp),
                    ],
                  )
                ],
              ));
        });
  }

  void addRecipiePickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
                padding: EdgeInsets.all(40.sp) +
                    EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                decoration: const BoxDecoration(
                  color: color_white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                child: Wrap(
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: DeviceUtils.getScaledWidth(context, 40, 40),
                          height: DeviceUtils.getScaledHeight(context, 5, 5),
                          decoration: const BoxDecoration(
                            color: color_grey,
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                          ),
                        ),
                        spacerView(context, h: 157.sp),
                        Text(
                          stringWhatDoYouWantToAdd.tr,
                          style: Style_24_Bold_Dark_Blue,
                          textAlign: TextAlign.center,
                        ),
                        spacerView(context, h: 66.sp),
                        getWidgetNonButton(
                          context,
                          btnLblAddRecipie.tr,
                          actionAddRecipie,
                          this,
                          isFilled: true,
                          height: DeviceUtils.getScaledHeight(context, 50, 50),
                          radius: BorderRadius.circular(30),
                        ),
                        spacerView(context, h: 16.sp),
                        getWidgetNonButton(
                          context,
                          btnLblAddNote.tr,
                          actionAddNote,
                          this,
                          isFilled: false,
                          height: DeviceUtils.getScaledHeight(context, 50, 50),
                          radius: BorderRadius.circular(30),
                        ),
                        spacerView(context, h: 195.sp),
                      ],
                    )
                  ],
                ));
          });
        });
  }

  void recipieMenuPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return Container(
              padding: EdgeInsets.all(40.sp),
              decoration: const BoxDecoration(
                color: color_white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
              ),
              child: Wrap(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: DeviceUtils.getScaledWidth(context, 40, 40),
                        height: DeviceUtils.getScaledHeight(context, 5, 5),
                        decoration: const BoxDecoration(
                          color: color_grey,
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                        ),
                      ),
                      spacerView(context, h: 209.sp),
                      Text(
                        stringMealDealHeading.tr,
                        style: Style_24_Bold_Dark_Blue,
                        textAlign: TextAlign.center,
                      ),
                      spacerView(context, h: 64.sp),
                      getWidgetNonButton(
                        context,
                        itemType == "note"
                            ? btnLblNotePreview.tr
                            : btnLblRecipiePreview.tr,
                        actionRecipiePrecview,
                        this,
                        isFilled: true,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                      ),
                      spacerView(context, h: 16.sp),
                      // getWidgetNonButton(
                      //   context,
                      //   btnLblAddFav,
                      //   actionAddFav,
                      //   this,
                      //   isFilled: false,
                      //   height: DeviceUtils.getScaledHeight(context, 50, 50),
                      //   radius: BorderRadius.circular(30),
                      // ),
                      // spacerView(context, h: 92.sp),
                      getWidgetNonButton(
                        context,
                        itemType == "note"
                            ? btnLblRemoveNote.tr
                            : btnLblRemoveRecipie.tr,
                        actionRemoveRecipie,
                        this,
                        isFilled: true,
                        height: DeviceUtils.getScaledHeight(context, 50, 50),
                        radius: BorderRadius.circular(30),
                        bgColor: redF0,
                        textStyle: Style_18_Bold_Red92,
                      ),
                      spacerView(context, h: 241.sp),
                    ],
                  )
                ],
              ));
        });
  }

  // void addMealPickerSheet(context,
  //     {bool isEdit = false, int? index, required bool isBatchCooking}) {
  //   showModalBottomSheet(
  //       context: context,
  //       isScrollControlled: true,
  //       backgroundColor: Colors.transparent,
  //       builder: (BuildContext bc) {
  //         return StatefulBuilder(
  //             builder: (BuildContext context, StateSetter mystate) {
  //           return Container(
  //             padding: EdgeInsets.only(top: 30.sp, left: 48.sp, right: 48.sp),
  //             height: MediaQuery.of(context).size.height - 100.sp,
  //             decoration: BoxDecoration(
  //               color: color_white,
  //               borderRadius: BorderRadius.only(
  //                   topLeft: Radius.circular(40.sp),
  //                   topRight: Radius.circular(40.sp)),
  //             ),
  //             child: Column(
  //               crossAxisAlignment: CrossAxisAlignment.start,
  //               children: [
  //                 Align(
  //                   alignment: Alignment.center,
  //                   child: Container(
  //                     width: DeviceUtils.getScaledWidth(context, 40, 40),
  //                     height: DeviceUtils.getScaledHeight(context, 5, 5),
  //                     decoration: const BoxDecoration(
  //                       color: color_grey,
  //                       borderRadius: BorderRadius.all(Radius.circular(4)),
  //                     ),
  //                   ),
  //                 ),
  //                 spacerView(context, h: 40.sp),
  //                 getTextField(etSearch, "Search", ""),
  //                 spacerView(context, h: 64.sp),
  //                 Text(
  //                   (isBatchCooking && (mProvide!.batchCookingMenu!.isEmpty) ||
  //                           (!isBatchCooking && mProvide!.normalMenu!.isEmpty))
  //                       ? "No Meals"
  //                       : stringAddMealHeading,
  //                   style: Style_24_Bold_Black03,
  //                 ),
  //                 spacerView(context, h: 24.sp),
  //                 Expanded(
  //                   child: GridView.builder(
  //                     itemCount: isBatchCooking
  //                         ? (mProvide?.batchCookingMenu?.length ?? 0)
  //                         : (mProvide?.normalMenu?.length ?? 0),
  //                     gridDelegate:
  //                         const SliverGridDelegateWithFixedCrossAxisCount(
  //                       crossAxisCount: 2,
  //                       crossAxisSpacing: 16.0,
  //                       mainAxisSpacing: 24.0,
  //                       mainAxisExtent: 300,
  //                     ),
  //                     itemBuilder: (BuildContext context, int index) {
  //                       return Column(
  //                         children: [
  //                           NetworkImageWidget(
  //                             height: 332.sp,
  //                             width: 364.sp,
  //                             url: isBatchCooking
  //                                 ? (mProvide?.batchCookingMenu?[index]
  //                                             .medias !=
  //                                         null)
  //                                     ? (mProvide?.batchCookingMenu?[index]
  //                                             .medias?.first.fullUrl ??
  //                                         imgPH)
  //                                     : imgPH
  //                                 : (mProvide?.normalMenu?[index].medias !=
  //                                         null)
  //                                     ? (mProvide?.normalMenu?[index].medias
  //                                             ?.first.fullUrl ??
  //                                         imgPH)
  //                                     : imgPH,
  //                             radius: BorderRadius.all(
  //                               Radius.circular(28.sp),
  //                             ),
  //                           ),
  //                           spacerView(context, h: 24.sp),
  //                           Row(
  //                             children: [
  //                               NetworkImageWidget(
  //                                 url: imgPH,
  //                                 height: 64.sp,
  //                                 width: 64.sp,
  //                                 shape: BoxShape.circle,
  //                               ),
  //                               spacerView(context, w: 16.sp),
  //                               Text(
  //                                 "Comidadediez",
  //                                 style: Style_14_Reg_Black,
  //                               ),
  //                             ],
  //                           ),
  //                           spacerView(context, h: 16.sp),
  //                           Text(
  //                             "14 ideas de ensaladas únicas",
  //                             style: Style_18_Bold_Black,
  //                           ),
  //                         ],
  //                       );
  //                     },
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           );
  //         });
  //       }).whenComplete(() {});
  // }

  void datePickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
                padding: EdgeInsets.all(40.sp),
                decoration: const BoxDecoration(
                  color: color_white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: DeviceUtils.getScaledWidth(context, 40, 40),
                          height: DeviceUtils.getScaledHeight(context, 5, 5),
                          decoration: const BoxDecoration(
                            color: color_grey,
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                          ),
                        ),
                        spacerView(context, h: 92.h),
                        Text(
                          stringSelectDate.tr,
                          style: Style_24_Reg_Black03,
                        ),
                        spacerView(context, h: 32.h),
                        Container(
                          height: 500.sp,
                          child: SfDateRangePicker(
                            startRangeSelectionColor: color_primary,
                            endRangeSelectionColor: color_primary,
                            selectionColor: color_primary.withOpacity(0.5),
                            rangeSelectionColor: color_primary.withOpacity(0.5),
                            onSelectionChanged: _onSelectionChanged,
                            selectionMode: DateRangePickerSelectionMode.range,
                            initialSelectedRange: PickerDateRange(
                              startDate, endDate,
                              // DateTime.now().subtract(const Duration(days: 7)),
                              // DateTime.now(),
                            ),
                          ),
                        ),
                        spacerView(context, h: 32.h),
                        Row(
                          children: [
                            getButtonPadding(
                              context,
                              btnLblCancel.tr,
                              actionCancel,
                              this,
                              color: greyF3,
                              textStyle: Style_14_Bold_Black,
                              padding: EdgeInsets.only(
                                  top: 32.sp,
                                  bottom: 32.sp,
                                  left: 48.sp,
                                  right: 48.sp),
                            ),
                            spacerView(context, w: 20.sp),
                            Expanded(
                              child: getButtonPadding(
                                context,
                                btnLblConfirmDate.tr,
                                actionSaveDate,
                                this,
                                textStyle: Style_14_Bold_WHITE,
                                padding: EdgeInsets.only(
                                  top: 32.sp,
                                  bottom: 32.sp,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ));
          });
        }).whenComplete(() {
      setState(() {});
    });
  }

  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    print(args);
    setState(() {
      if (args.value is PickerDateRange) {
        startDate = args.value.startDate;
        endDate = args.value.endDate;
      } else if (args.value is DateTime) {
        startDate = args.value;
        endDate = args.value;
      } else if (args.value is List<DateTime>) {
        // _dateCount = args.value.length.toString();
      } else {
        // _rangeCount = args.value.length.toString();
      }
    });
  }

  void addNotesPickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
                padding: EdgeInsets.all(40.sp) +
                    EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                decoration: const BoxDecoration(
                  color: color_white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            width: DeviceUtils.getScaledWidth(context, 40, 40),
                            height: DeviceUtils.getScaledHeight(context, 5, 5),
                            decoration: const BoxDecoration(
                              color: color_grey,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                          ),
                        ),
                        spacerView(context, h: 60.h),
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            stringAddNote.tr,
                            style: Style_24_Bold_Black03,
                          ),
                        ),
                        spacerView(context, h: 32.h),
                        Row(
                          children: [
                            Text(
                              stringExternalLink.tr,
                              style: Style_18_Bold_Black,
                            ),
                            spacerView(context, w: 10),
                            Container(
                              padding: EdgeInsets.all(8.sp),
                              decoration: BoxDecoration(
                                color: greyF3,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(8.sp),
                                ),
                              ),
                              child: Text(
                                "Optional",
                                style: Style_14_Bold_Black,
                              ),
                            ),
                          ],
                        ),
                        spacerView(context, h: 12.h),
                        getTextField(etEcternalLink, "", stringLinkEg.tr,
                            presenter: this,
                            action: ACTION_ON_NAME_CHANGE,
                            isMarginTop: false,
                            keyboardType: TextInputType.url),
                        spacerView(context, h: 32.h),
                        Text(
                          stringNoteDesc.tr,
                          style: Style_18_Bold_Black,
                        ),
                        spacerView(context, h: 12.h),
                        getTextField(etNoteDesc, "", "",
                            presenter: this,
                            action: ACTION_ON_NAME_CHANGE,
                            isMarginTop: false,
                            keyboardType: TextInputType.url),
                        spacerView(context, h: 65.h),
                        Row(
                          children: [
                            getButtonPadding(
                              context,
                              btnLblCancel.tr,
                              actionCancel,
                              this,
                              color: greyF3,
                              textStyle: Style_14_Bold_Black,
                              padding: EdgeInsets.only(
                                  top: 32.sp,
                                  bottom: 32.sp,
                                  left: 48.sp,
                                  right: 48.sp),
                            ),
                            spacerView(context, w: 20.sp),
                            Expanded(
                              child: getButtonPadding(
                                context,
                                stringAddNote.tr,
                                actionAddNoteDone,
                                this,
                                textStyle: Style_14_Bold_WHITE,
                                padding: EdgeInsets.only(
                                  top: 32.sp,
                                  bottom: 32.sp,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ));
          });
        }).whenComplete(() {
      setState(() {});
    });
  }
}
