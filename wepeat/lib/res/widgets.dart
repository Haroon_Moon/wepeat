import 'package:get/get.dart';
import 'package:wepeat/res/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/view/base.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'colors.dart';
import 'images.dart';

// Widgets
Widget widgetLogo() {
  return Column(
    children: [
      SvgPicture.asset(
        image_logo,
        height: 120.sp,
        width: 400.sp,
      ),
      SizedBox(height: 36.sp),
      Text(
        string_wepeat_description.tr,
        style: Style_20_Reg_White,
        textAlign: TextAlign.center,
      )
    ],
  );
}

Widget getWidgetTextField(
  TextEditingController controller,
  String strLabel,
  String strHint,
  bool showError,
  String? strError, {
  bool isEnabled = true,
  bool isObscureText = false,
  Presenter? presenter,
  String? action,
  bool passwordVisible = false,
  bool isPasswordField = false,
  bool isMarginTop = true,
  FocusNode? focusNode,
  bool inputActionGO = false,
  TextStyle? lblStyle,
  BoxBorder? border,
  BorderRadiusGeometry? radius,
  EdgeInsetsGeometry? lblMargin,
  EdgeInsetsGeometry? margin,
}) {
  return StatefulBuilder(builder: (context, setState) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: lblMargin ??
              EdgeInsets.only(
                  left: 60.sp, right: 60.sp, top: (isMarginTop) ? 32.sp : 0.sp),
          child: Text(
            strLabel,
            style: lblStyle ?? Style_18_Bold_White,
            textAlign: TextAlign.center,
          ),
        ),
        Container(
            margin: margin ??
                EdgeInsets.only(left: 60.sp, right: 60.sp, top: 24.sp),
            decoration: BoxDecoration(border: border, borderRadius: radius),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12.sp),
              child: TextField(
                controller: controller,
                style: Style_16_Reg_Black,
                toolbarOptions: const ToolbarOptions(
                    copy: true, paste: true, cut: true, selectAll: true
                    //by default all are disabled 'false'
                    ),
                textAlign: TextAlign.start,
                enabled: isEnabled,
                focusNode: focusNode,
                textAlignVertical: TextAlignVertical.bottom,
                textInputAction:
                    (inputActionGO) ? TextInputAction.go : TextInputAction.next,
                obscureText: isPasswordField ? !passwordVisible : isObscureText,
                autocorrect: false,
                enableSuggestions: false,
                decoration: InputDecoration(
                  isDense: false,
                  helperMaxLines: 1,
                  contentPadding: EdgeInsets.only(
                      left: 32.sp, right: 32.sp, top: 32.sp, bottom: 32.sp),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  hintText: strHint,
                  // labelText: strHint,
                  hintStyle: Style_16_Reg_Placeholder,
                  suffixStyle: Style_16_Reg_Placeholder,
                  prefixStyle: Style_16_Reg_Placeholder,
                  helperStyle: Style_16_Reg_Placeholder,
                  counterStyle: Style_16_Reg_Placeholder,
                  filled: true,
                  fillColor: color_white,
                  // errorText: showError ? strError : null,
                  // errorStyle: TextStyle(decoration: TextDecoration.none, color: color_red),
                  suffixIcon: isPasswordField
                      ? IconButton(
                          icon: Icon(
                            // Based on passwordVisible state choose the icon
                            passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: color_black,
                          ),
                          onPressed: () {
                            presenter!.onClick(ACTION_PASSWORD_VISIBILITY);
                          },
                        )
                      : null,
                ),
                onChanged: (text) {
                  if (presenter != null) {
                    presenter.onClick(action);
                  }
                  print("TextField $strHint: " + text);
                },
                onSubmitted: (value) {
                  if (inputActionGO) presenter!.onClick(ACTION_ON_GO);
                },
              ),
            )),
        if (showError)
          Container(
            margin: EdgeInsets.only(left: 82.sp, right: 82.sp, top: 10.sp),
            child: Text(
              strError!,
              style: Style_12_Bold_Red,
            ),
          )
      ],
    );
  });
}

Widget getTextField(
  TextEditingController controller,
  String strLabel,
  String strHint, {
  bool isEnabled = true,
  bool isObscureText = false,
  Presenter? presenter,
  String? action,
  bool passwordVisible = false,
  bool isPasswordField = false,
  bool isMarginTop = true,
  FocusNode? focusNode,
  bool inputActionGO = false,
  TextInputType? keyboardType,
}) {
  return StatefulBuilder(builder: (context, setState) {
    return Container(
        // margin: EdgeInsets.only(left: 60.sp, right: 60.sp, top: 24.sp),
        decoration: BoxDecoration(
            border: Border.all(color: color_grey),
            borderRadius: BorderRadius.all(Radius.circular(8.sp))),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(12.sp),
          child: TextField(
            controller: controller,
            style: Style_16_Reg_Black,
            toolbarOptions: const ToolbarOptions(
                copy: true, paste: true, cut: true, selectAll: true
                //by default all are disabled 'false'
                ),
            textAlign: TextAlign.start,
            enabled: isEnabled,
            focusNode: focusNode,
            keyboardType: keyboardType ?? TextInputType.text,
            textAlignVertical: TextAlignVertical.bottom,
            textInputAction:
                (inputActionGO) ? TextInputAction.go : TextInputAction.next,
            obscureText: isPasswordField ? !passwordVisible : isObscureText,
            autocorrect: false,
            enableSuggestions: false,
            decoration: InputDecoration(
              isDense: false,
              helperMaxLines: 1,
              contentPadding: EdgeInsets.only(
                  left: 32.sp, right: 32.sp, top: 32.sp, bottom: 32.sp),
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              hintText: strHint,
              // labelText: strHint,
              hintStyle: Style_16_Reg_Placeholder,
              suffixStyle: Style_16_Reg_Placeholder,
              prefixStyle: Style_16_Reg_Placeholder,
              helperStyle: Style_16_Reg_Placeholder,
              counterStyle: Style_16_Reg_Placeholder,
              filled: true,
              fillColor: color_white,
              // errorText: showError ? strError : null,
              // errorStyle: TextStyle(decoration: TextDecoration.none, color: color_red),
              suffixIcon: isPasswordField
                  ? IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: color_black,
                      ),
                      onPressed: () {
                        presenter!.onClick(ACTION_PASSWORD_VISIBILITY);
                      },
                    )
                  : null,
            ),
            onChanged: (text) {
              if (presenter != null) {
                presenter.onClick(action);
              }
              print("TextField $strHint: " + text);
            },
            onSubmitted: (value) {
              if (inputActionGO) presenter!.onClick(ACTION_ON_GO);
            },
          ),
        ));
  });
}

Widget getWidgetButton(BuildContext context, String btnName, String action,
    Presenter presenter, double btnWidth, bool loading,
    {bool isEnable = true}) {
  List<Color> _colors = [color_dark_blue, color_dark_blue];
  List<double> _stops = [0.0, 1.0];
  return Container(
    margin: const EdgeInsets.only(left: 10, right: 10),
    child: CupertinoButton(
      onPressed:
          (loading || !isEnable) ? null : () => presenter.onClick(action),
      pressedOpacity: 0.4,
      disabledColor: color_dark_blue,
      child: Opacity(
        opacity: isEnable ? 1.0 : 0.4,
        child: Container(
          alignment: Alignment.center,
          // width: btnWidth == 200 ? MediaQuery.of(context).size.width : btnWidth,
          width: MediaQuery.of(context).size.width,
          height: 100.sp,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(60.sp),
            gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: _colors,
              stops: _stops,
            ),
          ),
          child: loading
              ? SizedBox(
                  height: 50.sp,
                  width: 50.sp,
                  child: const CircularProgressIndicator(
                    color: color_white,
                    strokeWidth: 2.0,
                  ),
                )
              : FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    btnName,
                    textAlign: TextAlign.center,
                    style: Style_18_Bold_White,
                  ),
                ),
        ),
      ),
    ),
  );
}

Widget getWidgetFlatButton(Presenter presenter, String action, Widget child) {
  final ButtonStyle flatButtonStyle = TextButton.styleFrom(
    primary: Colors.black87,
    minimumSize: Size(88, 36),
    padding: EdgeInsets.symmetric(horizontal: 16.0),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(2.0)),
    ),
  );

  return TextButton(
      style: flatButtonStyle,
      onPressed: () {
        presenter.onClick(action);
      },
      // materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      // padding: EdgeInsets.zero,
      child: child);
  // FlatButton(
  //     onPressed: () {
  //       presenter.onClick(action);
  //     },
  //     materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
  //     padding: EdgeInsets.zero,
  //     child: child);
}

Widget getWidgetBackgroundLayer(BuildContext context) {
  List<Color> _colors = [color_primary_76, color_primary];
  List<double> _stops = [0.0, 0.46];

  return Stack(
    children: [
      Image.asset(
        image_splash_bg,
        fit: BoxFit.fill,
      ),
      Align(
        alignment: Alignment.topCenter,
        child: Container(
          height: (MediaQuery.of(context).size.height),
          width: (MediaQuery.of(context).size.width),
          decoration: BoxDecoration(
              gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: _colors,
            stops: _stops,
          )),
        ),
      ),
    ],
  );
}

Widget getWidgetNonButton(
  BuildContext context,
  String btnName,
  String action,
  Presenter presenter, {
  bool isFilled = true,
  Color? borderColor,
  TextStyle? textStyle,
  double? width,
  double? height,
  BorderRadiusGeometry? radius,
  Color? bgColor,
}) {
  return Container(
    child: InkWell(
      onTap: () => presenter.onClick(action),
      child: Container(
        alignment: Alignment.center,
        // width: btnWidth == 200 ? MediaQuery.of(context).size.width : btnWidth,
        width: width ?? MediaQuery.of(context).size.width,
        height: height ?? 100.sp,
        decoration: (isFilled)
            ? BoxDecoration(
                borderRadius: radius ?? BorderRadius.circular(60.sp),
                color: (isFilled) ? bgColor ?? color_primary : color_white)
            : BoxDecoration(
                border: Border.all(color: borderColor ?? color_placeholder),
                borderRadius: radius ?? BorderRadius.circular(60.sp),
              ),
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            btnName,
            textAlign: TextAlign.center,
            style: (isFilled)
                ? textStyle ?? Style_18_Bold_White
                : textStyle ?? Style_18_Bold_Black,
          ),
        ),
      ),
    ),
  );
}

Widget getButtonPadding(
    BuildContext context, String btnName, String action, Presenter presenter,
    {bool isFilled = true,
    Color? borderColor,
    TextStyle? textStyle,
    double? width,
    EdgeInsetsGeometry? padding,
    Color? color,
    PresenterWithValue? presenterValue,
    int? value}) {
  return Container(
    child: InkWell(
      onTap: () {
        if (presenterValue != null) {
          presenterValue.onClickWithValue(action, value);
        } else {
          presenter.onClick(action);
        }
      },
      child: Container(
        alignment: Alignment.center,
        // width: btnWidth == 200 ? MediaQuery.of(context).size.width : btnWidth,
        // width: width ?? MediaQuery.of(context).size.width,
        padding: padding,
        // height: 100.sp,
        decoration: (isFilled)
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(60.sp),
                color: (isFilled) ? color ?? color_primary : color_white)
            : BoxDecoration(
                border: Border.all(color: borderColor ?? color_placeholder),
                borderRadius: BorderRadius.circular(60.sp),
              ),
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            btnName,
            textAlign: TextAlign.center,
            style: (isFilled)
                ? textStyle ?? Style_18_Bold_White
                : textStyle ?? Style_18_Bold_Black,
          ),
        ),
      ),
    ),
  );
}

Widget getWidgetScreenLoader(BuildContext context, bool loading) {
  return Visibility(
    visible: loading,
    child: Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: transparent,
      child: Align(
        alignment: Alignment.center,
        child: FittedBox(
          fit: BoxFit.none,
          child: SizedBox(
            height: 70,
            width: 70,
            child: Card(
              color: color_grey,
              child: const Padding(
                padding: EdgeInsets.all(20.0),
                child: CircularProgressIndicator(
                  color: color_primary,
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
            ),
          ),
        ),
      ),
    ),
  );
}
