import 'dart:async';
import 'dart:convert';
import 'package:wepeat/model/user_profile.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constants.dart';

//shared_preferences
class SpUtil {
  static SpUtil? _instance;
  static Future<SpUtil?> get instance async {
    return await getInstance();
  }

  static SharedPreferences? _spf;

  SpUtil._();

  Future _init() async {
    _spf = await SharedPreferences.getInstance();
  }

  static Future<SpUtil?> getInstance() async {
    if (_instance == null) {
      _instance = SpUtil._();
      await _instance!._init();
    }
    return _instance;
  }

  static bool _beforCheck() {
    if (_spf == null) {
      return true;
    }
    return false;
  }

  bool hasKey(String key) {
    Set keys = getKeys()!;
    return keys.contains(key);
  }

  Set<String>? getKeys() {
    if (_beforCheck()) return null;
    return _spf!.getKeys();
  }

  get(String key) {
    if (_beforCheck()) return null;
    return _spf!.get(key);
  }

  getString(String key) {
    if (_beforCheck()) return "";
    return _spf?.getString(key);
  }

  Future<bool>? putString(String key, String? value) {
    if (_beforCheck()) return null;
    return _spf!.setString(key, value!);
  }

  bool? getBool(String key) {
    if (_beforCheck()) return null;
    return _spf!.getBool(key);
  }

  Future<bool>? putBool(String key, bool value) {
    if (_beforCheck()) return null;
    return _spf!.setBool(key, value);
  }

  int? getInt(String key) {
    if (_beforCheck()) return null;
    return _spf!.getInt(key);
  }

  Future<bool>? putInt(String key, int value) {
    if (_beforCheck()) return null;
    return _spf!.setInt(key, value);
  }

  double? getDouble(String key) {
    if (_beforCheck()) return null;
    return _spf!.getDouble(key);
  }

  Future<bool>? putDouble(String key, double value) {
    if (_beforCheck()) return null;
    return _spf!.setDouble(key, value);
  }

  List<String>? getStringList(String key) {
    return _spf!.getStringList(key);
  }

  Future<bool>? putStringList(String key, List<String> value) {
    if (_beforCheck()) return null;
    return _spf!.setStringList(key, value);
  }

  dynamic getDynamic(String key) {
    if (_beforCheck()) return null;
    return _spf!.get(key);
  }

  Future<bool>? remove(String key) {
    if (_beforCheck()) return null;
    return _spf!.remove(key);
  }

  Future<bool>? clear() {
    if (_beforCheck()) return null;
    return _spf!.clear();
  }

  UserProfile get user {
    var userJson = getString(KEY_USER);
    if (userJson == null) {
      return UserProfile();
    }

    return UserProfile.fromJson(json.decode(userJson));
  }

  set user(UserProfile userToSave) {
    putString(KEY_USER, json.encode(userToSave.toJson()));
  }
  //
  // ConfigData get config {
  //   var userJson = getString(KEY_CONFIG);
  //   if (userJson == null) {
  //     return ConfigData();
  //   }
  //
  //   return ConfigData.fromJson(json.decode(userJson));
  // }
  //
  // set config(ConfigData userToSave) {
  //   putString(KEY_CONFIG, json.encode(userToSave.toJson()));
  // }
}
