import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_crop/image_crop.dart';
import 'package:images_picker/images_picker.dart';
import 'package:wepeat/helper/custom_crop_file.dart';
import 'package:wepeat/res/strings.dart';

class CropImageScreen extends StatelessWidget {
  static const route = "CropImageScreen";
  void Function(File? file) onBack;
  File? myImageFile;
  double? aspectRatio;
  bool? showAspectRatioOptions;
  CropImageScreen(this.onBack, this.myImageFile, this.aspectRatio,
      this.showAspectRatioOptions);
  @override
  Widget build(BuildContext context) {
    return _CropImageContentScreen(this.onBack, this.myImageFile,
        this.aspectRatio, this.showAspectRatioOptions);
  }
}

/// View
class _CropImageContentScreen extends StatefulWidget {
  void Function(File? file) onBack;
  File? myImageFile;
  double? aspectRatio;
  bool? showAspectRatioOptions;

  _CropImageContentScreen(this.onBack, this.myImageFile, this.aspectRatio,
      this.showAspectRatioOptions);

  @override
  State<StatefulWidget> createState() {
    return _CropImageContentState();
  }
}

class _CropImageContentState extends State<_CropImageContentScreen>
    with TickerProviderStateMixin<_CropImageContentScreen> {
  final cropKey = GlobalKey<CustomCropFileState>();
  File? _file;
  File? _sample;
  File? _lastCropped;
  //List<Color> _colors = [color_yellow_light, color_yellow_dark];
  List<Color> _transparentColors = [Colors.transparent, Colors.transparent];
  List<double> _stops = [0.0, 1.0];
  bool isSquare = false;

  @override
  void initState() {
    super.initState();
    _sample = widget.myImageFile;
    _file = widget.myImageFile;
  }

  @override
  void dispose() {
    super.dispose();
    _file?.delete();
    _sample?.delete();
    // _lastCropped?.delete();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Container(
          color: Colors.black,
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
          child: _sample == null ? _buildOpeningImage() : _buildCroppingImage(),
        ),
      ),
    );
  }

  Widget _buildOpeningImage() {
    return Center(child: _buildOpenImage());
  }

  Widget _buildCroppingImage() {
    return Column(
      children: <Widget>[
        TextButton(
          child: Text(
            stringEditCrop.tr,
            //style: Style_Text_Full_Extra_Large_White,
          ),
          onPressed: () {
            // setState(() {
            //   widget.aspectRatio = 1 / 1;
            // });
          },
        ),
        Expanded(
          child: CustomCropFile.file(
            _sample!,
            key: cropKey,
            alwaysShowGrid: false,
            aspectRatio: widget.aspectRatio,
            scale: 1,
          ),
        ),
        Visibility(
          visible: widget.showAspectRatioOptions!,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(
                style: TextButton.styleFrom(
                    //backgroundColor: (isSquare)? color_transparent : color_yellow_light,
                    padding: EdgeInsets.all(2),
                    minimumSize: Size(60, 30),
                    alignment: Alignment.center),
                child: Text(
                  '4:5',
                  //style: TextStyle(fontSize: font_button_small, color: (isSquare)? color_white : color_black, fontFamily: 'NunitoRegular', letterSpacing: 1.8),
                ),
                onPressed: () {
                  setState(() {
                    isSquare = false;
                    widget.aspectRatio = 4 / 5;
                  });
                },
              ),
              TextButton(
                style: TextButton.styleFrom(
                    //backgroundColor: (isSquare)? color_yellow_light : color_transparent,
                    padding: EdgeInsets.all(2),
                    minimumSize: Size(100, 30),
                    alignment: Alignment.center),
                child: Text(
                  'SQUARE',
                  //style: TextStyle(fontSize: font_button_small, color: (isSquare)? color_black : color_white, fontFamily: 'NunitoRegular', letterSpacing: 1.8),
                ),
                onPressed: () {
                  setState(() {
                    isSquare = true;
                    widget.aspectRatio = 1 / 1;
                  });
                },
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.only(top: 0),
          alignment: AlignmentDirectional.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              TextButton(
                child: Text(
                  'Delete',
                  //style: Style_Text_Full_Extra_Large_Red,
                ),
                onPressed: () =>
                    Navigator.of(context, rootNavigator: true).pop(),
              ),
              TextButton(
                child: Text(
                  'Done',
                  //style: Style_Text_Full_Extra_Large_Yellow,
                ),
                onPressed: () => _cropImage(),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildOpenImage() {
    return TextButton(
      child: Text(
        'Open Image',
        style:
            Theme.of(context).textTheme.button!.copyWith(color: Colors.white),
      ),
      onPressed: () => _openImage(),
    );
  }

  Future<void> _openImage() async {
    List<Media>? res = await ImagesPicker.pick(
      count: 1,
      pickType: PickType.image,
      language: Language.System,
      maxSize: 9000,
    );
    final file = File(res![0].path);

    final sample = await ImageCrop.sampleImage(
      file: file,
      preferredSize: context.size!.longestSide.ceil(),
    );

    _sample?.delete();
    _file?.delete();

    setState(() {
      _sample = sample;
      _file = file;
    });
  }

  Future<void> _cropImage() async {
    final scale = cropKey.currentState!.scale;
    final area = cropKey.currentState!.area;
    if (area == null) {
      // cannot crop, widget is not setup
      return;
    }

    // scale up to use maximum possible number of pixels
    // this will sample image in higher resolution to make cropped image larger
    final sample = await ImageCrop.sampleImage(
      file: _file!,
      preferredSize: (2000 / scale).round(),
    );

    final file = await ImageCrop.cropImage(
      file: sample,
      area: area,
    );

    sample.delete();

    _lastCropped?.delete();
    _lastCropped = file;

    debugPrint("Moon:  " + '${file.path}');
    widget.onBack(_lastCropped);
  }
}
