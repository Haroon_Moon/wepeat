import 'package:wepeat/model/recipes_model.dart';

class PlannersRecipies {
  List<PlannersRecipiesData>? data;

  PlannersRecipies({this.data});

  PlannersRecipies.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <PlannersRecipiesData>[];
      json['data'].forEach((v) {
        data!.add(PlannersRecipiesData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PlannersRecipiesData {
  Recipe? recipe;
  int? recipeId;
  int? mealTypeId;
  int? day;
  int? order;

  PlannersRecipiesData(
      {this.recipe, this.recipeId, this.mealTypeId, this.day, this.order});

  PlannersRecipiesData.fromJson(Map<String, dynamic> json) {
    recipeId = json['recipe_id'];
    mealTypeId = json['meal_type_id'];
    day = json['day'];
    order = json['order'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['recipe_id'] = recipeId;
    data['meal_type_id'] = mealTypeId;
    data['day'] = day;
    data['order'] = order;
    return data;
  }
}
