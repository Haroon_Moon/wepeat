import 'dart:convert';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/widgets/bottom_nav_widget.dart';
import '../../viewmodel/billing_data_viewmodel.dart';
import '../../viewmodel/subscription_viewmodel.dart';

class AddCardScreen extends PageProvideNode<SubscriptionViewModel> {
  @override
  Widget buildContent(BuildContext context) {
    return _AddCardContentScreen(mProvider);
  }
}

/// View
class _AddCardContentScreen extends StatefulWidget {
  final SubscriptionViewModel provide;

  const _AddCardContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _AddCardContentState();
  }
}

class _AddCardContentState extends State<_AddCardContentScreen>
    with TickerProviderStateMixin<_AddCardContentScreen>
    implements Presenter {
  final controller = CardFormEditController();

  void update() => setState(() {});

  @override
  void initState() {
    controller.addListener(update);
    super.initState();
  }

  @override
  void dispose() {
    controller.removeListener(update);
    controller.dispose();
    super.dispose();
  }

  @override
  void onClick(String? action) {
    switch (action) {
      case actionSuccess:
        setState(() {});
        break;
      case actionPay:
        _handlePayPress();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Add Card Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: color_white,
      bottomNavigationBar: const BottomNavWidget(),
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Stack(
            children: [
              _buildBody(),
              Consumer<SubscriptionViewModel>(builder: (context, value, child) {
                return getWidgetScreenLoader(context, value.loading);
              }),
            ],
          ),
        ),
      ),
    );

    // ExampleScaffold(
    //   title: 'Card Form',
    //   tags: ['No Webhook'],
    //   padding: EdgeInsets.symmetric(horizontal: 16),
    //   children: [
    //     LoadingButton(
    //       onPressed:
    //           controller.details.complete == true ? _handlePayPress : null,
    //       text: 'Pay',
    //     ),
    //     Divider(),
    //     Padding(
    //       padding: EdgeInsets.all(8),
    //       child: Row(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         children: [
    //           OutlinedButton(
    //             onPressed: () => controller.focus(),
    //             child: Text('Focus'),
    //           ),
    //           SizedBox(width: 12),
    //           OutlinedButton(
    //             onPressed: () => controller.blur(),
    //             child: Text('Blur'),
    //           ),
    //         ],
    //       ),
    //     ),
    //     Divider(),
    //     SizedBox(height: 20),
    //     ResponseCard(
    //       response: controller.details.toJson().toPrettyString(),
    //     )
    //   ],
    // );
  }

  Widget _buildBody() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(40.sp),
          child: Row(
            children: [
              InkWell(
                onTap: () {
                  GeneralUtils.navigationPop(context);
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                ),
              ),
              spacerView(context, w: 32.sp),
              Text(
                stringMySubs.tr,
                style: Style_30_Bold_Black05,
              ),
            ],
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                CardFormField(
                  controller: controller,
                  countryCode: 'US',
                  enablePostalCode: false,
                  style: CardFormStyle(
                    borderColor: Colors.blueGrey,
                    textColor: Colors.black,
                    fontSize: 24,
                    placeholderColor: Colors.blue,
                  ),
                ),
                getButtonPadding(
                  context,
                  btnLblPay.tr,
                  actionPay,
                  this,
                  color: goldenCE,
                  textStyle: Style_14_Bold_WHITE,
                  padding: EdgeInsets.only(
                    top: 32.sp,
                    bottom: 32.sp,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _handlePayPress() async {
    if (!controller.details.complete) {
      return;
    }

    try {
      // 1. Gather customer billing information (ex. email)

      final billingDetails = BillingDetails(
        email: 'email@stripe.com',
        phone: '+48888000888',
        address: Address(
          city: 'Houston',
          country: 'US',
          line1: '1459  Circle Drive',
          line2: '',
          state: 'Texas',
          postalCode: '77063',
        ),
      ); // mocked data for tests

      // 2. Create payment method
      final paymentMethod =
          await Stripe.instance.createPaymentMethod(PaymentMethodParams.card(
        paymentMethodData: PaymentMethodData(
          billingDetails: billingDetails,
        ),
      ));

      // // 3. call API to create PaymentIntent
      // final paymentIntentResult = await callNoWebhookPayEndpointMethodId(
      //   useStripeSdk: true,
      //   paymentMethodId: paymentMethod.id,
      //   currency: 'usd', // mocked data
      //   items: [
      //     {'id': 'id'}
      //   ],
      // );

      // if (paymentIntentResult['error'] != null) {
      //   // Error during creating or confirming Intent
      //   ScaffoldMessenger.of(context).showSnackBar(
      //       SnackBar(content: Text('Error: ${paymentIntentResult['error']}')));
      //   return;
      // }

      // if (paymentIntentResult['clientSecret'] != null &&
      //     paymentIntentResult['requiresAction'] == null) {
      //   // Payment succedeed

      //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //       content:
      //           Text('Success!: The payment was confirmed successfully!')));
      //   return;
      // }

      // if (paymentIntentResult['clientSecret'] != null &&
      //     paymentIntentResult['requiresAction'] == true) {
      //   // 4. if payment requires action calling handleNextAction
      //   final paymentIntent = await Stripe.instance
      //       .handleNextAction(paymentIntentResult['clientSecret']);

      //   // todo handle error
      //   /*if (cardActionError) {
      //   Alert.alert(
      //   `Error code: ${cardActionError.code}`,
      //   cardActionError.message
      //   );
      // } else*/

      //   if (paymentIntent.status == PaymentIntentsStatus.RequiresConfirmation) {
      //     // 5. Call API to confirm intent
      //     await confirmIntent(paymentIntent.id);
      //   } else {
      //     // Payment succedeed
      //     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //         content: Text('Error: ${paymentIntentResult['error']}')));
      //   }
      // }
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Error: $e')));
      rethrow;
    }
  }

  // Future<void> confirmIntent(String paymentIntentId) async {
  //   final result = await callNoWebhookPayEndpointIntentId(
  //       paymentIntentId: paymentIntentId);
  //   if (result['error'] != null) {
  //     ScaffoldMessenger.of(context)
  //         .showSnackBar(SnackBar(content: Text('Error: ${result['error']}')));
  //   } else {
  //     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //         content: Text('Success!: The payment was confirmed successfully!')));
  //   }
  // }

  // Future<Map<String, dynamic>> callNoWebhookPayEndpointIntentId({
  //   required String paymentIntentId,
  // }) async {
  //   final url = Uri.parse('$kApiUrl/charge-card-off-session');
  //   final response = await http.post(
  //     url,
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: json.encode({'paymentIntentId': paymentIntentId}),
  //   );
  //   return json.decode(response.body);
  // }

  // Future<Map<String, dynamic>> callNoWebhookPayEndpointMethodId({
  //   required bool useStripeSdk,
  //   required String paymentMethodId,
  //   required String currency,
  //   List<Map<String, dynamic>>? items,
  // }) async {
  //   final url = Uri.parse('$kApiUrl/pay-without-webhooks');
  //   final response = await http.post(
  //     url,
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: json.encode({
  //       'useStripeSdk': useStripeSdk,
  //       'paymentMethodId': paymentMethodId,
  //       'currency': currency,
  //       'items': items
  //     }),
  //   );
  //   return json.decode(response.body);
  // }
}
