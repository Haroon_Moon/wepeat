class BaseResponseModel {
  int? code;
  String? message;

  BaseResponseModel({
      this.code, 
      this.message});

  BaseResponseModel.fromJson(dynamic json) {
    code = json["code"];
    message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["code"] = code;
    map["message"] = message;
    return map;
  }

}