import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/model/tags/type_tags_model.dart';
import 'package:wepeat/model/tags_model.dart';

import '../di/repository.dart';
import '../model/recipes_model.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../model/user_profile.dart';
import '../view/base.dart';

class RecipiesViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();
  RecipesModel? recipes;
  TypeTagsModel? recipieTagsType;

  RecipiesViewModel(this._repo);

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  int noOfApiCalls = 0;
  Future<Stream?> getTagsType(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getTagsTypes("recipes").doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) async {
        //success
        recipieTagsType = TypeTagsModel.fromJson(r);

        noOfApiCalls = 0;
        for (int i = 0; i < (recipieTagsType?.tagtypes?.length ?? 0); i++) {
          await getTagsId(
              presenter, (recipieTagsType?.tagtypes?[i].id ?? -1), i);
        }
        print(recipieTagsType);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getTagsId(Presenter presenter, int id, int index) async {
    if (await checkInternet()) {
      presenter.onClick(actionLoading);
      _repo.getTagsId(id).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        // loading = false;
      }).listen((r) {
        //success
        print(r);
        TagsModel tags = TagsModel.fromJson(r);
        recipieTagsType?.tagtypes?[index].tagsData = tags;
        print("$index done");
        noOfApiCalls++;

        if (noOfApiCalls == (recipieTagsType?.tagtypes?.length ?? 0)) {
          presenter.onClick(actionStopLoading);
        } else {
          presenter.onClick(actionLoading);
        }
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getRecipes(Presenter presenter, String? search,
      {String? nextUrl}) async {
    if (await checkInternet()) {
      List<int> tags = [];
      recipieTagsType?.tagtypes?.forEach((type) {
        type.tagsData?.data?.forEach((element) {
          if (element.isSelected) {
            tags.add(element.id!);
          }
        });
      });
      _repo.getRecipes(search, nextUrl: nextUrl, tags: tags).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        // presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) async {
        //success
        RecipesModel mApiResponse = RecipesModel.fromJson(r);

        if (mApiResponse.meta?.currentPage != 1) {
          recipes?.links = mApiResponse.links;
          recipes?.meta = mApiResponse.meta;
          recipes?.recipes?.addAll(mApiResponse.recipes!);
        } else {
          recipes = mApiResponse;
        }
        presenter.onClick(actionSuccess);
        for (int i = 0; i < (recipes?.recipes?.length ?? 0); i++) {
          await getUser(presenter, recipes!.recipes![i].userId!, i);
        }
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getUser(Presenter presenter, int userId, int index) async {
    if (await checkInternet()) {
      _repo.getUser(userId).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        UserProfile userProfile = UserProfile.fromJson(r['data']);
        recipes?.recipes![index].userProfile = userProfile;
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> addRecipie(Presenter presenter, int recipieId, String date,
      int mealTypeId, String portions) async {
    if (await checkInternet()) {
      _repo
          .addRecipie(recipieId, date, mealTypeId, portions, "recipe")
          .doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);
        presenter.onClick(actionSuccessAddRecipie);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }
}
