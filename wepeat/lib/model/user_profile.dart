import 'package:wepeat/model/payments/user_subscription_model.dart';

class UserProfile {
  String? name;
  String? email;
  String? description;
  String? iconUrl;
  CustomData? customerData;
  List<UserSubscriptionModel>? subscriptionData;

  UserProfile(
      {this.name,
      this.email,
      this.iconUrl,
      this.customerData,
      this.subscriptionData});

  UserProfile.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    description = json['description'];
    iconUrl = json['icon_url'];
    customerData = json['custom_data'] != null
        ? CustomData.fromJson(json['custom_data'])
        : null;
    if (json['subscription_data'] != null) {
      subscriptionData = <UserSubscriptionModel>[];
      json['subscription_data'].forEach((v) {
        subscriptionData?.add(UserSubscriptionModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['email'] = email;
    data['description'] = description;
    data['icon_url'] = iconUrl;
    if (customerData != null) {
      data['custom_data'] = customerData!.toJson();
    }
    if (subscriptionData != null) {
      data['subscription_data'] =
          subscriptionData!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CustomData {
  String? instagramURL;
  String? twitterURL;
  String? facebookURL;
  String? youtubeURL;
  String? pinterestURL;

  CustomData(
      {this.instagramURL,
      this.twitterURL,
      this.facebookURL,
      this.youtubeURL,
      this.pinterestURL});

  CustomData.fromJson(Map<String, dynamic> json) {
    instagramURL = json['instagramURL'];
    twitterURL = json['twitterURL'];
    facebookURL = json['facebookURL'];
    youtubeURL = json['youtubeURL'];
    pinterestURL = json['pinterestURL'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['instagramURL'] = instagramURL;
    data['twitterURL'] = twitterURL;
    data['facebookURL'] = facebookURL;
    data['youtubeURL'] = youtubeURL;
    data['pinterestURL'] = pinterestURL;
    return data;
  }
}
