
import '../di/repository.dart';
import '../view/base.dart';

class SplashViewModel extends BaseViewModel {
  final GithubRepo _repo;
  SplashViewModel(this._repo);

  bool _loading = false;
  double _btnWidth = 200.0;

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  double get btnWidth => _btnWidth;

  set btnWidth(double btnWidth) {
    _btnWidth = btnWidth;
    notifyListeners();
  }

}
