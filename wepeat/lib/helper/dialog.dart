import 'dart:async';

import 'package:wepeat/res/fonts.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/view/base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Future<T?> _showAlert<T>({required BuildContext context, Widget? child}) =>
    showDialog<T>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => child!,
    );

Future<bool?> showAlert(BuildContext context, Presenter? presenter,
    {String? title,
    String? subTitle = "",
    String negativeText = btnLblCancel,
    String positiveText = stringConfirm,
    bool onlyPositive = false,
    String negativeAction = "",
    String positiveAction = "",
    IconData? icon,
    bool showIcon = false}) {
  // spInit(context);
  return _showAlert<bool>(
      context: context,
      child: Theme(
        data: ThemeData.dark(),
        child: CupertinoAlertDialog(
          title: (showIcon)
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      icon,
                      size: 50.sp,
                    ),
                    SizedBox(
                      width: 10.sp,
                    ),
                    Flexible(
                      child: Text(
                        title!,
                        style: const TextStyle(fontWeight: FontWeight.w400),
                      ),
                    )
                  ],
                )
              : Column(
                  children: [
                    Text(
                      title!,
                      style: TextStyle(fontSize: font_16),
                    ),
                    Visibility(
                        visible: subTitle!.isNotEmpty,
                        child: Column(
                          children: [
                            SizedBox(
                              height: 10.sp,
                            ),
                            Text(
                              subTitle,
                              style: Style_14_Reg_Black,
                            )
                          ],
                        ))
                  ],
                ),
          actions: _buildAlertActions(context, onlyPositive, negativeText,
              positiveText, presenter, negativeAction, positiveAction),
        ),
      ));
}

List<Widget> _buildAlertActions(
  BuildContext context,
  bool onlyPositive,
  String negativeText,
  String positiveText,
  Presenter? presenter,
  String negativeAction,
  String positiveAction,
) {
  if (onlyPositive) {
    return [
      CupertinoDialogAction(
        child: Text(
          positiveText,
          // style: TextStyle(fontSize: font_top_title, color: color_blue_dark),
        ),
        isDefaultAction: true,
        onPressed: () {
          // Navigator.pop(context, false);
          Navigator.of(context, rootNavigator: true).pop();
          if (presenter != null) presenter.onClick(positiveAction);
        },
      ),
    ];
  } else {
    return [
      CupertinoDialogAction(
        child: Text(
          negativeText,
          // style: TextStyle(fontSize: font_top_title, color: color_blue_dark),
        ),
        isDefaultAction: true,
        onPressed: () {
          Navigator.of(context, rootNavigator: true).pop();
          presenter!.onClick(negativeAction);
        },
      ),
      CupertinoDialogAction(
        child: Text(
          positiveText,
          // style: TextStyle(fontSize: font_top_title, color: color_blue_dark),
        ),
        isDefaultAction: true,
        onPressed: () {
          Navigator.of(context, rootNavigator: true).pop();
          presenter!.onClick(positiveAction);
        },
      ),
    ];
  }
}

Future<bool?> showToastDialog(BuildContext context, String title,
    {int timeSec = 2}) async {
  _showAlert<bool>(
    context: context,
    child: CupertinoAlertDialog(
      title: Text(title, style: const TextStyle(fontWeight: FontWeight.w400)),
    ),
  );
  Timer.periodic(Duration(seconds: timeSec), (time) {
    Navigator.of(context, rootNavigator: true).pop();
    time.cancel();
  });
  return null;
}

void showFloatingSnackBar(BuildContext context, String title) async {
  final snackBar = SnackBar(
    content: Text(
      title,
      style: Style_14_Reg_Black,
    ),
    // action: SnackBarAction(
    //   label: 'Undo',
    //   textColor: Colors.white,
    //   onPressed: () {},
    // ),
    behavior: SnackBarBehavior.floating,
    backgroundColor: Colors.blue,
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

Future _showLoadingDialog(BuildContext c, LoadingDialog loading,
        {bool cancelable = true}) =>
    showDialog(
        context: c,
        barrierDismissible: cancelable,
        builder: (BuildContext c) => loading);

class LoadingDialog extends CupertinoAlertDialog {
  late BuildContext parentContext;
  late BuildContext currentContext;
  late bool showing;

  show(BuildContext context) {
    parentContext = context;
    showing = true;
    _showLoadingDialog(context, this).then((_) {
      showing = false;
    });
  }

  hide() {
    if (showing) {
      Navigator.removeRoute(parentContext, ModalRoute.of(currentContext)!);
    }
  }

  @override
  Widget build(BuildContext context) {
    currentContext = context;
    return WillPopScope(
      onWillPop: () => Future.value(true),
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Center(
            child: SizedBox(
              width: 120,
              height: 120,
              child: CupertinoPopupSurface(
                child: Semantics(
                  namesRoute: true,
                  scopesRoute: true,
                  explicitChildNodes: true,
                  child: const Center(
                    child: CupertinoActivityIndicator(),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
