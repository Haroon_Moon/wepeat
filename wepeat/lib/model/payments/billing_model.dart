class BillingModel {
  Data? data;

  BillingModel({this.data});

  BillingModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  String? name;
  String? email;
  String? phone;
  String? country;
  String? state;
  String? city;
  String? address;
  String? postalCode;

  Data(
      {this.name,
      this.email,
      this.phone,
      this.country,
      this.state,
      this.city,
      this.address,
      this.postalCode});

  Data.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    country = json['country'];
    state = json['state'];
    city = json['city'];
    address = json['address'];
    postalCode = json['postal_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['country'] = this.country;
    data['state'] = this.state;
    data['city'] = this.city;
    data['address'] = this.address;
    data['postal_code'] = this.postalCode;
    return data;
  }
}
