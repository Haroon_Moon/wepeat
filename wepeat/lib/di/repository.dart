import 'package:dio/dio.dart';
import 'package:path/path.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/net_utils.dart';

class GithubService {
  // Stream googleSocialLogin(params) => post(ENDPOINT_GOOGLE_LOGIN, params);
  Stream login(params) => post(ENDPOINT_LOGIN, params);
  Stream register(params) => post(ENDPOINT_REGISTER, params);
  Stream socialLogin(params) => post(ENDPOINT_SOCIAL, params);
  Stream logout(params) => delete(ENDPOINT_Logout, params: params);
  Stream getProfile(params) => get(ENDPOINT_MY_PROFILE, params: params);
  Stream getBillingDetails(params) => get(ENDPOINT_MY_BILLING, params: params);
  Stream updateBillingDetails(params) => put(ENDPOINT_MY_BILLING, params);
  Stream updateProfile(params) => put(ENDPOINT_MY_PROFILE, params);
  Stream updatePassword(params) => put(ENDPOINT_MY_CREDENTIALS, params);
  Stream updateImage(params) => postUpload(ENDPOINT_MY_IMAGE, params);
  Stream getHome(params) => get(ENDPOINT_MY_HOME, params: params);
  Stream updateHome(params) => put(ENDPOINT_MY_HOME, params);
  Stream updateHomeTags(params) => put(ENDPOINT_MY_HOME_TAGS, params);
  Stream getHomeMembers(params) =>
      get(ENDPOINT_MY_HOME_MEMBERS, params: params);
  Stream getTags(params) => get(ENDPOINT_ALL_TAGS, params: params);
  Stream getTagsType(url, params) => get(url, params: params);
  Stream getTagsId(url, params) => get(url, params: params);
  Stream getHomeTags(params) => get(ENDPOINT_MY_HOME_TAGS, params: params);
  Stream getUserTags(url, params) => get(url, params: params);
  Stream addMember(params) => postUpload(ENDPOINT_ADD_MEMBER, params);
  Stream delMember(url, params) => delete(url, params: params);
  Stream updateMemberImage(url, params) => postUpload(url, params);
  Stream updateMember(url, params) => put(url, params);
  Stream updateMemberTags(url, params) => put(url, params);
  Stream getMealTypes(params) => get(ENDPOINT_MEAL_TYPES, params: params);
  Stream getPlanners(url, params) => get(url, params: params);
  Stream getPlanRecipies(url, params) => get(url, params: params);
  Stream getRecipes(url, params) => get(url, params: params);
  Stream getRecipeDetail(url, params) => get(url, params: params);
  Stream getRecipeSteps(url, params) => get(url, params: params);
  Stream getRecipeFoods(url, params) => get(url, params: params);
  Stream getFoodDetail(url, params) => get(url, params: params);
  Stream getMeasureDetail(url, params) => get(url, params: params);
  Stream getCalendar(params) => get(ENDPOINT_CALENDAR, params: params);
  Stream updateCalendar(params) => put(ENDPOINT_CALENDAR, params);
  Stream getCalendarItems(url, params) => get(url, params: params);
  Stream getUser(url, params) => get(url, params: params);
  Stream addPlannerMeal(params) => post(ENDPOINT_CALENDAR_ITEMS, params);

  Stream getUserSubscription(params) =>
      get(ENDPOINT_USER_SUBSCRIPTION, params: params);
  Stream getPlans(params) => get(ENDPOINT_PLANS, params: params);

  Stream getSites(params) => get(ENDPOINT_SITES, params: params);
}

class GithubRepo {
  final GithubService _remote;

  GithubRepo(this._remote);
  // BaseRequestParam baseRequestParam = BaseRequestParam();

  Stream login(String email, String password, String deviceName) {
    Map<String, dynamic> params = {};
    // params = baseRequestParam.toJson();
    params['email'] = email;
    params['password'] = password;
    params['device_name'] = deviceName;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.login(params);
  }

  Stream register(
      String name, String email, String password, String passwordConfirm) {
    Map<String, dynamic> params = {};
    // params = baseRequestParam.toJson();
    params['name'] = name;
    params['email'] = email;
    params['password'] = password;
    params['password_confirmation'] = passwordConfirm;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.register(params);
  }

  Stream socialLogin(String accessToken, String type) {
    Map<String, dynamic> params = {};

    params['provider_id'] = accessToken;
    params['provider_name'] = type;
    params['device_name'] = "Mobile App";
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.socialLogin(params);
  }

  Stream getProfile() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getProfile(params);
  }

  Stream getSites() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getSites(params);
  }

  Stream getBillingDetails() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getBillingDetails(params);
  }

  Stream updateBillingDetails(String name, String email, String phone,
      String country, String state, String city, String address, String zip) {
    Map<String, dynamic> params = {};
    params['name'] = name;
    params['email'] = email;
    params['phone'] = phone;
    params['country'] = country;
    params['state'] = state;
    params['city'] = city;
    params['address'] = address;
    params['postal_code'] = zip;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.updateBillingDetails(params);
  }

  Stream updateProfile(
    String name,
    String email,
    String insta,
    String fb,
    String twitter,
    String pinterest,
    String youtube,
  ) {
    Map<String, dynamic> params = {};
    params['name'] = name;
    params['email'] = email;
    params['instagram_url'] = insta;
    params['twitter_url'] = twitter;
    params['facebook_url'] = fb;
    params['youtube_url'] = youtube;
    params['pinterest_url'] = pinterest;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.updateProfile(params);
  }

  Stream updatePassword(String email, String password, String confirmPassword) {
    Map<String, dynamic> params = {};
    params['email'] = email;
    params['password'] = password;
    params['password_confirmation'] = confirmPassword;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.updatePassword(params);
  }

  Stream updateUserImage(List img) {
    var formData = FormData.fromMap({
      if (img.isNotEmpty) 'image': img[0],
      'site_id': spUtil.getInt(keySiteId) ?? 1,
    });

    print("MyFormData" + formData.fields.toString());
    print("MyFormData" + formData.files.toString());

    return _remote.updateImage(formData);
  }

  Stream logout() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.logout(params);
  }

  Stream getHome() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getHome(params);
  }

  Stream getHomeMembers() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getHomeMembers(params);
  }

  Stream getTags() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getTags(params);
  }

  Stream getTagsId(int id) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    String url = ENDPOINT_ALL_TAGS + "?tag_type_id=${id}";

    return _remote.getTagsId(url, params);
  }

  Stream getTagsTypes(String type) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    String url = ENDPOINT_ALL_TAG_TYPES +
        "?available_in_model%5B%5D=${type}&is_filter=0&with_tags=0";
    return _remote.getTagsType(url, params);
  }

  Stream getHomeTags() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getHomeTags(params);
  }

  Stream getUserTags(userId) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    String url = ENDPOINT_UPDATE_MEMBER + "$userId/tags";
    return _remote.getUserTags(url, params);
  }

  Stream addMember(String? name, String? birthdate, List img) {
    var formData = FormData.fromMap({
      'name': name,
      'birthdate': birthdate,
      'site_id': spUtil.getInt(keySiteId) ?? 1,
      if (img.isNotEmpty) 'image': img[0],
    });

    print("MyFormData" + formData.fields.toString());
    print("MyFormData" + formData.files.toString());

    // Map<String, dynamic> params = {};
    // params['name'] = name;
    // params['birthdate'] = birthdate;
    // params['image'] = img;
    return _remote.addMember(formData);
  }

  Stream updateMemberImage(memberId, List img) {
    var formData = FormData.fromMap({
      if (img.isNotEmpty) 'image': img[0],
      'site_id': spUtil.getInt(keySiteId) ?? 1,
    });

    print("MyFormData" + formData.fields.toString());
    print("MyFormData" + formData.files.toString());

    String url = ENDPOINT_UPDATE_MEMBER + "$memberId/image";
    return _remote.updateMemberImage(url, formData);
  }

  Stream updateMember(String? name, String? birthdate, userId, List img) {
    // var formData = FormData.fromMap({
    //   'name': name,
    //   'birthdate': birthdate,
    //  if (img.isNotEmpty) 'image': img[0],
    // });

    // print("MyFormData" + formData.fields.toString());
    // print("MyFormData" + formData.files.toString());
    Map<String, dynamic> params = {};
    params['name'] = name;
    params['birthdate'] = birthdate;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    String url = ENDPOINT_ADD_MEMBER + "/$userId";
    return _remote.updateMember(url, params);
  }

  Stream updateHome(String? name, String? birthdate, img) {
    // var formData = FormData.fromMap({
    //   'name': name,
    //   'birthdate': birthdate,
    //   'image': img,
    // });

    // print("MyFormData" + formData.fields.toString());
    // print("MyFormData" + formData.files.toString());
    Map<String, dynamic> params = {};
    params['name'] = name;
    params['description'] = birthdate;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.updateHome(params);
  }

  Stream updateHomeTags(tags) {
    Map<String, dynamic> params = {};
    params['tags'] = tags;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.updateHomeTags(params);
  }

  Stream delMember(userId) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    String url = ENDPOINT_ADD_MEMBER + "/$userId";
    return _remote.delMember(url, params);
  }

  Stream updateMemberTags(userId, tags) {
    Map<String, dynamic> params = {};
    params['tags'] = tags;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    String url = ENDPOINT_UPDATE_MEMBER + "$userId/tags";
    return _remote.updateMemberTags(url, params);
  }

  Stream getPlanners(search,
      {String? nextUrl, List<int>? tags, bool? isBatchCooking}) {
    String? url = ENDPOINT_PLANNERS;
    if (isBatchCooking ?? false) {
      url = url + "?tags[]=104";
    }

    Map<String, dynamic> params = {};
    params['q'] = search;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    if (tags != null && tags.isNotEmpty) {
      params['tags[]'] = tags;
    }
    if (nextUrl != null) {
      url = nextUrl;
    }

    return _remote.getPlanners(url, params);
  }

  Stream getPlanRecipies(int id) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;

    String url = ENDPOINT_PLANNERS + "/$id/meals";
    return _remote.getPlanRecipies(url, params);
  }

  Stream getRecipes(search, {String? nextUrl, List<int>? tags}) {
    String? url = ENDPOINT_RECIPES;

    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    params['q'] = search;
    if (tags != null && tags.isNotEmpty) {
      params['tags[]'] = tags;
    }

    if (nextUrl != null) {
      url = nextUrl;
    }

    return _remote.getRecipes(url, params);
  }

  Stream getMealTypes() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getMealTypes(params);
  }

  Stream getCalendar() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getCalendar(params);
  }

  Stream updateCalendar(String title, String desc) {
    Map<String, dynamic> params = {};
    params['name'] = title;
    params['description'] = desc;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.updateCalendar(params);
  }

  Stream getCalendarIems(startDate, endDate) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    // startDate = "2020-03-21";
    // endDate = "2022-03-21";
    String url = ENDPOINT_CALENDAR_ITEMS +
        "?date_from=${startDate}&date_to=${endDate}&type[]=recipe_meal&type[]=recipe&type[]=note";
    return _remote.getCalendarItems(url, params);
  }

  Stream getRecipieDetail(int id) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;

    String url = ENDPOINT_RECIPES + "/$id";
    return _remote.getRecipeDetail(url, params);
  }

  Stream getRecipieSteps(int id) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;

    String url = ENDPOINT_RECIPES + "/$id/steps";
    return _remote.getRecipeSteps(url, params);
  }

  Stream getRecipieFoods(int id) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;

    String url = ENDPOINT_RECIPES + "/$id/foods";
    return _remote.getRecipeFoods(url, params);
  }

  Stream getFoodDetail(int foodId) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;

    String url = ENDPOINT_FOODS + "/$foodId";
    return _remote.getFoodDetail(url, params);
  }

  Stream getMeasureDetail(int measureId) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;

    String url = ENDPOINT_MEASURES + "/$measureId";
    return _remote.getMeasureDetail(url, params);
  }

  Stream getCalenderItemDetail(int id) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;

    String url = ENDPOINT_CALENDAR_ITEMS + "/$id";
    return _remote.getRecipeDetail(url, params);
  }

  Stream getUser(userId) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    String url = ENDPOINT_USER + "$userId";
    return _remote.getUser(url, params);
  }

  Stream addPlannerMeal(
      int plannerId, String date, String portions, String type) {
    Map<String, dynamic> params = {};
    // params = baseRequestParam.toJson();
    params['planner_id'] = plannerId;
    params['type'] = type;
    params['portions'] = portions;
    params['date'] = date;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.addPlannerMeal(params);
  }

  Stream addNote(
      String date, int mealTypeID, String content, String link, String type) {
    Map<String, dynamic> params = {};

    link = link != "" ? "https://" + link : link;

    params['date'] = date;
    params['meal_type_id'] = mealTypeID;
    params['type'] = type;
    params['content'] = content;
    params['link'] = link;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;

    return _remote.addPlannerMeal(params);
  }

  Stream addRecipie(int recipieId, String date, int mealTypeID, String portions,
      String type) {
    Map<String, dynamic> params = {};
    // params = baseRequestParam.toJson();
    params['date'] = date;
    params['meal_type_id'] = mealTypeID;
    params['portions'] = portions;
    params['type'] = type;
    params['recipe_id'] = recipieId;
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;

    return _remote.addPlannerMeal(params);
  }

  Stream delCalendarItem(itemId) {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;

    String url = ENDPOINT_CALENDAR_ITEMS + "/$itemId";
    return _remote.delMember(url, params);
  }

  Stream getUserSubscription() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getUserSubscription(params);
  }

  Stream getPlans() {
    Map<String, dynamic> params = {};
    params['site_id'] = spUtil.getInt(keySiteId) ?? 1;
    return _remote.getPlans(params);
  }
}
