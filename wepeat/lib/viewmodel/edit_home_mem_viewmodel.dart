import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/transformers.dart';

import '../di/repository.dart';
import '../helper/constants.dart';
import '../helper/dialog.dart';
import '../model/home_model.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../model/tags/type_tags_model.dart';
import '../model/tags_model.dart';
import '../view/base.dart';

class EditHomeMemViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  LoginErrorsResponse loginErrorsResponse = new LoginErrorsResponse();
  Member? homeMem;
  TypeTagsModel? memberTagsType;
  TagsModel? tags;
  List<Tag> genderTags = [];
  List<Tag> memberTypeTags = [];
  List<Tag> allergyTags = [];

  EditHomeMemViewModel(this._repo);

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  Future<Stream?> getTagsType(Presenter presenter, bool isEdit) async {
    if (await checkInternet()) {
      _repo.getTagsTypes("members").doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) async {
        //success
        memberTagsType = TypeTagsModel.fromJson(r);
        for (int i = 0; i < (memberTagsType?.tagtypes?.length ?? 0); i++) {
          await getTagsId(
              presenter, (memberTagsType?.tagtypes?[i].id ?? -1), i, isEdit);
        }
        print(memberTagsType);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getTagsId(
      Presenter presenter, int id, int index, bool isEdit) async {
    if (await checkInternet()) {
      _repo.getTagsId(id).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        TagsModel tags = TagsModel.fromJson(r);
        memberTagsType?.tagtypes?[index].tagsData = tags;
        print("done");
        // genderTags.clear();
        // memberTypeTags.clear();
        // allergyTags.clear();

        // tags = TagsModel.fromJson(r);
        // tags?.data?.forEach((element) {
        //   if (element.tagTypeId == 21) {
        //     genderTags.add(element);
        //   } else if (element.tagTypeId == 22) {
        //     memberTypeTags.add(element);
        //   } else if (element.tagTypeId == 6) {
        //     allergyTags.add(element);
        //   }
        // });

        if (isEdit)
          getUserTags(presenter, homeMem?.id);
        else
          presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getTags(Presenter presenter, bool isEdit) async {
    if (await checkInternet()) {
      _repo.getTags().doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        genderTags.clear();
        memberTypeTags.clear();
        allergyTags.clear();

        tags = TagsModel.fromJson(r);
        tags?.data?.forEach((element) {
          if (element.tagTypeId == 21) {
            genderTags.add(element);
          } else if (element.tagTypeId == 22) {
            memberTypeTags.add(element);
          } else if (element.tagTypeId == 6) {
            allergyTags.add(element);
          }
        });

        if (isEdit)
          getUserTags(presenter, homeMem?.id);
        else
          presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getUserTags(Presenter presenter, userId) async {
    if (await checkInternet()) {
      loading = true;
      _repo.getUserTags(userId).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        TagsModel userTags = TagsModel.fromJson(r);
        homeMem?.tags = userTags.data;
        memberTagsType?.tagtypes?.forEach((type) {
          type.tagsData?.data?.forEach((tag) {
            if (userTags.data!.any((element) => element.id == tag.id)) {
              tag.isSelected = true;
            }
          });
        });
        // genderTags.forEach((tag) {
        //   if (userTags.data!.any((element) => element.id == tag.id)) {
        //     tag.isSelected = true;
        //   }
        // });
        // memberTypeTags.forEach((tag) {
        //   if (userTags.data!.any((element) => element.id == tag.id)) {
        //     tag.isSelected = true;
        //   }
        // });
        // allergyTags.forEach((tag) {
        //   if (userTags.data!.any((element) => element.id == tag.id)) {
        //     tag.isSelected = true;
        //   }
        // });
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          } else if (e.response?.statusCode == 404) {
            // No tags created open picker to add new tags

          }
        }
      });
    }
  }

  Future<Stream?> addMember(Presenter presenter, String? name,
      String? birthdate, img, BuildContext context) async {
    if (await checkInternet()) {
      _repo.addMember(name, birthdate, img).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        Member homeMember = Member.fromJson(r['data']);
        homeMem = homeMember;
        updateMemberTags(presenter, homeMember.id);
        // presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
            showToastDialog(context, body["message"]);
          }
        }
      });
    }
  }

  Future<Stream?> updateMemberImage(
      Presenter presenter, BuildContext context, memberId, img) async {
    if (await checkInternet()) {
      _repo.updateMemberImage(memberId, img).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        loading = false;
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
            showToastDialog(context, body["message"]);
          }
        }
      });
    }
  }

  Future<Stream?> updateMember(
      Presenter presenter, String? name, String? birthdate, userId, img) async {
    if (await checkInternet()) {
      _repo.updateMember(name, birthdate, userId, img).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        Member homeMember = Member.fromJson(r['data']);
        // homeData?.members?[homeData!.members!.indexOf(homeData!.members!
        //         .firstWhere((element) => element.id == homeMember.id))] =
        //     homeMember;

        updateMemberTags(presenter, homeMember.id);
        // presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> updateMemberTags(Presenter presenter, userId) async {
    if (await checkInternet()) {
      loading = true;
      List<int> tags = [];
      memberTagsType?.tagtypes?.forEach((type) {
        type.tagsData?.data?.forEach((element) {
          if (element.isSelected) {
            tags.add(element.id!);
          }
        });
      });
      // genderTags.forEach((element) {
      //   if (element.isSelected) {
      //     tags.add(element.id!);
      //   }
      // });
      // memberTypeTags.forEach((element) {
      //   if (element.isSelected) {
      //     tags.add(element.id!);
      //   }
      // });
      // allergyTags.forEach((element) {
      //   if (element.isSelected) {
      //     tags.add(element.id!);
      //   }
      // });
      print(tags);
      _repo.updateMemberTags(userId, tags).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        // Member homeMember = Member.fromJson(r['data']);
        // homeData?.members?.add(homeMember);

        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }
}
