import 'package:flutter/material.dart';

const MaterialColor materialColor = MaterialColor(
  0xFF000000,
  <int, Color>{
    50: color_white,
    100: color_white,
    200: color_white,
    300: color_white,
    400: color_white,
    500: color_white,
    600: color_white,
    700: color_white,
    800: color_white,
    900: color_white,
  },
);
const color_transparent_black = Color.fromRGBO(0, 000, 000, 0.4);
const color_primary = Color(0xFF33CCCC);
const color_primary_76 = Color(0x7633CCCC);
const color_black = Color(0xFF000000);
const color_white = Color(0xFFffffff);
const color_placeholder = Color(0xFFB3B4BA);
const color_dark_blue = Color(0xFF0A0C3F);
const color_background = Color(0xFFfffffe);
const color_grey = Colors.grey;
const greyF3 = Color(0xFFF3F3F5);
const greyE5 = Color(0xFFE5E5E5);
const greyC2 = Color(0xFFC2C2CF);

const transparent = Colors.transparent;
const black03 = Color(0xFF030413);
const black05 = Color(0xFF05051C);

const redF0 = Color(0xFFFFF0F0);
const red92 = Color(0xFF921010);

const goldenCE = Color(0xFFCEBC5F);
