import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wepeat/helper/dialog.dart';
import 'package:wepeat/model/home_model.dart';
import 'package:wepeat/model/tags_model.dart';

import '../di/repository.dart';
import '../helper/constants.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../model/tags/type_tags_model.dart';
import '../view/base.dart';

class HomeViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  LoginErrorsResponse loginErrorsResponse = new LoginErrorsResponse();
  bool noHomeMember = true;
  HomeModel? homeData = HomeModel();
  TypeTagsModel? homeTagsType;
  TagsModel? tags;
  List<Tag> genderTags = [];
  List<Tag> memberTypeTags = [];
  List<Tag> allergyTags = [];
  List<Tag> foodStyleTags = [];
  List<Tag> equipmentTags = [];
  List<Tag> cookingEquipmentTags = [];
  List<Tag> basicTags = [];
  List<Tag> objectiveTags = [];

  HomeViewModel(this._repo);

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  Future<Stream?> getHome(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getHome().doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        HomeModel mHomeModel = HomeModel.fromJson(r['data']);
        homeData?.name = mHomeModel.name;
        homeData?.description = mHomeModel.description;
        getHomeMembers(presenter);
        // presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getHomeMembers(Presenter presenter) async {
    loading = true;
    if (await checkInternet()) {
      _repo.getHomeMembers().doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        HomeModel homeMembers = HomeModel.fromJson(r);
        homeData?.members = homeMembers.members;
        if (homeData?.members == null) {
          noHomeMember = true;
        } else {
          noHomeMember = false;
        }
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getTagsType(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getTagsTypes("homes").doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) async {
        //success
        homeTagsType = TypeTagsModel.fromJson(r);
        for (int i = 0; i < (homeTagsType?.tagtypes?.length ?? 0); i++) {
          await getTagsId(presenter, (homeTagsType?.tagtypes?[i].id ?? -1), i);
        }
        print(homeTagsType);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getTagsId(Presenter presenter, int id, int index) async {
    if (await checkInternet()) {
      _repo.getTagsId(id).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        TagsModel tags = TagsModel.fromJson(r);
        homeTagsType?.tagtypes?[index].tagsData = tags;
        print("done");

        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getTags(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getTags().doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        genderTags.clear();
        memberTypeTags.clear();
        allergyTags.clear();
        foodStyleTags.clear();
        equipmentTags.clear();
        cookingEquipmentTags.clear();
        basicTags.clear();
        objectiveTags.clear();
        tags = TagsModel.fromJson(r);
        tags?.data?.forEach((element) {
          if (element.tagTypeId == 21) {
            genderTags.add(element);
          } else if (element.tagTypeId == 22) {
            memberTypeTags.add(element);
          } else if (element.tagTypeId == 6) {
            allergyTags.add(element);
          } else if (element.tagTypeId == 4) {
            foodStyleTags.add(element);
          } else if (element.tagTypeId == 23) {
            equipmentTags.add(element);
          } else if (element.tagTypeId == 24) {
            cookingEquipmentTags.add(element);
          } else if (element.tagTypeId == 27) {
            basicTags.add(element);
          } else if (element.tagTypeId == 39) {
            objectiveTags.add(element);
          }
        });
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> addMember(Presenter presenter, String? name,
      String? birthdate, img, BuildContext context) async {
    if (await checkInternet()) {
      _repo.addMember(name, birthdate, img).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        Member homeMember = Member.fromJson(r['data']);
        homeData?.members?.add(homeMember);
        updateMemberTags(presenter, homeMember.id);
        // presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
            showToastDialog(context, body["message"]);
          }
        }
      });
    }
  }

//Update member

  Future<Stream?> updateMemberImage(
      Presenter presenter, BuildContext context, index, memberId, img) async {
    if (await checkInternet()) {
      _repo.updateMemberImage(memberId, img).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        // Member homeMember = Member.fromJson(r['data']);
        // homeData?.members?[homeData!.members!.indexOf(homeData!.members!
        //         .firstWhere((element) => element.id == homeMember.id))] =
        //     homeMember;

        // // homeData?.members?.add(homeMember);
        // updateMemberTags(presenter, homeMember.id);
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        loading = false;
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
            showToastDialog(context, body["message"]);
          }
        }
      });
    }
  }

  Future<Stream?> updateMember(Presenter presenter, String? name,
      String? birthdate, userId, index, img) async {
    if (await checkInternet()) {
      _repo.updateMember(name, birthdate, userId, img).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        Member homeMember = Member.fromJson(r['data']);
        homeData?.members?[homeData!.members!.indexOf(homeData!.members!
                .firstWhere((element) => element.id == homeMember.id))] =
            homeMember;

        // // homeData?.members?.add(homeMember);
        updateMemberTags(presenter, homeMember.id);
        // presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> updateMemberTags(Presenter presenter, userId) async {
    if (await checkInternet()) {
      loading = true;
      List<int> tags = [];
      genderTags.forEach((element) {
        if (element.isSelected) {
          tags.add(element.id!);
        }
      });
      memberTypeTags.forEach((element) {
        if (element.isSelected) {
          tags.add(element.id!);
        }
      });
      allergyTags.forEach((element) {
        if (element.isSelected) {
          tags.add(element.id!);
        }
      });
      print(tags);
      _repo.updateMemberTags(userId, tags).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        // Member homeMember = Member.fromJson(r['data']);
        // homeData?.members?.add(homeMember);

        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> getUserTags(
      PresenterWithValue presenter, userId, index) async {
    if (await checkInternet()) {
      loading = true;
      _repo.getUserTags(userId).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        TagsModel userTags = TagsModel.fromJson(r);
        homeData?.members?[index].tags = userTags.data;
        genderTags.forEach((tag) {
          if (userTags.data!.any((element) => element.id == tag.id)) {
            tag.isSelected = true;
          }
        });
        memberTypeTags.forEach((tag) {
          if (userTags.data!.any((element) => element.id == tag.id)) {
            tag.isSelected = true;
          }
        });
        allergyTags.forEach((tag) {
          if (userTags.data!.any((element) => element.id == tag.id)) {
            tag.isSelected = true;
          }
        });

        presenter.onClickWithValue(actionOpenEditMember, index);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          } else if (e.response?.statusCode == 404) {
            // No tags created open picker to add new tags
            presenter.onClickWithValue(actionOpenEditMember, index);
          }
        }
      });
    }
  }

//Delete Member
  Future<Stream?> delMember(Presenter presenter, userId, index) async {
    loading = true;
    if (await checkInternet()) {
      _repo.delMember(userId).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        homeData?.members?.removeAt(index);
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  // Home Apis
  Future<Stream?> getHomeTags(Presenter presenter) async {
    if (await checkInternet()) {
      loading = true;
      _repo.getHomeTags().doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        TagsModel homeTags = TagsModel.fromJson(r);
        homeData?.homeTags = homeTags.data;
        homeTagsType?.tagtypes?.forEach((type) {
          type.tagsData?.data?.forEach((tag) {
            if (homeTags.data!.any((element) => element.id == tag.id)) {
              tag.isSelected = true;
            }
          });
        });
        // foodStyleTags.forEach((tag) {
        //   if (homeTags.data!.any((element) => element.id == tag.id)) {
        //     tag.isSelected = true;
        //   }
        // });
        // equipmentTags.forEach((tag) {
        //   if (homeTags.data!.any((element) => element.id == tag.id)) {
        //     tag.isSelected = true;
        //   }
        // });
        // cookingEquipmentTags.forEach((tag) {
        //   if (homeTags.data!.any((element) => element.id == tag.id)) {
        //     tag.isSelected = true;
        //   }
        // });
        // basicTags.forEach((tag) {
        //   if (homeTags.data!.any((element) => element.id == tag.id)) {
        //     tag.isSelected = true;
        //   }
        // });
        // objectiveTags.forEach((tag) {
        //   if (homeTags.data!.any((element) => element.id == tag.id)) {
        //     tag.isSelected = true;
        //   }
        // });

        presenter.onClick(actionOpenEditMember);
      }, onError: (e) {
        //error

        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          } else if (e.response?.statusCode == 404) {
            // No tags created open picker to add new tags
            presenter.onClick(actionOpenEditMember);
          }
        }
      });
    }
  }

  Future<Stream?> updateHome(
      Presenter presenter, String? name, String? birthdate, img) async {
    if (await checkInternet()) {
      loading = true;
      _repo.updateHome(name, birthdate, img).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        HomeModel newhomeData = HomeModel.fromJson(r['data']);
        homeData?.name = newhomeData.name;
        homeData?.description = newhomeData.description;

        updateHomeTags(presenter);
        // presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }

  Future<Stream?> updateHomeTags(Presenter presenter) async {
    if (await checkInternet()) {
      loading = true;
      List<int> tags = [];
      homeTagsType?.tagtypes?.forEach((type) {
        type.tagsData?.data?.forEach((element) {
          if (element.isSelected) {
            tags.add(element.id!);
          }
        });
      });
      // foodStyleTags.forEach((element) {
      //   if (element.isSelected) {
      //     tags.add(element.id!);
      //   }
      // });
      // equipmentTags.forEach((element) {
      //   if (element.isSelected) {
      //     tags.add(element.id!);
      //   }
      // });
      // cookingEquipmentTags.forEach((element) {
      //   if (element.isSelected) {
      //     tags.add(element.id!);
      //   }
      // });
      // basicTags.forEach((element) {
      //   if (element.isSelected) {
      //     tags.add(element.id!);
      //   }
      // });
      // objectiveTags.forEach((element) {
      //   if (element.isSelected) {
      //     tags.add(element.id!);
      //   }
      // });
      print(tags);
      _repo.updateHomeTags(tags).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }
}
