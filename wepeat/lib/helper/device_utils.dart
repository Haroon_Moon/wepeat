import 'package:flutter/material.dart';
import 'package:wepeat/helper/dimens.dart';
import 'package:wepeat/helper/responsive.dart';

/// Helper class for device related operations.
///
class DeviceUtils {
  ///
  /// hides the keyboard if its already open
  ///
  static hideKeyboard(BuildContext context) {
    FocusScope.of(context).unfocus();
  }

  static double getDesignWidth(BuildContext context) =>
      Responsive.isMobile(context)
          ? Dimens.design_width
          : Dimens.design_width_tablet;

  static double getDesignHeight(BuildContext context) =>
      Responsive.isMobile(context)
          ? Dimens.design_height
          : Dimens.design_height_tablet;

  static double getValueToUse(
          BuildContext context, double mobileValue, double tabletValue) =>
      Responsive.isMobile(context) ? mobileValue : tabletValue;

  static double getScaledWidth(
          BuildContext context, double designSize, double designSizeTablet) =>
      getValueToUse(context, designSize, designSizeTablet) *
      getDeviceWidth(context) /
      getDesignWidth(context);

  static double getScaledHeight(
          BuildContext context, double designSize, double designSizeTablet) =>
      getValueToUse(context, designSize, designSizeTablet) *
      getDeviceHeight(context) /
      getDesignHeight(context);

  static double getScaledX(BuildContext context, double x, double xTablet) =>
      getValueToUse(context, x, xTablet) *
      getDeviceWidth(context) /
      getDesignWidth(context);

  static double getScaledY(BuildContext context, double y, double yTablet) =>
      getValueToUse(context, y, yTablet) *
      getDeviceHeight(context) /
      getDesignHeight(context);

  // static double getDeviceWidth(BuildContext context) =>
  //     (MediaQuery.of(context).orientation == Orientation.portrait
  //             ? MediaQuery.of(context).size.width
  //             : MediaQuery.of(context).size.height);

  // static double getDeviceHeight(BuildContext context) =>
  //   (MediaQuery.of(context).orientation == Orientation.portrait
  //             ? MediaQuery.of(context).size.height
  //             : MediaQuery.of(context).size.width);

  static double getDeviceWidth(BuildContext context) =>
      (MediaQuery.of(context).size.width);

  static double getDeviceHeight(BuildContext context) =>
      (MediaQuery.of(context).size.height);
}
