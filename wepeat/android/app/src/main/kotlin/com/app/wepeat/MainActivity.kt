package com.app.wepeat

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.util.Base64
import android.util.Log
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class MainActivity: FlutterFragmentActivity() {

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        printHashKey()
    }

    fun printHashKey() {
        // Add code to print out the key hash
        // Add code to print out the key hash
        try {
            val info: PackageInfo = baseContext.getPackageManager()
                .getPackageInfo(baseContext.getPackageName(), PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.i(
                    "KeyHash",
                    "printHashKey() Hash Key: $hashKey"
                )
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("KeyHash", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e("KeyHash", "printHashKey()", e)
        }
    }
}
