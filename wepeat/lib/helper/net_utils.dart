import 'dart:async';
import 'dart:math';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart' as getx;
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:wepeat/res/strings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// import 'package:geolocator/geolocator.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:wepeat/helper/dialog.dart';
import 'package:url_launcher/url_launcher.dart';

import '../di/app_module.dart';
import 'constants.dart';

late Timer guestUserLogoutTimer;

Stream get(String url, {Map<String, dynamic>? params}) =>
    Stream.fromFuture(_get(url, params: params)).asBroadcastStream();

Stream post(String url, Map<String, dynamic> params) =>
    Stream.fromFuture(_post(url, params)).asBroadcastStream();

Stream put(String url, Map<String, dynamic> params) =>
    Stream.fromFuture(_put(url, params)).asBroadcastStream();

Stream postUpload(String url, FormData params) =>
    Stream.fromFuture(_postUpload(url, params)).asBroadcastStream();

Stream delete(String url, {Map<String, dynamic>? params}) =>
    Stream.fromFuture(_delete(url, params)).asBroadcastStream();

Future _get(String url, {Map<String, dynamic>? params}) async {
  var response = await dio.get(url, queryParameters: params);
  return response.data;
}

Future _post(String url, Map<String, dynamic> params) async {
  var response = await dio.post(url, data: params);
  return response.data;
}

Future _put(String url, Map<String, dynamic> params) async {
  var response = await dio.put(url, data: params);
  return response.data;
}

Future _postUpload(String url, FormData params) async {
  var response = await dio.post(url, data: params);
  return response.data;
}

Future _delete(String url, Map<String, dynamic>? params) async {
  var response = await dio.delete(url, data: params);
  return response.data;
}

String removeZeroFromStart(String mobileNo) {
  String number = mobileNo;
  var arr = mobileNo.split("");
  if (arr[0] == '0') {
    number = "";
    for (int i = 1; i < arr.length; i++) {
      number = number + arr[i];
    }
  }
  return number;
}

void cancelGuestUserTimer() {
  print("Guest Timer Cancel");
  guestUserLogoutTimer.cancel();
}

bool checkValidatePassword(String value) {
  String pattern =
      r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~._]).{8,}$';
  RegExp regExp = RegExp(pattern);
  return regExp.hasMatch(value);
}

bool checkValidateEmail(String email) {
  String pattern =
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
  RegExp regExp = RegExp(pattern);
  return regExp.hasMatch(email);
}

Future<void> openMap(double latitude, double longitude) async {
  String googleUrl =
      'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
  if (await canLaunch(googleUrl)) {
    await launch(googleUrl);
  } else {
    throw 'Could not open the map.';
  }
}

void logoutUser(BuildContext context) {
  Timer.periodic(const Duration(seconds: 1), (time) {
    spUtil!.remove(KEY_USER);
    spUtil!.remove(KEY_TOKEN);
    time.cancel();
  });
  GeneralUtils.navigationPushReplacement(context, Routes.loginWidget,
      routeName: Routes.login);
  // Navigator.pushAndRemoveUntil(
  //   contextGlobal,
  //   MaterialPageRoute(
  //     builder: (BuildContext context) => LoginScreen(),
  //   ),
  //       (route) => false,
  // );
}

void onErrorHandler(BuildContext context, int? code, String? errorMsg) {
  if (code == 401) {
    logoutUser(context);
  } else {
    // showErrorAlert(context, errorMsg);
  }
}

double getBottomNavHeight() {
  return 164.sp;
  // return (Platform.isIOS) ? 170.h : 120.h;
}

String dateFormatter(String date) {
  var parsedDate = DateFormat('HH:mm a, dd/MM/yyyy').parse(date);
  final DateFormat formatter = DateFormat('dd/MM/yyyy');
  final String formatted = formatter.format(parsedDate);
  return formatted;
}

String splitDate(String date) {
  List list = date.split(", ");
  return list[1];
}

// void openLocationSettings() async {
//   await Geolocator.openLocationSettings();
// }

Future<bool> isInternetConnected() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile ||
      connectivityResult == ConnectivityResult.wifi) {
    return true;
  } else {
    return false;
  }
}

void showInternetAlert(BuildContext context) {
  showAlert(context, null,
      onlyPositive: true,
      positiveText: stringOK,
      title: string_no_internet_connection.tr);
}

void showErrorAlert(BuildContext context, String? errorMsg) {
  showAlert(context, null,
      onlyPositive: true, positiveText: stringOK, title: errorMsg);
}

////////////////////// FCM Token ////////////////////////////
// FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
// FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;

// Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
//   // If you're going to use other Firebase services in the background, such as Firestore,
//   // make sure you call `initializeApp` before using other Firebase services.
//   await Firebase.initializeApp();
//   print('Handling a background message' + message.data.toString());
// }

// Future<File?> firebaseCloudMessaging_Listeners() async {
//   // if (Platform.isIOS) iOS_Permission();
//   if (Platform.isIOS) {
//     // Permissions();
//     await _firebaseMessaging.requestPermission(
//         announcement: true,
//         carPlay: true,
//         criticalAlert: true,
//         sound: true,
//         badge: true,
//         alert: true,
//         provisional: false);
//   }

//   await _firebaseMessaging.getToken().then((token) {
//     print("FCM_Token: " + token!);
//     spUtil!.putString(KEY_FCM_TOKEN, token);
//   });

//   FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

//   flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

//   /// Update the iOS foreground notification presentation options to allow
//   /// heads up notifications.
//   await _firebaseMessaging.setForegroundNotificationPresentationOptions(
//     alert: true,
//     badge: true,
//     sound: true,
//   );

//   FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
//     print("Message" + message.toString());
//     final body = json.decode(message.data["data"]);
//     sleep(const Duration(milliseconds: 500));
//     // eventBus.fire(NotifyCount(
//     //     badgeCount: body['badge_count'], messageCount: body['message_count']));
//     //
//     // if (message.data.containsKey("notification_type") &&
//     //     message.data['notification_type'] == "6") {
//     //   eventBus.fire(message.data['notification_type']);
//     // }
//     //
//     // if (showPushNotification) {
//     //   showNotification(message);
//     // }
//     // PushNotification pushNotification = PushNotification.fromJson( json.decode(message.data["data"]));
//   });

//   FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
//     print('A new onMessageOpenedApp event was published!');
//     final body = json.decode(message.data["data"]);
//     // if (body.containsKey("post_obj")) {
//     //   Posts posts = Posts.fromJson(body['post_obj']);
//     //   eventBus.fire(posts);
//     // }
//     // if (message.data.containsKey("notification_type") &&
//     //     (message.data['notification_type'] == "9" ||
//     //         message.data['notification_type'] == "11")) {
//     //   final body = json.decode(message.data["data"]);
//     //   MessageNotify messageNotify = MessageNotify.fromJson(body);
//     //   eventBus.fire(messageNotify);
//     // }
//   });
//   return null;
// }

// void showNotification(message) async {
//   var androidPlatformChannelSpecifics = const AndroidNotificationDetails(
//       "Wepeat", 'Wepeat',
//       playSound: true,
//       enableVibration: true,
//       importance: Importance.max,
//       priority: Priority.high,
//       icon: '@mipmap/ic_launcher');
//   var iOSPlatformChannelSpecifics = const IOSNotificationDetails();
//   var platformChannelSpecifics = NotificationDetails(
//       android: androidPlatformChannelSpecifics,
//       iOS: iOSPlatformChannelSpecifics);

//   RemoteNotification notification = message.notification;

//   await flutterLocalNotificationsPlugin!.show(
//     2,
//     notification.title,
//     notification.body,
//     platformChannelSpecifics,
//     payload: '@mipmap/ic_launcher',
//   );
// }

// void configLocalNotification() {
//   var initializationSettingsAndroid =
//       const AndroidInitializationSettings('@mipmap/ic_launcher');
//   var initializationSettingsIOS = const IOSInitializationSettings(
//     requestAlertPermission: true,
//     requestBadgePermission: true,
//     requestSoundPermission: true,
//   );
//   var initializationSettings = InitializationSettings(
//       android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
//   flutterLocalNotificationsPlugin!.initialize(initializationSettings);
// }

//////////////   Facebook Login   /////////////////

FacebookAuth facebookAuth = FacebookAuth.instance;

Future<String> facebookLogin(BuildContext context) async {
  LoginResult result = await facebookAuth
      .login(loginBehavior: LoginBehavior.dialogOnly, permissions: [
    'public_profile',
    'email',
    'pages_show_list',
    'pages_messaging',
    'pages_manage_metadata'
  ]);
  String token = "";

  switch (result.status) {
    case LoginStatus.success:
      // final FacebookAccessToken accessToken = result.accessToken;
      print("Facebook Response: ${result.accessToken!.token.toString()}");
      facebookLogOut();
      token = result.accessToken!.token.toString();
      break;
    case LoginStatus.cancelled:
      print('Login cancelled by the user.');
      break;
    case LoginStatus.failed:
      showToastDialog(
          context,
          'Something went wrong with the login process.\n'
          'Here\'s the error Facebook gave us: ${result.message}');
      break;
    case LoginStatus.operationInProgress:
      // TODO: Handle this case.
      break;
  }
  return token;
}

Future<void> facebookLogOut() async {
  await facebookAuth.logOut();
  print('Logged out.');
}

//////////////   Google Login   /////////////////

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
    // 'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

Future<String?> googleLogin(BuildContext context) async {
  try {
    String? token = "";
    print("Line1");
    await _googleSignIn.signIn().then((result) async {
      await result!.authentication.then((googleKey) {
        print("Google Response: " + googleKey.idToken!);
        token = googleKey.idToken;
      }).catchError((err) {
        showToastDialog(context, err);
      });
    }).catchError((err) {
      print(err);
      showToastDialog(context, err);
    });

    print("Line2");
    googleLogout();
    return token;
    // _googleSignIn.signInSilently();
    // await _googleSignIn.signIn();

  } catch (error) {
    // showToastDialog(context, error);
  }
  return null;
}

Future<void> googleLogout() => _googleSignIn.disconnect();

String parseTimeStamp(int value) {
  var date = DateTime.fromMillisecondsSinceEpoch(value * 1000);
  var d12 = DateFormat.yMMMMd('en_US').format(date);
  return d12;
}

String formatBytes(int bytes, int decimals) {
  if (bytes <= 0) return "0 B";
  const suffixes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
  var i = (log(bytes) / log(1000)).floor();
  return ((bytes / pow(1000, i)).toStringAsFixed(decimals)) + ' ' + suffixes[i];
}

spInit(BuildContext context) {
  ScreenUtil.init(
    context,
    // BoxConstraints(
    //     maxWidth: MediaQuery.of(context).size.width,
    //     maxHeight: MediaQuery.of(context).size.height),
    designSize: const Size(750, 1334),
    minTextAdapt: true,
    // orientation: Orientation.portrait,
  );
}

Widget spacerView(BuildContext context, {double? h, double? w, Color? color}) {
  return Container(
    height: h ?? 0,
    width: w ?? 0,
    color: color,
  );
}

hideKeyboard(BuildContext context) {
  FocusScopeNode currentFocus = FocusScope.of(context);

  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
}
