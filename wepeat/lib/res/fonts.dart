
// Font Sizes
double font_10 = 10;
double font_12 = 12;
double font_14 = 14;
double font_16 = 16;
double font_18 = 18;
double font_20 = 20;
double font_24 = 24;
double font_28 = 28;
double font_30 = 30;
double font_32 = 32;
double font_40 = 40;
