import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/dialog.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/widgets/bottom_nav_widget.dart';
import 'package:wepeat/widgets/network_image_widget.dart';

import '../../viewmodel/billing_data_viewmodel.dart';
import '../../viewmodel/my_data_viewmodel.dart';

class BillingDataScreen extends PageProvideNode<BillingDataViewModel> {
  @override
  Widget buildContent(BuildContext context) {
    return _BillingDataContentScreen(mProvider);
  }
}

/// View
class _BillingDataContentScreen extends StatefulWidget {
  final BillingDataViewModel provide;

  const _BillingDataContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _BillingDataContentState();
  }
}

class _BillingDataContentState extends State<_BillingDataContentScreen>
    with TickerProviderStateMixin<_BillingDataContentScreen>
    implements Presenter {
  BillingDataViewModel? mProvide;
  TextEditingController etName = TextEditingController();
  TextEditingController etEmail = TextEditingController();
  TextEditingController etPhone = TextEditingController();
  TextEditingController etCountry = TextEditingController();
  TextEditingController etState = TextEditingController();
  TextEditingController etCity = TextEditingController();
  TextEditingController etAddress = TextEditingController();
  TextEditingController etZip = TextEditingController();

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    setListeners();
    mProvide?.getBillingDetails(this);
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    // mProvide!.isApiResponseError.addListener(() {
    //   if (mProvide!.isApiResponseError.value) {
    //     // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
    //     setState(() {
    //       if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
    //         isErrorEmail = true;
    //         strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
    //       }
    //       if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
    //         isErrorPassword = true;
    //         strErrorPassword =
    //             mProvide!.loginErrorsResponse.errors!.password![0];
    //       }
    //     });
    //     mProvide!.isApiResponseError.value = false;
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Billing Data Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: color_white,
      bottomNavigationBar: const BottomNavWidget(),
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Stack(
            children: [
              _buildBody(),
              Consumer<BillingDataViewModel>(builder: (context, value, child) {
                return getWidgetScreenLoader(context, value.loading);
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(40.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              InkWell(
                onTap: () {
                  GeneralUtils.navigationPop(context);
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                ),
              ),
              spacerView(context, w: 32.sp),
              Text(
                stringBillingData.tr,
                style: Style_30_Bold_Black05,
              ),
            ],
          ),
          spacerView(context, h: 48.sp),
          Text(
            stringYourName.tr,
            style: Style_18_Bold_Black,
          ),
          spacerView(context, h: 24.sp),
          getTextField(etName, "", stringIntroduceName.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Text(
            stringYourEmail.tr,
            style: Style_18_Bold_Black,
          ),
          spacerView(context, h: 24.sp),
          getTextField(etEmail, "", stringYourEmail.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Text(
            stringPhone.tr,
            style: Style_18_Bold_Black,
          ),
          spacerView(context, h: 24.sp),
          getTextField(etPhone, "", stringInputPhone.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Text(
            stringCountry.tr,
            style: Style_18_Bold_Black,
          ),
          spacerView(context, h: 24.sp),
          getTextField(etCountry, "", stringInputCountry.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Text(
            stringState.tr,
            style: Style_18_Bold_Black,
          ),
          spacerView(context, h: 24.sp),
          getTextField(etState, "", stringInputState.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Text(
            stringCity.tr,
            style: Style_18_Bold_Black,
          ),
          spacerView(context, h: 24.sp),
          getTextField(etCity, "", stringInputCity.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Text(
            stringAddress.tr,
            style: Style_18_Bold_Black,
          ),
          spacerView(context, h: 24.sp),
          getTextField(etAddress, "", stringInputAddress.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Text(
            stringZip.tr,
            style: Style_18_Bold_Black,
          ),
          spacerView(context, h: 24.sp),
          getTextField(etZip, "", stringInputZip.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 48.sp),
          _buildBtnView(),
        ],
      ),
    );
  }

  Widget _buildBtnView() {
    return Row(
      children: [
        getButtonPadding(
          context,
          btnLblCancel.tr,
          actionCancel,
          this,
          color: greyF3,
          textStyle: Style_14_Bold_Black,
          padding: EdgeInsets.only(
              top: 32.sp, bottom: 32.sp, left: 48.sp, right: 48.sp),
        ),
        spacerView(context, w: 20.sp),
        Expanded(
          child: getButtonPadding(
            context,
            btnLblApplyChanges.tr,
            actionSaveChanges,
            this,
            textStyle: Style_14_Bold_WHITE,
            padding: EdgeInsets.only(
              top: 32.sp,
              bottom: 32.sp,
            ),
          ),
        ),
      ],
    );
  }

  @override
  void onClick(String? action) {
    switch (action) {
      case actionCancel:
        GeneralUtils.navigationPop(context);
        break;
      case actionSaveChanges:
        //Tapped Apply changes button
        hideKeyboard(context);
        if (etName.text.trim().isNotEmpty && etEmail.text.trim().isNotEmpty) {
          mProvide?.updateBillingDetails(
              this,
              etName.text.trim(),
              etEmail.text.trim(),
              etPhone.text.trim(),
              etCountry.text.trim(),
              etState.text.trim(),
              etCity.text.trim(),
              etAddress.text.trim(),
              etZip.text.trim());
        } else {
          showToastDialog(context, "Name & Email is required");
        }
        break;
      case actionSuccess:
        setState(() {
          etName.text = mProvide?.billing?.data?.name ?? "";
          etEmail.text = mProvide?.billing?.data?.email ?? "";
          etPhone.text = mProvide?.billing?.data?.phone ?? "";
          etCountry.text = mProvide?.billing?.data?.country ?? "";
          etState.text = mProvide?.billing?.data?.state ?? "";
          etCity.text = mProvide?.billing?.data?.city ?? "";
          etAddress.text = mProvide?.billing?.data?.address ?? "";
          etZip.text = mProvide?.billing?.data?.postalCode ?? "";
        });
        break;
      case actionUpdatedSuccess:
        setState(() {
          etName.text = mProvide?.billing?.data?.name ?? "";
          etEmail.text = mProvide?.billing?.data?.email ?? "";
          etPhone.text = mProvide?.billing?.data?.phone ?? "";
          etCountry.text = mProvide?.billing?.data?.country ?? "";
          etState.text = mProvide?.billing?.data?.state ?? "";
          etCity.text = mProvide?.billing?.data?.city ?? "";
          etAddress.text = mProvide?.billing?.data?.address ?? "";
          etZip.text = mProvide?.billing?.data?.postalCode ?? "";
        });
        showToastDialog(context, stringUpdated.tr);
        break;
    }
  }
}
