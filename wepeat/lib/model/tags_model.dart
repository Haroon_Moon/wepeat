class TagsModel {
  List<Tag>? data;

  TagsModel({this.data});

  TagsModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Tag>[];
      json['data'].forEach((v) {
        data!.add(Tag.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Tag {
  int? id;
  String? key;
  String? name;
  String? description;
  int? tagTypeId;
  bool isSelected = false;

  Tag({this.id, this.key, this.name, this.description, this.tagTypeId});

  Tag.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    key = json['key'];
    name = json['name'];
    description = json['description'];
    tagTypeId = json['tag_type_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['key'] = key;
    data['name'] = name;
    data['description'] = description;
    data['tag_type_id'] = tagTypeId;
    return data;
  }
}
