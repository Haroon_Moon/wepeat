class Dimens {
  Dimens._();

  //for all screens
  static const double horizontal_padding = 12.0;
  static const double vertical_padding = 12.0;

  static const double design_width = 375.0;
  static const double design_height = 812.0;

  static const double design_width_tablet = 768.0;
  static const double design_height_tablet = 1024.0;
}
