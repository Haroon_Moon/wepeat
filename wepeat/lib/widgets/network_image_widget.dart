import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';

import '../res/images.dart';

class NetworkImageWidget extends StatelessWidget {
  String? url;
  double? width;
  double? height;
  BoxFit? fit;
  BoxShape? shape;
  BoxBorder? border;
  BorderRadiusGeometry? radius;

  NetworkImageWidget({
    Key? key,
    this.url,
    this.width,
    this.height,
    this.fit = BoxFit.cover,
    this.shape = BoxShape.rectangle,
    this.border,
    this.radius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return url == null
        ? Container()
        : CachedNetworkImage(
            imageUrl: url!,
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: imageProvider,
                  fit: fit,
                ),
                shape: shape ?? BoxShape.circle,
                border: border,
                borderRadius: radius,
              ),
            ),
            width: width,
            height: height,
            fit: fit,
            placeholder: (context, url) => Container(
              decoration: BoxDecoration(
                color: color_primary.withOpacity(0.3),
                shape: shape ?? BoxShape.circle,
              ),
            ),
            errorWidget: (context, url, error) => Container(
              decoration: BoxDecoration(
                color: color_primary.withOpacity(0.3),
                shape: shape ?? BoxShape.circle,
              ),
              child: Image.asset(
                imageNoImgPH,
                width: width,
                height: height,
                fit: fit,
              ),
            ),
          );
  }
}
