import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:rxdart/transformers.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/model/payments/billing_model.dart';

import '../di/repository.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../view/base.dart';

class BillingDataViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();
  BillingModel? billing;

  BillingDataViewModel(this._repo);

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  Future<Stream?> getBillingDetails(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getBillingDetails().doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        billing = BillingModel.fromJson(r);
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> updateBillingDetails(
      Presenter presenter,
      String name,
      String email,
      String phone,
      String country,
      String state,
      String city,
      String address,
      String zip) async {
    if (await checkInternet()) {
      _repo
          .updateBillingDetails(
              name, email, phone, country, state, city, address, zip)
          .doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        billing = BillingModel.fromJson(r);
        presenter.onClick(actionUpdatedSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }
}
