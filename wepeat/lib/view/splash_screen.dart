import 'dart:async';

import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/view/login_screen.dart';
import 'package:wepeat/view/plan/plan_screen.dart';
import 'package:wepeat/viewmodel/splash_viewmodel.dart';

import 'base.dart';

class SplashScreen extends PageProvideNode<SplashViewModel> {
  @override
  Widget buildContent(BuildContext context) {
    return _SplashContentScreen(mProvider);
  }
}

/// View
class _SplashContentScreen extends StatefulWidget {
  final SplashViewModel provide;

  const _SplashContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _SplashContentState();
  }
}

class _SplashContentState extends State<_SplashContentScreen>
    with TickerProviderStateMixin<_SplashContentScreen>
    implements Presenter {
  SplashViewModel? mProvide;

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    // firebaseCloudMessaging_Listeners();
    Timer.periodic(const Duration(milliseconds: 500), (time) {
      moveNext();
      time.cancel();
    });
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  @override
  void onClick(String? action) {
    print("onClick: " + action!);
    switch (action) {
      // case ACTION_ON_SUCCESS:
      //   isConfigReceived = true;
      //   spUtil!.config = mProvide!.response.data!;
      //   moveNext();
      //   break;
    }
  }

  void moveNext() {
    if (spUtil.getString(KEY_TOKEN) != null &&
        spUtil.getString(KEY_TOKEN) != "") {
      Navigator.pushAndRemoveUntil(
        context,
        PageRouteBuilder(
            opaque: false,
            settings: const RouteSettings(name: Routes.home),
            pageBuilder: (context, animation, secondaryAnimation) =>
                PlanScreen(),
            transitionDuration: const Duration(seconds: 1),
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              return FadeTransition(
                opacity: animation,
                child: child,
              );
            }),
        (route) => false,
      );
    } else {
      Navigator.pushAndRemoveUntil(
        context,
        PageRouteBuilder(
            opaque: false,
            settings: const RouteSettings(name: Routes.login),
            pageBuilder: (context, animation, secondaryAnimation) =>
                LoginScreen(),
            transitionDuration: const Duration(seconds: 1),
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              return FadeTransition(
                opacity: animation,
                child: child,
              );
            }),
        (route) => false,
      );
    }

    //   if (mProvide?.user != null &&
    //       spUtil.user.uid != null &&
    //       spUtil.user.userType == TYPE_ADMIN) {
    //     Navigator.pushAndRemoveUntil(
    //       context,
    //       PageRouteBuilder(
    //           opaque: false,
    //           settings: const RouteSettings(name: Routes.home),
    //           pageBuilder: (context, animation, secondaryAnimation) =>
    //               HomeScreen(),
    //           transitionDuration: Duration(seconds: 1),
    //           transitionsBuilder:
    //               (context, animation, secondaryAnimation, child) {
    //             return FadeTransition(
    //               opacity: animation,
    //               child: child,
    //             );
    //           }),
    //       (route) => false,
    //     );
    //   } else {
    // if (spUtil.getBool(isTutorialComplete) ?? false) {
    // Navigator.pushAndRemoveUntil(
    //   context,
    //   PageRouteBuilder(
    //       opaque: false,
    //       settings: const RouteSettings(name: Routes.login),
    //       pageBuilder: (context, animation, secondaryAnimation) =>
    //           LoginScreen(),
    //       transitionDuration: Duration(seconds: 1),
    //       transitionsBuilder: (context, animation, secondaryAnimation, child) {
    //         return FadeTransition(
    //           opacity: animation,
    //           child: child,
    //         );
    //       }),
    //   (route) => false,
    // );
    // Navigator.pushAndRemoveUntil(
    //   context,
    //   MaterialPageRoute(
    //     builder: (BuildContext context) => GlobalScreen(),
    //   ),
    //       (route) => false,
    // );
    // }
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Splash Screen --------");
    // spInit(context);

    return const Scaffold(
      backgroundColor: color_primary,
      // body: Stack
      //   children: [
      //     getWidgetBackgroundLayer(context),
      //     Center(
      //         child: Image.asset(
      //       image_moly_logo,
      //       width: 600.sp,
      //       height: 300.sp,
      //     )),
      //   ],
      // ),
    );
  }
}
