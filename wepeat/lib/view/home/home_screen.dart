import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart' as dio;
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/crop_image_screen.dart';
import 'package:wepeat/helper/device_utils.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/model/my_media_model.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/viewmodel/home_viewmodel.dart';
import 'package:wepeat/widgets/bottom_nav_widget.dart';
import 'package:wepeat/widgets/network_image_widget.dart';

class HomeScreen extends PageProvideNode<HomeViewModel> {
  @override
  Widget buildContent(BuildContext context) {
    return _HomeContentScreen(mProvider);
  }
}

/// View
class _HomeContentScreen extends StatefulWidget {
  final HomeViewModel provide;

  _HomeContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _HomeContentState();
  }
}

class _HomeContentState extends State<_HomeContentScreen>
    with TickerProviderStateMixin<_HomeContentScreen>
    implements Presenter, PresenterWithValue {
  HomeViewModel? mProvide;
  TextEditingController etName = TextEditingController();
  TextEditingController etDesc = TextEditingController();
  String dob = "";
  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();
  final GlobalKey<TagsState> _tagMemberStateKey = GlobalKey<TagsState>();
  final GlobalKey<TagsState> _tagAllergiesStateKey = GlobalKey<TagsState>();
  final GlobalKey<TagsState> _tagFoodStyleStateKey = GlobalKey<TagsState>();
  final GlobalKey<TagsState> _tagKitchenStateKey = GlobalKey<TagsState>();
  final GlobalKey<TagsState> _tagCookingStateKey = GlobalKey<TagsState>();
  final GlobalKey<TagsState> _tagBasicStateKey = GlobalKey<TagsState>();
  final GlobalKey<TagsState> _objectivesStateKey = GlobalKey<TagsState>();
  List<MyMedia> media = [];
  List mediaImages = [];
  List mediaVideos = [];
  List mediaThumbnail = [];
  var imageFile;

  @override
  void initState() {
    super.initState();

    mProvide = widget.provide;
    setListeners();
    spUtil.putInt(keyBottomSelected, 0);
    mProvide?.getHome(this);
    // mProvide?.getTags(this);
    mProvide?.getTagsType(this);
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    // mProvide!.isApiResponseError.addListener(() {
    //   if (mProvide!.isApiResponseError.value) {
    //     // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
    //     setState(() {
    //       if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
    //         isErrorEmail = true;
    //         strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
    //       }
    //       if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
    //         isErrorPassword = true;
    //         strErrorPassword =
    //             mProvide!.loginErrorsResponse.errors!.password![0];
    //       }
    //     });
    //     mProvide!.isApiResponseError.value = false;
    //   }
    // });
  }

  @override
  void onClick(String? action) {
    print("onClick: " + action!);
    switch (action) {
      case ACTION_MAYBE_LATER:
        GeneralUtils.navigationPop(context);
        break;
      case ACTION_ON_SUCCESS:
        setState(() {
          mProvide?.getHome(this);
          mProvide?.getTags(this);
        });
        break;
      case actionAddMember:
        etName.clear();
        etDesc.clear();
        dob = "";
        media.clear();
        mediaImages.clear();
        imageFile = null;
        mProvide?.genderTags.forEach((element) => element.isSelected = false);
        mProvide?.memberTypeTags
            .forEach((element) => element.isSelected = false);
        mProvide?.allergyTags.forEach((element) => element.isSelected = false);
        GeneralUtils.navigationPushTo(
            context, Routes.editHomeMemWidget(isEdit: false),
            routeName: Routes.editHomeMem, returnFunction: () {
          onClick(ACTION_ON_SUCCESS);
        });
        // showAddMemberPickerSheet(context);
        break;
      case ACTION_CONFIRM_ADD_MEMBER:
        setState(() {
          GeneralUtils.navigationPop(context);
        });
        mProvide?.addMember(
            this, etName.text.trim(), dob, mediaImages, context);
        break;

      case actionUpdateHome:
        setState(() {
          GeneralUtils.navigationPop(context);
        });
        mProvide?.updateHome(
            this, etName.text.trim(), etDesc.text.trim(), mediaImages);
        break;

      case actionSuccess:
        if (mounted) setState(() {});
        break;

      case actionEditHome:
        GeneralUtils.navigationPop(context);
        mProvide?.getHomeTags(this);
        etName.text = mProvide?.homeData?.name ?? "";
        etDesc.text = mProvide?.homeData?.description ?? "";
        break;

      case actionOpenEditMember:
        showHomePickerSheet(context);
        break;
    }
  }

  @override
  void onClickWithValue(String action, int? value) {
    print(action);
    switch (action) {
      case actionEditMember:
        mProvide?.getUserTags(
            this, mProvide?.homeData?.members?[value!].id, value);
        etName.text = mProvide?.homeData?.members?[value!].name ?? "";
        dob = mProvide?.homeData?.members?[value!].birthdate ?? "";
        break;
      case actionOpenEditMember:
        GeneralUtils.navigationPushTo(
            context,
            Routes.editHomeMemWidget(
                member: mProvide?.homeData?.members?[value!], isEdit: true),
            routeName: Routes.editHomeMem, returnFunction: () {
          onClick(ACTION_ON_SUCCESS);
        });
        // showAddMemberPickerSheet(context, isEdit: true, index: value);
        break;
      case ACTION_CONFIRM_UPDATE_MEMBER:
        setState(() {
          GeneralUtils.navigationPop(context);
        });
        if (mediaImages.isNotEmpty) {
          mProvide?.updateMemberImage(this, context, value,
              mProvide?.homeData?.members?[value!].id, mediaImages);
        }
        mProvide?.updateMember(
          this,
          etName.text.trim(),
          dob,
          mProvide?.homeData?.members?[value!].id,
          value,
          mediaImages,
        );
        // media.clear();
        // mediaImages.clear();
        // imageFile = null;
        break;
      case actionDelMember:
        mProvide?.delMember(
            this, mProvide?.homeData?.members?[value!].id, value);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Home Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: color_white,
      bottomNavigationBar: BottomNavWidget(),
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Stack(
            children: [
              _buildBody(),
              Consumer<HomeViewModel>(builder: (context, value, child) {
                return getWidgetScreenLoader(context, value.loading);
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(40.sp),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            mProvide?.homeData?.name ?? stringMyHome.tr,
            style: Style_32_Bold_Black03,
          ),
          spacerView(context, h: 64.sp),
          Text(
            mProvide?.homeData?.description ?? stringMyFamily.tr,
            style: Style_28_Bold_Black03,
          ),
          spacerView(context, h: 16.sp),
          Text(
            stringAllMemOfFamilyAreIncludedHere.tr,
            style: Style_18_Reg_Black03,
          ),
          spacerView(context, h: 36.sp),
          getWidgetNonButton(
              context, stringEditHomeInfo.tr, actionEditHome, this,
              isFilled: false,
              borderColor: color_primary,
              textStyle: Style_18_Bold_Primary),
          spacerView(context, h: 64.sp),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  stringListOfHouseMem.tr,
                  style: Style_18_Bold_Black,
                ),
              ),
              getButtonPadding(
                  context, stringAddmember.tr, actionAddMember, this,
                  isFilled: true,
                  padding: EdgeInsets.only(
                      left: 32.sp, right: 32.sp, top: 12.sp, bottom: 12.sp)),
            ],
          ),
          spacerView(context, h: 28.sp),
          if (mProvide?.homeData?.members != null &&
              mProvide!.homeData!.members!.isNotEmpty &&
              !mProvide!.noHomeMember)
            _buildMembersListView(),
          if (mProvide?.homeData?.members == null && mProvide!.noHomeMember)
            _addMembersiew(),
        ],
      ),
    );
  }

  Widget _buildMembersListView() {
    return ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return _buildMemberTile(index);
        },
        separatorBuilder: (context, index) {
          return spacerView(context, h: 20.sp);
        },
        itemCount: mProvide?.homeData?.members?.length ?? 0);
  }

  Widget _buildMemberTile(int index) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: color_grey),
        borderRadius: BorderRadius.all(Radius.circular(16.sp)),
      ),
      padding: EdgeInsets.all(24.sp),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              NetworkImageWidget(
                url:
                    mProvide?.homeData?.members?[index].media?.fullUrl ?? imgPH,
                height: 132.sp,
                width: 132.sp,
                shape: BoxShape.circle,
                fit: BoxFit.cover,
              ),
              spacerView(context, w: 20.sp),
              Container(
                width: MediaQuery.of(context).size.width / 3.5,
                child: Text(
                  (mProvide?.homeData?.members?[index].name ?? ""),
                  style: Style_18_Bold_Black,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
          Row(
            children: [
              getButtonPadding(
                  context, stringEdit.tr, actionOpenEditMember, this,
                  width: 100.sp,
                  isFilled: false,
                  presenterValue: this,
                  value: index,
                  padding: EdgeInsets.only(
                      left: 32.sp, right: 32.sp, top: 12.sp, bottom: 12.sp)),
              spacerView(context, w: 28.sp),
              InkWell(
                  onTap: () {
                    onClickWithValue(actionDelMember, index);
                  },
                  child: SvgPicture.asset(deleteIcon))
            ],
          ),
        ],
      ),
    );
  }

  Widget _addMembersiew() {
    return InkWell(
      onTap: () {
        onClick(actionAddMember);
      },
      child: DottedBorder(
        strokeWidth: 1,
        borderType: BorderType.RRect,
        radius: Radius.circular(40.sp),
        dashPattern: [10],
        color: color_grey,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(40.sp)),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.30,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(addUserIcon),
                Text(
                  stringClickToAddNewMem.tr,
                  style: Style_20_Bold_Black03,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void showAddMemberPickerSheet(context, {bool isEdit = false, int? index}) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
                padding: EdgeInsets.all(40.sp),
                height: MediaQuery.of(context).size.height - 200.sp,
                decoration: BoxDecoration(
                  color: color_white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40.sp),
                      topRight: Radius.circular(40.sp)),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          width: 80.sp,
                          height: 10.sp,
                          decoration: BoxDecoration(
                            color: color_grey,
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.sp)),
                          ),
                        ),
                      ),
                      SizedBox(height: 30.sp),
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          isEdit ? stringUpdateMember.tr : stringAddMember.tr,
                          style: Style_24_Bold_Dark_Blue,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      spacerView(context, h: 64.sp),
                      Row(
                        children: [
                          Text(
                            stringPutAPhoto.tr,
                            style: Style_18_Bold_Black,
                          ),
                          spacerView(context, w: 12.sp),
                          Container(
                            padding: EdgeInsets.all(8.sp),
                            decoration: BoxDecoration(
                              color: greyF3,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8.sp),
                              ),
                            ),
                            child: Text(
                              stringOptional.tr,
                              style: Style_14_Bold_Black,
                            ),
                          ),
                        ],
                      ),
                      spacerView(context, h: 30.sp),
                      InkWell(
                        onTap: () {
                          showPickerSheet(context, 0);
                        },
                        child: Stack(
                          alignment: Alignment.bottomRight,
                          children: [
                            imageFile == null
                                ? NetworkImageWidget(
                                    url: isEdit
                                        ? mProvide?.homeData?.members![index!]
                                                .media?.fullUrl ??
                                            imgPH
                                        : imgPH,
                                    height: 206.sp,
                                    width: 206.sp,
                                    shape: BoxShape.circle,
                                  )
                                : ClipOval(
                                    child: SizedBox.fromSize(
                                      size: Size.fromRadius(48), // Image radius
                                      child: Image.file(
                                        imageFile,
                                        height: 206.sp,
                                        width: 206.sp,
                                      ),
                                    ),
                                  ),
                            SvgPicture.asset(syncIcon)
                          ],
                        ),
                      ),
                      spacerView(context, h: 64.sp),
                      Text(
                        stringWhatsItCalled.tr,
                        style: Style_18_Bold_Black,
                      ),
                      spacerView(context, h: 24.sp),
                      getTextField(etName, "", stringIntroduceName.tr,
                          presenter: this,
                          action: ACTION_ON_EMAIL_CHANGE,
                          isMarginTop: false),
                      spacerView(context, h: 64.sp),
                      Text(
                        stringWhenWasBorn.tr,
                        style: Style_18_Bold_Black,
                      ),
                      spacerView(context, h: 24.sp),
                      InkWell(
                        onTap: () {
                          DatePicker.showDatePicker(
                            context,
                            showTitleActions: true,
                            locale: LocaleType.en,
                            onChanged: (date) {
                              print('change $date');
                              mystate(() {
                                dob = "${date.year}/${date.month}/${date.day}";
                              });
                            },
                            onConfirm: (date) {
                              print('confirm $date');
                              mystate(() {
                                dob = "${date.year}/${date.month}/${date.day}";
                              });
                            },
                          );
                        },
                        child: Container(
                          height: 100.sp,
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.only(left: 32.sp, top: 32.sp),
                          decoration: BoxDecoration(
                            border: Border.all(color: color_grey),
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.sp)),
                          ),
                          child: Text(
                            dob,
                            style: Style_16_Reg_Placeholder,
                          ),
                        ),
                      ),
                      spacerView(context, h: 64.sp),
                      Text(
                        stringWhatGender.tr,
                        style: Style_18_Bold_Black,
                      ),
                      spacerView(context, h: 24.sp),
                      Tags(
                        key: _tagStateKey,
                        columns: 3,
                        alignment: WrapAlignment.spaceBetween,
                        itemCount: mProvide!.genderTags.length,
                        itemBuilder: (int index) {
                          return ItemTags(
                            key: Key(index.toString()),
                            alignment: MainAxisAlignment.center,
                            index: index,
                            title: mProvide!.genderTags[index].name,
                            textStyle: TextStyle(fontSize: 32.sp),
                            activeColor: color_primary,
                            active: mProvide!.genderTags[index].isSelected,
                            borderRadius:
                                BorderRadius.all(Radius.circular(16.sp)),
                            border: Border.all(
                                color: mProvide!.genderTags[index].isSelected
                                    ? color_primary
                                    : black03),
                            padding: EdgeInsets.only(
                                left: 22.sp,
                                right: 22.sp,
                                top: 20.sp,
                                bottom: 20.sp),
                            textColor: color_black,
                            textActiveColor: color_white,
                            combine: ItemTagsCombine.withTextBefore,
                            onPressed: (item) {
                              mystate(() {
                                mProvide!.genderTags[index].isSelected =
                                    !mProvide!.genderTags[index].isSelected;
                              });
                            },
                            onLongPressed: (item) => print(item),
                          );
                        },
                      ),
                      spacerView(context, h: 64.sp),
                      Text(
                        stringWhatType.tr,
                        style: Style_18_Bold_Black,
                      ),
                      spacerView(context, h: 24.sp),
                      Tags(
                        key: _tagMemberStateKey,
                        columns: 3,
                        alignment: WrapAlignment.spaceBetween,
                        itemCount: mProvide!.memberTypeTags.length,
                        itemBuilder: (int index) {
                          return ItemTags(
                            key: Key(index.toString()),
                            alignment: MainAxisAlignment.center,
                            index: index,
                            title: mProvide!.memberTypeTags[index].name,
                            textStyle: TextStyle(fontSize: 32.sp),
                            activeColor: color_primary,
                            active: mProvide!.memberTypeTags[index].isSelected,
                            borderRadius:
                                BorderRadius.all(Radius.circular(16.sp)),
                            border: Border.all(
                                color:
                                    mProvide!.memberTypeTags[index].isSelected
                                        ? color_primary
                                        : black03),
                            padding: EdgeInsets.only(
                                left: 22.sp,
                                right: 22.sp,
                                top: 20.sp,
                                bottom: 20.sp),
                            textColor: color_black,
                            textActiveColor: color_white,
                            combine: ItemTagsCombine.withTextBefore,
                            onPressed: (item) {
                              mystate(() {
                                mProvide!.memberTypeTags[index].isSelected =
                                    !mProvide!.memberTypeTags[index].isSelected;
                              });
                            },
                            onLongPressed: (item) => print(item),
                          );
                        },
                      ),
                      spacerView(context, h: 64.sp),
                      Text(
                        stringAnyAllergies.tr,
                        style: Style_18_Bold_Black,
                      ),
                      spacerView(context, h: 24.sp),
                      Tags(
                        key: _tagAllergiesStateKey,
                        columns: 3,
                        alignment: WrapAlignment.spaceBetween,
                        itemCount: mProvide!.allergyTags.length,
                        itemBuilder: (int index) {
                          return ItemTags(
                            key: Key(index.toString()),
                            alignment: MainAxisAlignment.center,
                            index: index,
                            title: mProvide!.allergyTags[index].name,
                            textStyle: TextStyle(fontSize: 32.sp),
                            activeColor: color_primary,
                            active: mProvide!.allergyTags[index].isSelected,
                            borderRadius:
                                BorderRadius.all(Radius.circular(16.sp)),
                            border: Border.all(
                                color: mProvide!.allergyTags[index].isSelected
                                    ? color_primary
                                    : black03),
                            padding: EdgeInsets.only(
                                left: 22.sp,
                                right: 22.sp,
                                top: 20.sp,
                                bottom: 20.sp),
                            textColor: color_black,
                            textActiveColor: color_white,
                            combine: ItemTagsCombine.withTextBefore,
                            onPressed: (item) {
                              mystate(() {
                                mProvide!.allergyTags[index].isSelected =
                                    !mProvide!.allergyTags[index].isSelected;
                              });
                            },
                            onLongPressed: (item) => print(item),
                          );
                        },
                      ),
                      spacerView(context, h: 64.sp),
                      Divider(),
                      spacerView(context, h: 64.sp),
                      Row(
                        children: [
                          getButtonPadding(context, btnLblCancel.tr,
                              ACTION_MAYBE_LATER, this,
                              isFilled: true,
                              color: greyF3,
                              textStyle: Style_18_Bold_Black,
                              padding: EdgeInsets.only(
                                  left: 48.sp,
                                  right: 48.sp,
                                  top: 32.sp,
                                  bottom: 32.sp)),
                          spacerView(context, w: 20.sp),
                          Expanded(
                            child: isEdit
                                ? getButtonPadding(
                                    context,
                                    stringUpdateMember.tr,
                                    ACTION_CONFIRM_UPDATE_MEMBER,
                                    this,
                                    isFilled: true,
                                    presenterValue: this,
                                    value: index,
                                    padding: EdgeInsets.only(
                                        // left: 32.sp,
                                        // right: 32.sp,
                                        top: 32.sp,
                                        bottom: 32.sp),
                                  )
                                : getButtonPadding(
                                    context,
                                    stringConfirmMember.tr,
                                    ACTION_CONFIRM_ADD_MEMBER,
                                    this,
                                    isFilled: true,
                                    padding: EdgeInsets.only(
                                        // left: 32.sp,
                                        // right: 32.sp,
                                        top: 32.sp,
                                        bottom: 32.sp),
                                  ),
                          ),
                        ],
                      ),
                      spacerView(context, h: 64.sp),
                    ],
                  ),
                ));
          });
        }).whenComplete(() {});
  }

  void showHomePickerSheet(context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Container(
                padding:
                    EdgeInsets.all(DeviceUtils.getScaledX(context, 20, 20)),
                height: MediaQuery.of(context).size.height - 200.sp,
                decoration: BoxDecoration(
                  color: color_white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40.sp),
                      topRight: Radius.circular(40.sp)),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          width: 80.sp,
                          height: 10.sp,
                          decoration: BoxDecoration(
                            color: color_grey,
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.sp)),
                          ),
                        ),
                      ),
                      SizedBox(height: 30.sp),
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          stringUpdateMyHomeInfo.tr,
                          style: Style_24_Bold_Dark_Blue,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      spacerView(context, h: 64.sp),
                      Row(
                        children: [
                          Text(
                            stringNameYourHome.tr,
                            style: Style_18_Bold_Black,
                          ),
                          spacerView(context, w: 16.sp),
                          Container(
                            padding: EdgeInsets.all(8.sp),
                            decoration: BoxDecoration(
                              color: greyF3,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8.sp),
                              ),
                            ),
                            child: Text(
                              stringOptional.tr,
                              style: Style_14_Bold_Black,
                            ),
                          ),
                        ],
                      ),
                      spacerView(context, h: 24.sp),
                      getTextField(etName, "", stringIntroduceName.tr,
                          presenter: this,
                          action: ACTION_ON_EMAIL_CHANGE,
                          isMarginTop: false),
                      spacerView(context, h: 64.sp),
                      Row(
                        children: [
                          Text(
                            stringHowYouDescribeIt.tr,
                            style: Style_18_Bold_Black,
                          ),
                          spacerView(context, w: 16.sp),
                          Container(
                            padding: EdgeInsets.all(8.sp),
                            decoration: BoxDecoration(
                              color: greyF3,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8.sp),
                              ),
                            ),
                            child: Text(
                              stringOptional.tr,
                              style: Style_14_Bold_Black,
                            ),
                          ),
                        ],
                      ),
                      spacerView(context, h: 24.sp),
                      getTextField(etDesc, "", "",
                          presenter: this,
                          action: ACTION_ON_EMAIL_CHANGE,
                          isMarginTop: false),
                      spacerView(context, h: 64.sp),
                      // if (mProvide!.foodStyleTags.isNotEmpty)
                      //   Text(
                      //     "Food style",
                      //     style: Style_18_Bold_Black,
                      //   ),
                      // if (mProvide!.foodStyleTags.isNotEmpty)
                      //   spacerView(context, h: 24.sp),
                      // if (mProvide!.foodStyleTags.isNotEmpty)
                      //   Tags(
                      //     key: _tagFoodStyleStateKey,
                      //     columns: 3,
                      //     alignment: WrapAlignment.start,
                      //     itemCount: mProvide!.foodStyleTags.length,
                      //     itemBuilder: (int index) {
                      //       return ItemTags(
                      //         key: Key(index.toString()),
                      //         alignment: MainAxisAlignment.center,
                      //         index: index,
                      //         title: mProvide!.foodStyleTags[index].name,
                      //         textStyle: TextStyle(fontSize: 32.sp),
                      //         activeColor: color_primary,
                      //         active: mProvide!.foodStyleTags[index].isSelected,
                      //         borderRadius:
                      //             BorderRadius.all(Radius.circular(16.sp)),
                      //         border: Border.all(
                      //             color:
                      //                 mProvide!.foodStyleTags[index].isSelected
                      //                     ? color_primary
                      //                     : black03),
                      //         padding: EdgeInsets.only(
                      //             left: 22.sp,
                      //             right: 22.sp,
                      //             top: 20.sp,
                      //             bottom: 20.sp),
                      //         textColor: color_black,
                      //         textActiveColor: color_white,
                      //         combine: ItemTagsCombine.withTextBefore,
                      //         onPressed: (item) {
                      //           mystate(() {
                      //             mProvide!.foodStyleTags[index].isSelected =
                      //                 !mProvide!
                      //                     .foodStyleTags[index].isSelected;
                      //           });
                      //         },
                      //         onLongPressed: (item) => print(item),
                      //       );
                      //     },
                      //   ),
                      // spacerView(context, h: 64.sp),
                      // if (mProvide!.equipmentTags.isNotEmpty)
                      //   Text(
                      //     "Equipments",
                      //     style: Style_18_Bold_Black,
                      //   ),
                      // if (mProvide!.equipmentTags.isNotEmpty)
                      //   Container(
                      //     width: MediaQuery.of(context).size.width,
                      //     decoration: BoxDecoration(
                      //         border: Border.all(color: color_grey),
                      //         borderRadius:
                      //             BorderRadius.all(Radius.circular(8.sp))),
                      //     padding: EdgeInsets.all(40.sp),
                      //     margin: EdgeInsets.only(top: 24.sp),
                      //     child: Column(
                      //       crossAxisAlignment: CrossAxisAlignment.start,
                      //       children: [
                      //         Text(
                      //           "Kitchen robot",
                      //           style: Style_18_Reg_Black,
                      //         ),
                      //         spacerView(context, h: 24.sp),
                      //         Tags(
                      //           key: _tagKitchenStateKey,
                      //           columns: 3,
                      //           alignment: WrapAlignment.start,
                      //           itemCount: mProvide!.equipmentTags.length,
                      //           itemBuilder: (int index) {
                      //             return ItemTags(
                      //               key: Key(index.toString()),
                      //               alignment: MainAxisAlignment.center,
                      //               index: index,
                      //               title: mProvide!.equipmentTags[index].name,
                      //               textStyle: TextStyle(fontSize: 32.sp),
                      //               activeColor: color_primary,
                      //               active: mProvide!
                      //                   .equipmentTags[index].isSelected,
                      //               borderRadius: BorderRadius.all(
                      //                   Radius.circular(16.sp)),
                      //               border: Border.all(
                      //                   color: mProvide!
                      //                           .equipmentTags[index].isSelected
                      //                       ? color_primary
                      //                       : black03),
                      //               padding: EdgeInsets.only(
                      //                   left: 22.sp,
                      //                   right: 22.sp,
                      //                   top: 20.sp,
                      //                   bottom: 20.sp),
                      //               textColor: color_black,
                      //               textActiveColor: color_white,
                      //               combine: ItemTagsCombine.withTextBefore,
                      //               onPressed: (item) {
                      //                 mystate(() {
                      //                   mProvide!.equipmentTags[index]
                      //                           .isSelected =
                      //                       !mProvide!.equipmentTags[index]
                      //                           .isSelected;
                      //                 });
                      //               },
                      //               onLongPressed: (item) => print(item),
                      //             );
                      //           },
                      //         ),
                      //       ],
                      //     ),
                      //   ),
                      // if (mProvide!.cookingEquipmentTags.isNotEmpty)
                      //   Container(
                      //     width: MediaQuery.of(context).size.width,
                      //     decoration: BoxDecoration(
                      //         border: Border.all(color: color_grey),
                      //         borderRadius:
                      //             BorderRadius.all(Radius.circular(8.sp))),
                      //     padding: EdgeInsets.all(40.sp),
                      //     margin: EdgeInsets.only(top: 24.sp),
                      //     child: Column(
                      //       crossAxisAlignment: CrossAxisAlignment.start,
                      //       children: [
                      //         Text(
                      //           "Cooking equipment",
                      //           style: Style_18_Reg_Black,
                      //         ),
                      //         spacerView(context, h: 24.sp),
                      //         Tags(
                      //           key: _tagCookingStateKey,
                      //           columns: 3,
                      //           alignment: WrapAlignment.start,
                      //           itemCount:
                      //               mProvide!.cookingEquipmentTags.length,
                      //           itemBuilder: (int index) {
                      //             return ItemTags(
                      //               key: Key(index.toString()),
                      //               alignment: MainAxisAlignment.center,
                      //               index: index,
                      //               title: mProvide!
                      //                   .cookingEquipmentTags[index].name,
                      //               textStyle: TextStyle(fontSize: 32.sp),
                      //               activeColor: color_primary,
                      //               active: mProvide!
                      //                   .cookingEquipmentTags[index].isSelected,
                      //               borderRadius: BorderRadius.all(
                      //                   Radius.circular(16.sp)),
                      //               border: Border.all(
                      //                   color: mProvide!
                      //                           .cookingEquipmentTags[index]
                      //                           .isSelected
                      //                       ? color_primary
                      //                       : black03),
                      //               padding: EdgeInsets.only(
                      //                   left: 22.sp,
                      //                   right: 22.sp,
                      //                   top: 20.sp,
                      //                   bottom: 20.sp),
                      //               textColor: color_black,
                      //               textActiveColor: color_white,
                      //               combine: ItemTagsCombine.withTextBefore,
                      //               onPressed: (item) {
                      //                 mystate(() {
                      //                   mProvide!.cookingEquipmentTags[index]
                      //                           .isSelected =
                      //                       !mProvide!
                      //                           .cookingEquipmentTags[index]
                      //                           .isSelected;
                      //                 });
                      //               },
                      //               onLongPressed: (item) => print(item),
                      //             );
                      //           },
                      //         ),
                      //       ],
                      //     ),
                      //   ),
                      // if (mProvide!.basicTags.isNotEmpty)
                      //   Container(
                      //     width: MediaQuery.of(context).size.width,
                      //     decoration: BoxDecoration(
                      //         border: Border.all(color: color_grey),
                      //         borderRadius:
                      //             BorderRadius.all(Radius.circular(8.sp))),
                      //     padding: EdgeInsets.all(40.sp),
                      //     margin: EdgeInsets.only(top: 24.sp),
                      //     child: Column(
                      //       crossAxisAlignment: CrossAxisAlignment.start,
                      //       children: [
                      //         Text(
                      //           "Basics",
                      //           style: Style_18_Reg_Black,
                      //         ),
                      //         spacerView(context, h: 24.sp),
                      //         Tags(
                      //           key: _tagBasicStateKey,
                      //           columns: 3,
                      //           alignment: WrapAlignment.start,
                      //           itemCount: mProvide!.basicTags.length,
                      //           itemBuilder: (int index) {
                      //             return ItemTags(
                      //               key: Key(index.toString()),
                      //               alignment: MainAxisAlignment.center,
                      //               index: index,
                      //               title: mProvide!.basicTags[index].name,
                      //               textStyle: TextStyle(fontSize: 32.sp),
                      //               activeColor: color_primary,
                      //               active:
                      //                   mProvide!.basicTags[index].isSelected,
                      //               borderRadius: BorderRadius.all(
                      //                   Radius.circular(16.sp)),
                      //               border: Border.all(
                      //                   color: mProvide!
                      //                           .basicTags[index].isSelected
                      //                       ? color_primary
                      //                       : black03),
                      //               padding: EdgeInsets.only(
                      //                   left: 22.sp,
                      //                   right: 22.sp,
                      //                   top: 20.sp,
                      //                   bottom: 20.sp),
                      //               textColor: color_black,
                      //               textActiveColor: color_white,
                      //               combine: ItemTagsCombine.withTextBefore,
                      //               onPressed: (item) {
                      //                 mystate(() {
                      //                   mProvide!.basicTags[index].isSelected =
                      //                       !mProvide!
                      //                           .basicTags[index].isSelected;
                      //                 });
                      //               },
                      //               onLongPressed: (item) => print(item),
                      //             );
                      //           },
                      //         ),
                      //       ],
                      //     ),
                      //   ),
                      // if (mProvide!.objectiveTags.isNotEmpty)
                      //   Container(
                      //     width: MediaQuery.of(context).size.width,
                      //     decoration: BoxDecoration(
                      //         border: Border.all(color: color_grey),
                      //         borderRadius:
                      //             BorderRadius.all(Radius.circular(8.sp))),
                      //     padding: EdgeInsets.all(40.sp),
                      //     margin: EdgeInsets.only(top: 24.sp),
                      //     child: Column(
                      //       crossAxisAlignment: CrossAxisAlignment.start,
                      //       children: [
                      //         Text(
                      //           "Objetivo",
                      //           style: Style_18_Reg_Black,
                      //         ),
                      //         spacerView(context, h: 24.sp),
                      //         Tags(
                      //           key: _objectivesStateKey,
                      //           columns: 3,
                      //           alignment: WrapAlignment.start,
                      //           itemCount: mProvide!.objectiveTags.length,
                      //           itemBuilder: (int index) {
                      //             return ItemTags(
                      //               key: Key(index.toString()),
                      //               alignment: MainAxisAlignment.center,
                      //               index: index,
                      //               title: mProvide!.objectiveTags[index].name,
                      //               textStyle: TextStyle(fontSize: 32.sp),
                      //               activeColor: color_primary,
                      //               active: mProvide!
                      //                   .objectiveTags[index].isSelected,
                      //               borderRadius: BorderRadius.all(
                      //                   Radius.circular(16.sp)),
                      //               border: Border.all(
                      //                   color: mProvide!
                      //                           .objectiveTags[index].isSelected
                      //                       ? color_primary
                      //                       : black03),
                      //               padding: EdgeInsets.only(
                      //                   left: 22.sp,
                      //                   right: 22.sp,
                      //                   top: 20.sp,
                      //                   bottom: 20.sp),
                      //               textColor: color_black,
                      //               textActiveColor: color_white,
                      //               combine: ItemTagsCombine.withTextBefore,
                      //               onPressed: (item) {
                      //                 mystate(() {
                      //                   mProvide!.objectiveTags[index]
                      //                           .isSelected =
                      //                       !mProvide!.objectiveTags[index]
                      //                           .isSelected;
                      //                 });
                      //               },
                      //               onLongPressed: (item) => print(item),
                      //             );
                      //           },
                      //         ),
                      //       ],
                      //     ),
                      //   ),
                      _buildTagsViewList(),
                      spacerView(context, h: 64.sp),
                      Divider(),
                      spacerView(context, h: 64.sp),
                      Row(
                        children: [
                          getButtonPadding(context, btnLblCancel.tr,
                              ACTION_MAYBE_LATER, this,
                              isFilled: true,
                              color: greyF3,
                              textStyle: Style_18_Bold_Black,
                              padding: EdgeInsets.only(
                                  left: 48.sp,
                                  right: 48.sp,
                                  top: 32.sp,
                                  bottom: 32.sp)),
                          spacerView(context, w: 20.sp),
                          Expanded(
                            child: getButtonPadding(context,
                                stringUpdateInfo.tr, actionUpdateHome, this,
                                isFilled: true,
                                padding: EdgeInsets.only(
                                    // left: 32.sp,
                                    // right: 32.sp,
                                    top: 32.sp,
                                    bottom: 32.sp)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ));
          });
        }).whenComplete(() {});
  }

  Widget _buildTagsViewList() {
    return ListView.separated(
      padding: EdgeInsets.zero,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return _buildTagTile(index);
      },
      separatorBuilder: (context, index) {
        return spacerView(context, h: 64.sp);
      },
      itemCount: mProvide?.homeTagsType?.tagtypes?.length ?? 0,
    );
  }

  Widget _buildTagTile(i) {
    final GlobalKey<TagsState> _stateKey = GlobalKey<TagsState>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          mProvide?.homeTagsType?.tagtypes?[i].name ?? "",
          style: Style_18_Bold_Black,
        ),
        spacerView(context, h: 24.sp),
        Tags(
          key: _stateKey,
          columns: 3,
          alignment: WrapAlignment.start,
          itemCount:
              mProvide?.homeTagsType?.tagtypes?[i].tagsData?.data?.length ?? 0,
          itemBuilder: (int index) {
            return ItemTags(
              key: Key(index.toString()),
              alignment: MainAxisAlignment.center,
              index: index,
              title: mProvide
                  ?.homeTagsType?.tagtypes?[i].tagsData?.data?[index].name,
              textStyle: TextStyle(fontSize: 32.sp),
              activeColor: color_primary,
              active: mProvide?.homeTagsType?.tagtypes?[i].tagsData
                  ?.data?[index].isSelected,
              borderRadius: BorderRadius.all(Radius.circular(16.sp)),
              border: Border.all(
                  color: (mProvide?.homeTagsType?.tagtypes?[i].tagsData
                              ?.data?[index].isSelected ??
                          false)
                      ? color_primary
                      : black03),
              padding: EdgeInsets.only(
                  left: 22.sp, right: 22.sp, top: 20.sp, bottom: 20.sp),
              textColor: color_black,
              textActiveColor: color_white,
              combine: ItemTagsCombine.withTextBefore,
              onPressed: (item) {
                setState(() {
                  mProvide?.homeTagsType?.tagtypes?[i].tagsData?.data?[index]
                      .isSelected = !(mProvide?.homeTagsType?.tagtypes?[i]
                          .tagsData?.data?[index].isSelected ??
                      false);
                });
              },
              onLongPressed: (item) => print(item),
            );
          },
        ),
      ],
    );
  }

  void showPickerSheet(context, int type) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text(stringGallery.tr),
                      onTap: () {
                        _imgFromGallery(type);
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text(stringCamera.tr),
                    onTap: () {
                      imgFromCamera(type);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  imgFromCamera(int type) async {
    List<Media>? res = await ImagesPicker.openCamera(
      pickType: Platform.isAndroid
          ? PickType.image
          : (type == 0)
              ? PickType.image
              : PickType.video,
      maxTime: 9000,
    );
    if (res != null) {
      _imageAndVideoCropper(res);
    }
  }

  _imgFromGallery(int type) async {
    List<Media>? res = await ImagesPicker.pick(
      count: 1,
      pickType: (type == 0) ? PickType.image : PickType.video,
      language: Language.System,
      maxSize: 9000,
    );
    if (res != null) {
      _imageAndVideoCropper(res);
    }
  }

  Future<File?> showCropImageDialog(BuildContext context, Media res,
      double? aspectRatio, bool? showAspectRatioOptions) async {
    File? sendingFile;
    await showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black,
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return CropImageScreen((File? file) {
          Navigator.of(context, rootNavigator: true).pop();
          res.path = file!.path;
          sendingFile = file;
        }, File(res.path), aspectRatio, showAspectRatioOptions);
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 0), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
    return sendingFile;
  }

  _imageAndVideoCropper(List<Media> res) async {
    for (int i = 0; i < res.length; i++) {
      String mimeStr = lookupMimeType(res[i].path)!;
      var fileType = mimeStr.split('/');
      if ((fileType[0] == "image")) {
        File? croppedFile =
            await showCropImageDialog(context, res[i], 1 / 1, false);
        if (croppedFile != null) {
          res[i].path = croppedFile.path;
        } else {
          res[i].path = "";
        }
      } else {
        //In-Case Video
      }
    }

    // ignore: unnecessary_null_comparison
    if (res != null) {
      handleMediaResult(res);
    }
  }

  void handleMediaResult(List<Media> res) {
    print(res.map((e) => e.path).toList());
    for (int i = 0; i < res.length; i++) {
      if (res[i].path != "") {
        String mimeStr = lookupMimeType(res[i].path)!;
        var fileType = mimeStr.split('/');
        print('file type $fileType');
        media.clear();
        media.add(MyMedia(
            mediaPath: res[i].path,
            mediaType: (fileType[0] == "image") ? 0 : 1,
            thumbnail: res[i].thumbPath));
      }
    }

    print(media);
    finalApiCall();
  }

  void finalApiCall() {
    //List tier_list = []; //[mProvide.response.data.subscriptionTier[_checkboxValue].id];

    for (int i = 0; i < media.length; i++) {
      print("mediaPath= " + media[i].mediaPath!);
      File file = File(media[i].mediaPath!);
      imageFile = file;
      String fileName = file.path.split('/').last;
      print("filePath= " + file.path);
      print("fileName= " + fileName);
      if (media[i].mediaType == 0) {
        mediaImages
            .add(dio.MultipartFile.fromFileSync(file.path, filename: fileName));
      }
      if (media[i].mediaType == 1) {
        mediaVideos
            .add(dio.MultipartFile.fromFileSync(file.path, filename: fileName));
        mediaThumbnail.add(dio.MultipartFile.fromFileSync(media[i].thumbnail!,
            filename: fileName));
      }
    }
    setState(() {});
  }
}
