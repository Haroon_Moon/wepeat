import 'package:wepeat/model/tags_model.dart';

class TypeTagsModel {
  List<TagTypeItem>? tagtypes;

  TypeTagsModel({this.tagtypes});

  TypeTagsModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      tagtypes = <TagTypeItem>[];
      json['data'].forEach((v) {
        tagtypes!.add(TagTypeItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (tagtypes != null) {
      data['data'] = tagtypes!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TagTypeItem {
  int? id;
  String? key;
  String? name;
  String? description;
  List<String>? availableInModel;
  bool? isMultiple;
  TagsModel? tagsData;

  TagTypeItem({
    this.id,
    this.key,
    this.name,
    this.description,
    this.availableInModel,
    this.isMultiple,
    this.tagsData,
  });

  TagTypeItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    key = json['key'];
    name = json['name'];
    description = json['description'];
    availableInModel = json['available_in_model'].cast<String>();
    isMultiple = json['is_multiple'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['key'] = key;
    data['name'] = name;
    data['description'] = description;
    data['available_in_model'] = availableInModel;
    data['is_multiple'] = isMultiple;
    return data;
  }
}
