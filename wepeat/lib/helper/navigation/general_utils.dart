import 'package:flutter/material.dart';

/// Helper class for device related operations.

class GeneralUtils {
  static navigationPushTo(BuildContext context, Widget route,
      {String? routeName, dynamic data, dynamic returnFunction}) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    FocusScope.of(context).requestFocus(FocusNode());
    Navigator.push(
      context,
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation, secondaryAnimation) => route,
          settings: RouteSettings(name: routeName, arguments: data),
          transitionDuration: const Duration(seconds: 1),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return SlideTransition(
              position: Tween<Offset>(
                begin: const Offset(0, 0),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            );
          }),
    ).then((value) {
      if (returnFunction != null) {
        returnFunction();
      }
    });
  }

  static navigationPushReplacement(BuildContext context, Widget route,
      {String? routeName, dynamic data}) {
    Navigator.of(context).popUntil((route) => route.isFirst);
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    FocusScope.of(context).requestFocus(FocusNode());
    Navigator.pushReplacement(
      context,
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation, secondaryAnimation) => route,
          settings: RouteSettings(name: routeName, arguments: data),
          transitionDuration: const Duration(seconds: 1),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return SlideTransition(
              position: Tween<Offset>(
                begin: const Offset(0, 0),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            );
          }),
    );
  }

  static navigationPushAndRemoveUntil(BuildContext context, Widget route,
      {routeName, data}) {
    Navigator.pushAndRemoveUntil(
      context,
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation, secondaryAnimation) => route,
          settings: RouteSettings(name: routeName, arguments: data),
          transitionDuration: const Duration(seconds: 1),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return SlideTransition(
              position: Tween<Offset>(
                begin: const Offset(0, 0),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            );
          }),
      ModalRoute.withName(routeName),
    );
  }

  static navigationPop(BuildContext context) {
    if (Navigator.of(context).canPop()) Navigator.of(context).pop();
  }

  static popUntil(BuildContext context, String routeName) {
    Navigator.of(context).popUntil(ModalRoute.withName(routeName));
  }
}
