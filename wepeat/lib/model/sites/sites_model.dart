class SitesModel {
  List<Data>? data;

  SitesModel({this.data});

  SitesModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int? id;
  String? prefix;
  String? name;
  String? locale;
  bool? isDefault;

  Data({this.id, this.prefix, this.name, this.locale, this.isDefault});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    prefix = json['prefix'];
    name = json['name'];
    locale = json['locale'];
    isDefault = json['is_default'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['prefix'] = this.prefix;
    data['name'] = this.name;
    data['locale'] = this.locale;
    data['is_default'] = this.isDefault;
    return data;
  }
}
