class UserCalendarModel {
  CalendarDataData? calendarData;

  UserCalendarModel({this.calendarData});

  UserCalendarModel.fromJson(Map<String, dynamic> json) {
    calendarData =
        json['data'] != null ? CalendarDataData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (calendarData != null) {
      data['data'] = calendarData?.toJson();
    }
    return data;
  }
}

class CalendarDataData {
  String? name;
  String? description;

  CalendarDataData({this.name, this.description});

  CalendarDataData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['description'] = description;
    return data;
  }
}
