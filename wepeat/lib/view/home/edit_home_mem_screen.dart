import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart' as dio;
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/crop_image_screen.dart';
import 'package:wepeat/helper/device_utils.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/model/home_model.dart';
import 'package:wepeat/model/my_media_model.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/viewmodel/home_viewmodel.dart';
import 'package:wepeat/widgets/bottom_nav_widget.dart';
import 'package:wepeat/widgets/network_image_widget.dart';
import 'package:wepeat/model/my_media_model.dart';
import '../../viewmodel/edit_home_mem_viewmodel.dart';

class EditHomeMemScreen extends PageProvideNode<EditHomeMemViewModel> {
  bool? isEdit = false;
  Member? homeMem;

  EditHomeMemScreen({this.isEdit = false, this.homeMem});

  @override
  Widget buildContent(BuildContext context) {
    return _EditHomeMemContentScreen(mProvider, isEdit, this.homeMem);
  }
}

/// View
class _EditHomeMemContentScreen extends StatefulWidget {
  final EditHomeMemViewModel provide;
  bool? isEdit;
  Member? homeMem;

  _EditHomeMemContentScreen(this.provide, this.isEdit, this.homeMem);

  @override
  State<StatefulWidget> createState() {
    return _EditHomeMemContentState();
  }
}

class _EditHomeMemContentState extends State<_EditHomeMemContentScreen>
    with TickerProviderStateMixin<_EditHomeMemContentScreen>
    implements Presenter {
  EditHomeMemViewModel? mProvide;
  TextEditingController etName = TextEditingController();
  String dob = "";
  List<MyMedia> media = [];
  List mediaImages = [];
  List mediaVideos = [];
  List mediaThumbnail = [];
  var imageFile;
  // final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();
  // final GlobalKey<TagsState> _tagMemberStateKey = GlobalKey<TagsState>();
  // final GlobalKey<TagsState> _tagAllergiesStateKey = GlobalKey<TagsState>();

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    setListeners();

    if (widget.isEdit!) {
      mProvide?.homeMem = widget.homeMem;
      etName.text = widget.homeMem?.name ?? "";
      dob = widget.homeMem?.birthdate ?? "";
    } else {
      //New Memeber
      etName.clear();
      dob = "";
      media.clear();
      mediaImages.clear();
      imageFile = null;
      mProvide?.memberTagsType?.tagtypes?.forEach((type) {
        type.tagsData?.data?.forEach((element) {
          if (element.isSelected) {
            element.isSelected = false;
          }
        });
      });
    }
    // mProvide?.getTags(this, widget.isEdit!);
    mProvide?.getTagsType(this, widget.isEdit!);
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    // mProvide!.isApiResponseError.addListener(() {
    //   if (mProvide!.isApiResponseError.value) {
    //     // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
    //     setState(() {
    //       if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
    //         isErrorEmail = true;
    //         strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
    //       }
    //       if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
    //         isErrorPassword = true;
    //         strErrorPassword =
    //             mProvide!.loginErrorsResponse.errors!.password![0];
    //       }
    //     });
    //     mProvide!.isApiResponseError.value = false;
    //   }
    // });
  }

  @override
  void onClick(String? action) {
    switch (action) {
      case ACTION_CONFIRM_UPDATE_MEMBER:
        if (mediaImages.isNotEmpty) {
          mProvide?.updateMemberImage(
              this, context, widget.homeMem?.id, mediaImages);
        }
        mProvide?.updateMember(
          this,
          etName.text.trim(),
          dob,
          widget.homeMem?.id,
          mediaImages,
        );
        break;
      case ACTION_CONFIRM_ADD_MEMBER:
        mProvide?.addMember(
            this, etName.text.trim(), dob, mediaImages, context);
        break;
      case actionUserTagsSuccess:
        setState(() {
          widget.homeMem?.tags = mProvide?.homeMem?.tags;
        });
        break;
      case actionSuccess:
        if (mounted) setState(() {});
        break;
      case ACTION_MAYBE_LATER:
        GeneralUtils.navigationPop(context);
        break;
      case ACTION_ON_SUCCESS:
        GeneralUtils.navigationPop(context);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Edit Home Member Screen --------");
    spInit(context);

    return Scaffold(
      body: InkWell(
        onTap: () {
          GeneralUtils.navigationPop(context);
        },
        child: Container(
          color: color_grey.withOpacity(0.7),
          child: Container(
            margin: EdgeInsets.only(top: 200.sp),
            child: Stack(
              children: [
                GestureDetector(
                  onTap: () {
                    FocusScopeNode currentFocus = FocusScope.of(context);
                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                  },
                  onPanUpdate: (details) {
                    print(details.delta.dy);
                    if (details.delta.dy > 0.0) {
                      GeneralUtils.navigationPop(context);
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: color_grey),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(54.sp),
                            topRight: Radius.circular(54.sp))),
                    child: Stack(
                      children: [
                        // Padding(padding: EdgeInsets.only(left: 60.sp, right: 60.sp), child:
                        Container(
                          padding: EdgeInsets.all(40.sp),
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: 80.sp,
                                    height: 10.sp,
                                    decoration: BoxDecoration(
                                      color: color_grey,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5.sp)),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 30.sp),
                                Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    widget.isEdit!
                                        ? stringUpdateMember.tr
                                        : stringAddMember.tr,
                                    style: Style_24_Bold_Dark_Blue,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                spacerView(context, h: 64.sp),
                                Row(
                                  children: [
                                    Text(
                                      stringDoYouWantToPutPhoto.tr,
                                      style: Style_18_Bold_Black,
                                    ),
                                    spacerView(context, w: 12.sp),
                                    Container(
                                      padding: EdgeInsets.all(8.sp),
                                      decoration: BoxDecoration(
                                        color: greyF3,
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(8.sp),
                                        ),
                                      ),
                                      child: Text(
                                        stringOptional.tr,
                                        style: Style_14_Bold_Black,
                                      ),
                                    ),
                                  ],
                                ),
                                spacerView(context, h: 30.sp),
                                InkWell(
                                  onTap: () {
                                    showPickerSheet(context, 0);
                                  },
                                  child: Stack(
                                    alignment: Alignment.bottomRight,
                                    children: [
                                      imageFile == null
                                          ? NetworkImageWidget(
                                              url: widget.isEdit!
                                                  ? widget.homeMem?.media
                                                          ?.fullUrl ??
                                                      imgPH
                                                  : imgPH,
                                              height: 206.sp,
                                              width: 206.sp,
                                              shape: BoxShape.circle,
                                            )
                                          : ClipOval(
                                              child: SizedBox.fromSize(
                                                size: Size.fromRadius(
                                                    48), // Image radius
                                                child: Image.file(
                                                  imageFile,
                                                  height: 206.sp,
                                                  width: 206.sp,
                                                ),
                                              ),
                                            ),
                                      SvgPicture.asset(syncIcon)
                                    ],
                                  ),
                                ),
                                spacerView(context, h: 64.sp),
                                Text(
                                  stringWhatsItCalled.tr,
                                  style: Style_18_Bold_Black,
                                ),
                                spacerView(context, h: 24.sp),
                                getTextField(etName, "", stringIntroduceName.tr,
                                    presenter: this,
                                    action: ACTION_ON_EMAIL_CHANGE,
                                    isMarginTop: false),
                                spacerView(context, h: 64.sp),
                                Text(
                                  stringWhenWasBorn.tr,
                                  style: Style_18_Bold_Black,
                                ),
                                spacerView(context, h: 24.sp),
                                InkWell(
                                  onTap: () {
                                    DeviceUtils.hideKeyboard(context);
                                    DatePicker.showDatePicker(
                                      context,
                                      showTitleActions: true,
                                      locale: LocaleType.en,
                                      onChanged: (date) {
                                        print('change $date');
                                        setState(() {
                                          dob =
                                              "${date.year}/${date.month}/${date.day}";
                                        });
                                      },
                                      onConfirm: (date) {
                                        print('confirm $date');
                                        setState(() {
                                          dob =
                                              "${date.year}/${date.month}/${date.day}";
                                        });
                                      },
                                    );
                                  },
                                  child: Container(
                                    height: 100.sp,
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        left: 32.sp, top: 32.sp),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: color_grey),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.sp)),
                                    ),
                                    child: Text(
                                      dob,
                                      style: Style_16_Reg_Placeholder,
                                    ),
                                  ),
                                ),
                                spacerView(context, h: 64.sp),
                                _buildTagsViewList(),
                                // if (mProvide!.genderTags.isNotEmpty)
                                //   Text(
                                //     "What gender do you identify with?",
                                //     style: Style_18_Bold_Black,
                                //   ),
                                // if (mProvide!.genderTags.isNotEmpty)
                                //   spacerView(context, h: 24.sp),
                                // if (mProvide!.genderTags.isNotEmpty)
                                //   Tags(
                                //     key: _tagStateKey,
                                //     columns: 3,
                                //     alignment: WrapAlignment.spaceBetween,
                                //     itemCount: mProvide!.genderTags.length,
                                //     itemBuilder: (int index) {
                                //       return ItemTags(
                                //         key: Key(index.toString()),
                                //         alignment: MainAxisAlignment.center,
                                //         index: index,
                                //         title: mProvide!.genderTags[index].name,
                                //         textStyle: TextStyle(fontSize: 32.sp),
                                //         activeColor: color_primary,
                                //         active: mProvide!
                                //             .genderTags[index].isSelected,
                                //         borderRadius: BorderRadius.all(
                                //             Radius.circular(16.sp)),
                                //         border: Border.all(
                                //             color: mProvide!.genderTags[index]
                                //                     .isSelected
                                //                 ? color_primary
                                //                 : black03),
                                //         padding: EdgeInsets.only(
                                //             left: 22.sp,
                                //             right: 22.sp,
                                //             top: 20.sp,
                                //             bottom: 20.sp),
                                //         textColor: color_black,
                                //         textActiveColor: color_white,
                                //         combine: ItemTagsCombine.withTextBefore,
                                //         onPressed: (item) {
                                //           setState(() {
                                //             mProvide!.genderTags[index]
                                //                     .isSelected =
                                //                 !mProvide!.genderTags[index]
                                //                     .isSelected;
                                //           });
                                //         },
                                //         onLongPressed: (item) => print(item),
                                //       );
                                //     },
                                //   ),
                                // spacerView(context, h: 64.sp),
                                // if (mProvide!.memberTypeTags.isNotEmpty)
                                //   Text(
                                //     "What type of member is it?",
                                //     style: Style_18_Bold_Black,
                                //   ),
                                // if (mProvide!.memberTypeTags.isNotEmpty)
                                //   spacerView(context, h: 24.sp),
                                // if (mProvide!.memberTypeTags.isNotEmpty)
                                //   Tags(
                                //     key: _tagMemberStateKey,
                                //     columns: 3,
                                //     alignment: WrapAlignment.spaceBetween,
                                //     itemCount: mProvide!.memberTypeTags.length,
                                //     itemBuilder: (int index) {
                                //       return ItemTags(
                                //         key: Key(index.toString()),
                                //         alignment: MainAxisAlignment.center,
                                //         index: index,
                                //         title: mProvide!
                                //             .memberTypeTags[index].name,
                                //         textStyle: TextStyle(fontSize: 32.sp),
                                //         activeColor: color_primary,
                                //         active: mProvide!
                                //             .memberTypeTags[index].isSelected,
                                //         borderRadius: BorderRadius.all(
                                //             Radius.circular(16.sp)),
                                //         border: Border.all(
                                //             color: mProvide!
                                //                     .memberTypeTags[index]
                                //                     .isSelected
                                //                 ? color_primary
                                //                 : black03),
                                //         padding: EdgeInsets.only(
                                //             left: 22.sp,
                                //             right: 22.sp,
                                //             top: 20.sp,
                                //             bottom: 20.sp),
                                //         textColor: color_black,
                                //         textActiveColor: color_white,
                                //         combine: ItemTagsCombine.withTextBefore,
                                //         onPressed: (item) {
                                //           setState(() {
                                //             mProvide!.memberTypeTags[index]
                                //                     .isSelected =
                                //                 !mProvide!.memberTypeTags[index]
                                //                     .isSelected;
                                //           });
                                //         },
                                //         onLongPressed: (item) => print(item),
                                //       );
                                //     },
                                //   ),
                                // spacerView(context, h: 64.sp),
                                // if (mProvide!.allergyTags.isNotEmpty)
                                //   Text(
                                //     "Do you have any allergies?",
                                //     style: Style_18_Bold_Black,
                                //   ),
                                // if (mProvide!.allergyTags.isNotEmpty)
                                //   spacerView(context, h: 24.sp),
                                // if (mProvide!.allergyTags.isNotEmpty)
                                //   Tags(
                                //     key: _tagAllergiesStateKey,
                                //     columns: 3,
                                //     alignment: WrapAlignment.spaceBetween,
                                //     itemCount: mProvide!.allergyTags.length,
                                //     itemBuilder: (int index) {
                                //       return ItemTags(
                                //         key: Key(index.toString()),
                                //         alignment: MainAxisAlignment.center,
                                //         index: index,
                                //         title:
                                //             mProvide!.allergyTags[index].name,
                                //         textStyle: TextStyle(fontSize: 32.sp),
                                //         activeColor: color_primary,
                                //         active: mProvide!
                                //             .allergyTags[index].isSelected,
                                //         borderRadius: BorderRadius.all(
                                //             Radius.circular(16.sp)),
                                //         border: Border.all(
                                //             color: mProvide!.allergyTags[index]
                                //                     .isSelected
                                //                 ? color_primary
                                //                 : black03),
                                //         padding: EdgeInsets.only(
                                //             left: 22.sp,
                                //             right: 22.sp,
                                //             top: 20.sp,
                                //             bottom: 20.sp),
                                //         textColor: color_black,
                                //         textActiveColor: color_white,
                                //         combine: ItemTagsCombine.withTextBefore,
                                //         onPressed: (item) {
                                //           setState(() {
                                //             mProvide!.allergyTags[index]
                                //                     .isSelected =
                                //                 !mProvide!.allergyTags[index]
                                //                     .isSelected;
                                //           });
                                //         },
                                //         onLongPressed: (item) => print(item),
                                //       );
                                //     },
                                //   ),

                                spacerView(context, h: 64.sp),
                                Divider(),
                                spacerView(context, h: 64.sp),
                                Row(
                                  children: [
                                    getButtonPadding(context, btnLblCancel.tr,
                                        ACTION_MAYBE_LATER, this,
                                        isFilled: true,
                                        color: greyF3,
                                        textStyle: Style_18_Bold_Black,
                                        padding: EdgeInsets.only(
                                            left: 48.sp,
                                            right: 48.sp,
                                            top: 32.sp,
                                            bottom: 32.sp)),
                                    spacerView(context, w: 20.sp),
                                    Expanded(
                                      child: widget.isEdit!
                                          ? getButtonPadding(
                                              context,
                                              stringUpdateMember.tr,
                                              ACTION_CONFIRM_UPDATE_MEMBER,
                                              this,
                                              isFilled: true,
                                              padding: EdgeInsets.only(
                                                  // left: 32.sp,
                                                  // right: 32.sp,
                                                  top: 32.sp,
                                                  bottom: 32.sp),
                                            )
                                          : getButtonPadding(
                                              context,
                                              stringConfirmMember.tr,
                                              ACTION_CONFIRM_ADD_MEMBER,
                                              this,
                                              isFilled: true,
                                              padding: EdgeInsets.only(
                                                  // left: 32.sp,
                                                  // right: 32.sp,
                                                  top: 32.sp,
                                                  bottom: 32.sp),
                                            ),
                                    ),
                                  ],
                                ),
                                spacerView(context, h: 64.sp),
                              ],
                            ),
                          ),
                        ),
                        Consumer<EditHomeMemViewModel>(
                            builder: (context, value, child) {
                          return getWidgetScreenLoader(context, value.loading);
                        }),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTagsViewList() {
    return ListView.separated(
      padding: EdgeInsets.zero,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return _buildTagTile(index);
      },
      separatorBuilder: (context, index) {
        return spacerView(context, h: 64.sp);
      },
      itemCount: mProvide?.memberTagsType?.tagtypes?.length ?? 0,
    );
  }

  Widget _buildTagTile(i) {
    final GlobalKey<TagsState> _stateKey = GlobalKey<TagsState>();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          mProvide?.memberTagsType?.tagtypes?[i].name ?? "",
          style: Style_18_Bold_Black,
        ),
        spacerView(context, h: 24.sp),
        Tags(
          key: _stateKey,
          columns: 3,
          alignment: WrapAlignment.spaceBetween,
          itemCount:
              mProvide?.memberTagsType?.tagtypes?[i].tagsData?.data?.length ??
                  0,
          itemBuilder: (int index) {
            return ItemTags(
              key: Key(index.toString()),
              alignment: MainAxisAlignment.center,
              index: index,
              title: mProvide
                  ?.memberTagsType?.tagtypes?[i].tagsData?.data?[index].name,
              textStyle: TextStyle(fontSize: 32.sp),
              activeColor: color_primary,
              active: mProvide?.memberTagsType?.tagtypes?[i].tagsData
                  ?.data?[index].isSelected,
              borderRadius: BorderRadius.all(Radius.circular(16.sp)),
              border: Border.all(
                  color: (mProvide?.memberTagsType?.tagtypes?[i].tagsData
                              ?.data?[index].isSelected ??
                          false)
                      ? color_primary
                      : black03),
              padding: EdgeInsets.only(
                  left: 22.sp, right: 22.sp, top: 20.sp, bottom: 20.sp),
              textColor: color_black,
              textActiveColor: color_white,
              combine: ItemTagsCombine.withTextBefore,
              onPressed: (item) {
                setState(() {
                  mProvide?.memberTagsType?.tagtypes?[i].tagsData?.data?[index]
                      .isSelected = !(mProvide?.memberTagsType?.tagtypes?[i]
                          .tagsData?.data?[index].isSelected ??
                      false);
                });
              },
              onLongPressed: (item) => print(item),
            );
          },
        ),
      ],
    );
  }

  void showPickerSheet(context, int type) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text(stringGallery.tr),
                      onTap: () {
                        _imgFromGallery(type);
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text(stringCamera.tr),
                    onTap: () {
                      imgFromCamera(type);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  imgFromCamera(int type) async {
    List<Media>? res = await ImagesPicker.openCamera(
      pickType: Platform.isAndroid
          ? PickType.image
          : (type == 0)
              ? PickType.image
              : PickType.video,
      maxTime: 9000,
    );
    if (res != null) {
      _imageAndVideoCropper(res);
    }
  }

  _imgFromGallery(int type) async {
    List<Media>? res = await ImagesPicker.pick(
      count: 1,
      pickType: (type == 0) ? PickType.image : PickType.video,
      language: Language.System,
      maxSize: 9000,
    );
    if (res != null) {
      _imageAndVideoCropper(res);
    }
  }

  Future<File?> showCropImageDialog(BuildContext context, Media res,
      double? aspectRatio, bool? showAspectRatioOptions) async {
    File? sendingFile;
    await showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black,
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return CropImageScreen((File? file) {
          Navigator.of(context, rootNavigator: true).pop();
          res.path = file!.path;
          sendingFile = file;
        }, File(res.path), aspectRatio, showAspectRatioOptions);
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 0), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
    return sendingFile;
  }

  _imageAndVideoCropper(List<Media> res) async {
    for (int i = 0; i < res.length; i++) {
      String mimeStr = lookupMimeType(res[i].path)!;
      var fileType = mimeStr.split('/');
      if ((fileType[0] == "image")) {
        File? croppedFile =
            await showCropImageDialog(context, res[i], 1 / 1, false);
        if (croppedFile != null) {
          res[i].path = croppedFile.path;
        } else {
          res[i].path = "";
        }
      } else {
        //In-Case Video
      }
    }

    // ignore: unnecessary_null_comparison
    if (res != null) {
      handleMediaResult(res);
    }
  }

  void handleMediaResult(List<Media> res) {
    print(res.map((e) => e.path).toList());
    for (int i = 0; i < res.length; i++) {
      if (res[i].path != "") {
        String mimeStr = lookupMimeType(res[i].path)!;
        var fileType = mimeStr.split('/');
        print('file type $fileType');
        media.clear();
        media.add(MyMedia(
            mediaPath: res[i].path,
            mediaType: (fileType[0] == "image") ? 0 : 1,
            thumbnail: res[i].thumbPath));
      }
    }

    print(media);
    finalApiCall();
  }

  void finalApiCall() {
    //List tier_list = []; //[mProvide.response.data.subscriptionTier[_checkboxValue].id];

    for (int i = 0; i < media.length; i++) {
      print("mediaPath= " + media[i].mediaPath!);
      File file = File(media[i].mediaPath!);
      imageFile = file;
      String fileName = file.path.split('/').last;
      print("filePath= " + file.path);
      print("fileName= " + fileName);
      if (media[i].mediaType == 0) {
        mediaImages
            .add(dio.MultipartFile.fromFileSync(file.path, filename: fileName));
      }
      if (media[i].mediaType == 1) {
        mediaVideos
            .add(dio.MultipartFile.fromFileSync(file.path, filename: fileName));
        mediaThumbnail.add(dio.MultipartFile.fromFileSync(media[i].thumbnail!,
            filename: fileName));
      }
    }
    setState(() {});
  }
}
