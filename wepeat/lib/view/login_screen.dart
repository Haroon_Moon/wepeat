import 'dart:async';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/navigation/routes.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/view/register_screen.dart';

import '../res/images.dart';
import '../res/widgets.dart';
import '../viewmodel/login_viewmodel.dart';
import 'base.dart';

class LoginScreen extends PageProvideNode<LoginViewModel> {
  @override
  Widget buildContent(BuildContext context) {
    return _LoginContentScreen(mProvider);
  }
}

/// View
class _LoginContentScreen extends StatefulWidget {
  final LoginViewModel provide;

  const _LoginContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _LoginContentState();
  }
}

class _LoginContentState extends State<_LoginContentScreen>
    with TickerProviderStateMixin<_LoginContentScreen>
    implements Presenter {
  LoginViewModel? mProvide;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  GoogleSignInAuthentication? googleKey;

  TextEditingController etEmail = TextEditingController();
  bool isErrorEmail = false;
  String? strErrorEmail = "";

  TextEditingController etPassword = TextEditingController();
  bool isErrorPassword = false;
  String? strErrorPassword = "";
  bool isPasswordVisible = false;

  bool canLogin = true;

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    setListeners();
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    mProvide!.isApiResponseError.addListener(() {
      if (mProvide!.isApiResponseError.value) {
        // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
        setState(() {
          if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
            isErrorEmail = true;
            strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
          }
          if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
            isErrorPassword = true;
            strErrorPassword =
                mProvide!.loginErrorsResponse.errors!.password![0];
          }
        });
        mProvide!.isApiResponseError.value = false;
      }
    });
  }

  void checkValidations() {
    setState(() {
      if (etEmail.text.isNotEmpty &&
          checkValidateEmail(etEmail.text.toString().trim()) &&
          isErrorEmail) isErrorEmail = false;
      if (etPassword.text.isNotEmpty && isErrorPassword) {
        isErrorPassword = false;
      }
      if (etEmail.text.toString().trim().isEmpty) {
        isErrorEmail = true;
        strErrorEmail = string_error_email.tr;
      } else if (!checkValidateEmail(etEmail.text.toString().trim())) {
        isErrorEmail = true;
        strErrorEmail = string_error_email_invalid.tr;
      } else if (etPassword.text.toString().trim().isEmpty) {
        isErrorPassword = true;
        strErrorPassword = string_error_password.tr;
      } else {
        mProvide!.login(etEmail.text.toString().trim(),
            etPassword.text.toString().trim(), "Android", this);
      }
    });
  }

  @override
  void onClick(String? action) {
    print("onClick: " + action!);
    switch (action) {
      case ACTION_PASSWORD_VISIBILITY:
        setState(() {
          isPasswordVisible = !isPasswordVisible;
        });
        break;
      case ACTION_ON_GOOGLE:
        callGoogleLoginApi();
        break;
      case ACTION_ON_FACEBOOK:
        callFBLoginApi();
        break;
      case ACTION_ON_EMAIL_CHANGE:
        setState(() {
          isErrorEmail = false;
        });
        break;
      case ACTION_ON_PASSWORD_CHANGE:
        setState(() {
          isErrorPassword = false;
        });
        break;
      case ACTION_ON_REFRESH:
        setState(() {});
        break;
      case ACTION_LOG_IN:
        checkValidations();
        break;
      case ACTION_ON_SUCCESS:
        GeneralUtils.navigationPushReplacement(context, Routes.planWidget,
            routeName: Routes.plan);

        break;
      case ACTION_REGISTER:
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => RegisterScreen()));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build Login Screen --------");
    spInit(context);

    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: color_primary,
        body: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Stack(
            children: [
              getWidgetBackgroundLayer(context),
              SingleChildScrollView(
                  child: Center(
                child: Column(
                  children: [
                    SizedBox(height: 260.sp),
                    widgetLogo(),
                    SizedBox(height: 90.sp),
                    getWidgetTextField(etEmail, string_your_email.tr,
                        string_enter_email_acc.tr, isErrorEmail, strErrorEmail,
                        presenter: this,
                        action: ACTION_ON_EMAIL_CHANGE,
                        isMarginTop: false),
                    getWidgetTextField(
                        etPassword,
                        string_your_password.tr,
                        string_enter_password.tr,
                        isErrorPassword,
                        strErrorPassword,
                        presenter: this,
                        action: ACTION_ON_PASSWORD_CHANGE,
                        isPasswordField: true,
                        passwordVisible: isPasswordVisible),
                    Container(
                      margin: EdgeInsets.only(
                          left: 60.sp, right: 60.sp, top: 60.sp, bottom: 26.sp),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                              child: Container(
                            height: 1.sp,
                            color: color_white,
                          )),
                          Padding(
                            padding: EdgeInsets.only(left: 20.sp, right: 20.sp),
                            child: Text(
                              string_also_connect_with.tr,
                              style: Style_18_Reg_White,
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Expanded(
                              child: Container(
                            height: 1.sp,
                            color: color_white,
                          )),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            onClick(ACTION_ON_GOOGLE);
                          },
                          child: SvgPicture.asset(
                            image_google,
                            height: 80.sp,
                            width: 80.sp,
                          ),
                        ),
                        SizedBox(
                          width: 30.sp,
                        ),
                        InkWell(
                          onTap: () {
                            onClick(ACTION_ON_FACEBOOK);
                          },
                          child: SvgPicture.asset(
                            image_facebook,
                            height: 80.sp,
                            width: 80.sp,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 30.sp,
                    ),
                    // Consumer<LoginViewModel>(builder: (context, value, child) {
                    //       return getWidgetButton(
                    //           context,
                    //           string_login_to_acc,
                    //           ACTION_LOG_IN,
                    //           this,
                    //           value.btnWidth,
                    //           value.loading,
                    //           isEnable: canLogin);
                    //     }),
                    getWidgetButton(
                        context,
                        string_login_to_acc.tr,
                        ACTION_LOG_IN,
                        this,
                        mProvide!.btnWidth,
                        mProvide!.loading,
                        isEnable: canLogin),
                    SizedBox(
                      height: 20.sp,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(string_dont_have_account.tr,
                            style: Style_16_Reg_White),
                        SizedBox(width: 5.sp),
                        getWidgetFlatButton(
                          this,
                          ACTION_REGISTER,
                          Text(string_create_now.tr,
                              style: Style_16_Bold_White),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 40.sp,
                    ),
                  ],
                ),
              )),
              // getWidgetBackgroundLayer(context)
            ],
          ),
        ));
  }

  Future<void> callFBLoginApi() async {
    String? token;
    try {
      token = await facebookLogin(context);
    } catch (error) {
      print(error);
    }
    if (token != null && token.isNotEmpty) {
      mProvide?.socialLogin(token, TYPE_FACEBOOK, this);
    }
    // else
    //   showToastDialog(context, string_something_wrong_msg);
  }

  Future<void> callGoogleLoginApi() async {
    print("Google Start");
    // _googleSignIn.signOut();
    // bool isSignedIn = await _googleSignIn.isSignedIn();
    // if (isSignedIn) {
    //   print("Already logged in");
    // } else {
    //   try {
    //     GoogleSignInAccount? account = await _googleSignIn.signIn();
    //     // googleKey = await account?.authentication;
    //     // print("AccessToken: ${googleKey?.accessToken}");
    //     // print("idToken: ${googleKey?.idToken}");
    //     // print("serverAuthCode: ${googleKey?.serverAuthCode}");
    //     print(account);
    //     final names = account?.displayName?.split(' ');
    //     if (account?.id != null) {
    //       mProvide?.socialLogin(account?.id ?? "", TYPE_GOOGLE, this);
    //     } else {
    //       // showToastDialog(context, string_something_wrong_msg);
    //     }
    //   } catch (error) {
    //     print(error);
    //   }
    // }

    try {
      String? token = await googleLogin(context);
      print(token);
      // mProvide?.socialLogin(token ?? "", TYPE_GOOGLE, this);
    } catch (error) {
      print(error);
    }
  }
}
