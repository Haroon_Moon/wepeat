import 'dart:io';

import 'package:dio/dio.dart' as dio;
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:mime/mime.dart';
import 'package:provider/provider.dart';
import 'package:wepeat/di/app_module.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/helper/crop_image_screen.dart';
import 'package:wepeat/helper/device_utils.dart';
import 'package:wepeat/helper/dialog.dart';
import 'package:wepeat/helper/navigation/general_utils.dart';
import 'package:wepeat/helper/net_utils.dart';
import 'package:flutter/material.dart';
import 'package:wepeat/model/my_media_model.dart';
import 'package:wepeat/res/colors.dart';
import 'package:wepeat/res/images.dart';
import 'package:wepeat/res/strings.dart';
import 'package:wepeat/res/styles.dart';
import 'package:wepeat/res/widgets.dart';
import 'package:wepeat/view/base.dart';
import 'package:wepeat/viewmodel/user_viewmodel.dart';
import 'package:wepeat/widgets/bottom_nav_widget.dart';
import 'package:wepeat/widgets/network_image_widget.dart';

import '../../helper/navigation/routes.dart';
import '../../viewmodel/my_data_viewmodel.dart';

class MyDataScreen extends PageProvideNode<MyDataViewModel> {
  @override
  Widget buildContent(BuildContext context) {
    return _MyDataContentScreen(mProvider);
  }
}

/// View
class _MyDataContentScreen extends StatefulWidget {
  final MyDataViewModel provide;

  const _MyDataContentScreen(this.provide);

  @override
  State<StatefulWidget> createState() {
    return _MyDataContentState();
  }
}

class _MyDataContentState extends State<_MyDataContentScreen>
    with TickerProviderStateMixin<_MyDataContentScreen>
    implements Presenter {
  MyDataViewModel? mProvide;
  TextEditingController etName = TextEditingController();
  TextEditingController etEmail = TextEditingController();
  TextEditingController etInsta = TextEditingController();
  TextEditingController etFb = TextEditingController();
  TextEditingController etTwitter = TextEditingController();
  TextEditingController etPinterest = TextEditingController();
  TextEditingController etYt = TextEditingController();
  TextEditingController etPassword = TextEditingController();
  TextEditingController etConfirmPassword = TextEditingController();
  List<MyMedia> media = [];
  List mediaImages = [];
  List mediaVideos = [];
  List mediaThumbnail = [];
  var imageFile;

  @override
  void initState() {
    super.initState();
    mProvide = widget.provide;
    setListeners();
    etName.text = spUtil.user.name ?? "";
    etEmail.text = spUtil.user.email ?? "";

    etInsta.text = spUtil.user.customerData?.instagramURL ?? "";
    etFb.text = spUtil.user.customerData?.facebookURL ?? "";
    etTwitter.text = spUtil.user.customerData?.twitterURL ?? "";
    etPinterest.text = spUtil.user.customerData?.pinterestURL ?? "";
    etYt.text = spUtil.user.customerData?.youtubeURL ?? "";
  }

  @override
  void dispose() {
    print('-------dispose-------');
    super.dispose();
  }

  void setListeners() {
    mProvide!.isInternetError.addListener(() {
      if (mProvide!.isInternetError.value) {
        showInternetAlert(context);
        mProvide!.isInternetError.value = false;
      }
    });

    // mProvide!.isApiResponseError.addListener(() {
    //   if (mProvide!.isApiResponseError.value) {
    //     // onErrorHandler(context, mProvide.baseResponseModel.code, mProvide.baseResponseModel.message);
    //     setState(() {
    //       if (mProvide!.loginErrorsResponse.errors!.email!.isNotEmpty) {
    //         isErrorEmail = true;
    //         strErrorEmail = mProvide!.loginErrorsResponse.errors!.email![0];
    //       }
    //       if (mProvide!.loginErrorsResponse.errors!.password!.isNotEmpty) {
    //         isErrorPassword = true;
    //         strErrorPassword =
    //             mProvide!.loginErrorsResponse.errors!.password![0];
    //       }
    //     });
    //     mProvide!.isApiResponseError.value = false;
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    print("-------- Build My Data Screen --------");
    spInit(context);

    return Scaffold(
      backgroundColor: color_white,
      bottomNavigationBar: const BottomNavWidget(),
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Stack(
            children: [
              _buildBody(),
              Consumer<MyDataViewModel>(builder: (context, value, child) {
                return getWidgetScreenLoader(context, value.loading);
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(40.sp),
      child: Column(
        children: [
          Row(
            children: [
              InkWell(
                onTap: () {
                  GeneralUtils.navigationPop(context);
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                ),
              ),
              spacerView(context, w: 32.sp),
              Text(
                stringMyData.tr,
                style: Style_30_Bold_Black05,
              ),
            ],
          ),
          spacerView(context, h: 48.sp),
          _buildDetailView(),
          spacerView(context, h: 48.sp),
          _buildSocialView(),
          spacerView(context, h: 48.sp),
          _buildPasswordView(),
          spacerView(context, h: 48.sp),
          _buildBtnView(),
        ],
      ),
    );
  }

  Widget _buildDetailView() {
    return Container(
      padding: EdgeInsets.all(40.sp),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border.all(color: greyE5),
        borderRadius: BorderRadius.all(Radius.circular(20.sp)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                stringMyPhoto.tr,
                style: Style_18_Reg_Black03,
              ),
              spacerView(context, w: 16.sp),
              Container(
                padding: EdgeInsets.all(8.sp),
                decoration: BoxDecoration(
                  color: greyF3,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.sp),
                  ),
                ),
                child: Text(
                  stringOptional.tr,
                  style: Style_14_Bold_Black,
                ),
              ),
            ],
          ),
          spacerView(context, h: 30.sp),
          InkWell(
            onTap: () {
              showPickerSheet(context, 0);
            },
            child: NetworkImageWidget(
              url: (spUtil.user.iconUrl != null && spUtil.user.iconUrl != "")
                  ? spUtil.user.iconUrl
                  : imgPH,
              height: 180.sp,
              width: 180.sp,
              shape: BoxShape.circle,
            ),
          ),
          spacerView(context, h: 64.sp),
          Text(
            stringYourName.tr,
            style: Style_18_Bold_Black,
          ),
          spacerView(context, h: 24.sp),
          getTextField(etName, "", stringIntroduceName.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Text(
            stringYourEmail.tr,
            style: Style_18_Bold_Black,
          ),
          spacerView(context, h: 24.sp),
          getTextField(etEmail, "", stringYourEmail.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
        ],
      ),
    );
  }

  Widget _buildSocialView() {
    return Container(
      padding: EdgeInsets.all(40.sp),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border.all(color: greyE5),
        borderRadius: BorderRadius.all(Radius.circular(20.sp)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                stringInstagram.tr,
                style: Style_18_Bold_Black,
              ),
              spacerView(context, w: 16.sp),
              Container(
                padding: EdgeInsets.all(8.sp),
                decoration: BoxDecoration(
                  color: greyF3,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.sp),
                  ),
                ),
                child: Text(
                  stringOptional.tr,
                  style: Style_14_Bold_Black,
                ),
              ),
            ],
          ),
          spacerView(context, h: 24.sp),
          getTextField(etInsta, "", stringEnterInstagram.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Row(
            children: [
              Text(
                stringFacebook.tr,
                style: Style_18_Bold_Black,
              ),
              spacerView(context, w: 16.sp),
              Container(
                padding: EdgeInsets.all(8.sp),
                decoration: BoxDecoration(
                  color: greyF3,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.sp),
                  ),
                ),
                child: Text(
                  stringOptional.tr,
                  style: Style_14_Bold_Black,
                ),
              ),
            ],
          ),
          spacerView(context, h: 24.sp),
          getTextField(etFb, "", stringEnterFacebook.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Row(
            children: [
              Text(
                stringTwitter.tr,
                style: Style_18_Bold_Black,
              ),
              spacerView(context, w: 16.sp),
              Container(
                padding: EdgeInsets.all(8.sp),
                decoration: BoxDecoration(
                  color: greyF3,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.sp),
                  ),
                ),
                child: Text(
                  stringOptional.tr,
                  style: Style_14_Bold_Black,
                ),
              ),
            ],
          ),
          spacerView(context, h: 24.sp),
          getTextField(etTwitter, "", stringEnterTwitter.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Row(
            children: [
              Text(
                stringPinterest.tr,
                style: Style_18_Bold_Black,
              ),
              spacerView(context, w: 16.sp),
              Container(
                padding: EdgeInsets.all(8.sp),
                decoration: BoxDecoration(
                  color: greyF3,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.sp),
                  ),
                ),
                child: Text(
                  stringOptional.tr,
                  style: Style_14_Bold_Black,
                ),
              ),
            ],
          ),
          spacerView(context, h: 24.sp),
          getTextField(etPinterest, "", stringEnterPinterest.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Row(
            children: [
              Text(
                stringYoutube.tr,
                style: Style_18_Bold_Black,
              ),
              spacerView(context, w: 16.sp),
              Container(
                padding: EdgeInsets.all(8.sp),
                decoration: BoxDecoration(
                  color: greyF3,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.sp),
                  ),
                ),
                child: Text(
                  stringOptional.tr,
                  style: Style_14_Bold_Black,
                ),
              ),
            ],
          ),
          spacerView(context, h: 24.sp),
          getTextField(etYt, "", stringEnterYoutube.tr,
              presenter: this,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
        ],
      ),
    );
  }

  Widget _buildPasswordView() {
    return Container(
      padding: EdgeInsets.all(40.sp),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border.all(color: greyE5),
        borderRadius: BorderRadius.all(Radius.circular(20.sp)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                stringPassword.tr,
                style: Style_18_Bold_Black,
              ),
              spacerView(context, w: 16.sp),
              Container(
                padding: EdgeInsets.all(8.sp),
                decoration: BoxDecoration(
                  color: greyF3,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.sp),
                  ),
                ),
                child: Text(
                  stringOptional.tr,
                  style: Style_14_Bold_Black,
                ),
              ),
            ],
          ),
          spacerView(context, h: 24.sp),
          getTextField(etPassword, "", stringPassword.tr,
              presenter: this,
              isObscureText: true,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
          spacerView(context, h: 64.sp),
          Row(
            children: [
              Text(
                stringConfirmPassword.tr,
                style: Style_18_Bold_Black,
              ),
              spacerView(context, w: 16.sp),
              Container(
                padding: EdgeInsets.all(8.sp),
                decoration: BoxDecoration(
                  color: greyF3,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.sp),
                  ),
                ),
                child: Text(
                  stringOptional.tr,
                  style: Style_14_Bold_Black,
                ),
              ),
            ],
          ),
          spacerView(context, h: 24.sp),
          getTextField(etConfirmPassword, "", stringConfirmPassword.tr,
              presenter: this,
              isObscureText: true,
              action: ACTION_ON_EMAIL_CHANGE,
              isMarginTop: false),
        ],
      ),
    );
  }

  Widget _buildBtnView() {
    return Row(
      children: [
        getButtonPadding(
          context,
          btnLblCancel.tr,
          actionCancel,
          this,
          color: greyF3,
          textStyle: Style_14_Bold_Black,
          padding: EdgeInsets.only(
              top: 32.sp, bottom: 32.sp, left: 48.sp, right: 48.sp),
        ),
        spacerView(context, w: 20.sp),
        Expanded(
          child: getButtonPadding(
            context,
            btnLblApplyChanges.tr,
            actionSaveChanges,
            this,
            textStyle: Style_14_Bold_WHITE,
            padding: EdgeInsets.only(
              top: 32.sp,
              bottom: 32.sp,
            ),
          ),
        ),
      ],
    );
  }

  @override
  void onClick(String? action) {
    switch (action) {
      case actionCancel:
        GeneralUtils.navigationPop(context);
        break;
      case actionSaveChanges:
        //Tapped Apply changes button
        hideKeyboard(context);

        mProvide?.updateProfile(
          this,
          etName.text.trim(),
          etEmail.text.trim(),
          etInsta.text.trim(),
          etFb.text.trim(),
          etTwitter.text.trim(),
          etPinterest.text.trim(),
          etYt.text.trim(),
        );
        if (etPassword.text.trim().isNotEmpty &&
            etConfirmPassword.text.trim().isNotEmpty) {
          if (etEmail.text.trim().isNotEmpty) {
            mProvide?.updatePassword(this, etEmail.text.trim(),
                etPassword.text.trim(), etConfirmPassword.text.trim());
          } else {
            showToastDialog(context, "Email is required to update password");
          }
        }
        break;
      case actionSuccess:
        setState(() {});
        break;
      case actionUpdatedSuccess:
        setState(() {
          etPassword.clear();
          etConfirmPassword.clear();
        });
        showToastDialog(context, stringUpdated.tr);
        break;
    }
  }

  void showPickerSheet(context, int type) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text(stringGallery.tr),
                      onTap: () {
                        _imgFromGallery(type);
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text(stringCamera.tr),
                    onTap: () {
                      imgFromCamera(type);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  imgFromCamera(int type) async {
    List<Media>? res = await ImagesPicker.openCamera(
      pickType: Platform.isAndroid
          ? PickType.image
          : (type == 0)
              ? PickType.image
              : PickType.video,
      maxTime: 9000,
    );
    if (res != null) {
      _imageAndVideoCropper(res);
    }
  }

  _imgFromGallery(int type) async {
    List<Media>? res = await ImagesPicker.pick(
      count: 1,
      pickType: (type == 0) ? PickType.image : PickType.video,
      language: Language.System,
      maxSize: 9000,
    );
    if (res != null) {
      _imageAndVideoCropper(res);
    }
  }

  Future<File?> showCropImageDialog(BuildContext context, Media res,
      double? aspectRatio, bool? showAspectRatioOptions) async {
    File? sendingFile;
    await showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black,
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return CropImageScreen((File? file) {
          Navigator.of(context, rootNavigator: true).pop();
          res.path = file!.path;
          sendingFile = file;
        }, File(res.path), aspectRatio, showAspectRatioOptions);
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 0), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
    return sendingFile;
  }

  _imageAndVideoCropper(List<Media> res) async {
    for (int i = 0; i < res.length; i++) {
      String mimeStr = lookupMimeType(res[i].path)!;
      var fileType = mimeStr.split('/');
      if ((fileType[0] == "image")) {
        File? croppedFile =
            await showCropImageDialog(context, res[i], 1 / 1, false);
        if (croppedFile != null) {
          res[i].path = croppedFile.path;
        } else {
          res[i].path = "";
        }
      } else {
        //In-Case Video
      }
    }

    // ignore: unnecessary_null_comparison
    if (res != null) {
      handleMediaResult(res);
    }
  }

  void handleMediaResult(List<Media> res) {
    print(res.map((e) => e.path).toList());
    for (int i = 0; i < res.length; i++) {
      if (res[i].path != "") {
        String mimeStr = lookupMimeType(res[i].path)!;
        var fileType = mimeStr.split('/');
        print('file type $fileType');
        media.clear();
        media.add(MyMedia(
            mediaPath: res[i].path,
            mediaType: (fileType[0] == "image") ? 0 : 1,
            thumbnail: res[i].thumbPath));
      }
    }

    print(media);
    finalApiCall();
  }

  void finalApiCall() {
    //List tier_list = []; //[mProvide.response.data.subscriptionTier[_checkboxValue].id];

    for (int i = 0; i < media.length; i++) {
      print("mediaPath= " + media[i].mediaPath!);
      File file = File(media[i].mediaPath!);
      imageFile = file;
      String fileName = file.path.split('/').last;
      print("filePath= " + file.path);
      print("fileName= " + fileName);
      if (media[i].mediaType == 0) {
        mediaImages
            .add(dio.MultipartFile.fromFileSync(file.path, filename: fileName));
      }
      if (media[i].mediaType == 1) {
        mediaVideos
            .add(dio.MultipartFile.fromFileSync(file.path, filename: fileName));
        mediaThumbnail.add(dio.MultipartFile.fromFileSync(media[i].thumbnail!,
            filename: fileName));
      }
    }
    setState(() {
      if (mediaImages.isNotEmpty) {
        mProvide?.updateUserImage(this, context, mediaImages);
      }
    });
  }
}
