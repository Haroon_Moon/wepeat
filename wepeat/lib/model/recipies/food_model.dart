class FoodModel {
  int? foodId;
  int? measureId;
  String? quantity;
  FoodData? foodData;
  MeasuresData? measureData;

  FoodModel({this.foodId, this.measureId, this.quantity});

  FoodModel.fromJson(Map<String, dynamic> json) {
    foodId = json['food_id'];
    measureId = json['measure_id'];
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['food_id'] = foodId;
    data['measure_id'] = measureId;
    data['quantity'] = quantity;
    return data;
  }
}

class FoodData {
  Data? data;

  FoodData({this.data});

  FoodData.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? id;
  String? key;
  String? name;
  String? description;
  List<int>? tags;
  List<Composition>? composition;
  List<Measures>? measures;

  Data(
      {this.id,
      this.key,
      this.name,
      this.description,
      this.tags,
      this.composition,
      this.measures});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    key = json['key'];
    name = json['name'];
    description = json['description'];
    tags = json['tags'].cast<int>();
    if (json['composition'] != null) {
      composition = <Composition>[];
      json['composition'].forEach((v) {
        composition!.add(Composition.fromJson(v));
      });
    }
    if (json['measures'] != null) {
      measures = <Measures>[];
      json['measures'].forEach((v) {
        measures!.add(Measures.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['key'] = key;
    data['name'] = name;
    data['description'] = description;
    data['tags'] = tags;
    if (composition != null) {
      data['composition'] = composition!.map((v) => v.toJson()).toList();
    }
    if (measures != null) {
      data['measures'] = measures!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Composition {
  int? componentId;
  dynamic? value;

  Composition({this.componentId, this.value});

  Composition.fromJson(Map<String, dynamic> json) {
    componentId = json['component_id'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['component_id'] = componentId;
    data['value'] = value;
    return data;
  }
}

class Measures {
  int? measureId;
  String? value;
  bool? isDefault;

  Measures({this.measureId, this.value, this.isDefault});

  Measures.fromJson(Map<String, dynamic> json) {
    measureId = json['measure_id'];
    value = json['value'];
    isDefault = json['is_default'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['measure_id'] = measureId;
    data['value'] = value;
    data['is_default'] = isDefault;
    return data;
  }
}

class MeasuresData {
  int? id;
  String? key;
  String? name;
  String? description;

  MeasuresData({this.id, this.key, this.name, this.description});

  MeasuresData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    key = json['key'];
    name = json['name'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['key'] = this.key;
    data['name'] = this.name;
    data['description'] = this.description;
    return data;
  }
}
