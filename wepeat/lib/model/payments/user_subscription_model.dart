class UserSubscriptionModel {
  int? planId;
  String? name;
  String? status;
  bool? active;
  bool? onGracePeriod;
  bool? onTrial;
  bool? cancelled;
  String? stripeStatus;
  String? stripeId;
  String? stripePlan;
  String? stripeExtraData;
  int? quantity;
  String? endsAt;
  String? trialEndsAt;
  String? createdAt;
  String? updatedAt;

  UserSubscriptionModel(
      {this.planId,
      this.name,
      this.status,
      this.active,
      this.onGracePeriod,
      this.onTrial,
      this.cancelled,
      this.stripeStatus,
      this.stripeId,
      this.stripePlan,
      this.stripeExtraData,
      this.quantity,
      this.endsAt,
      this.trialEndsAt,
      this.createdAt,
      this.updatedAt});

  UserSubscriptionModel.fromJson(Map<String, dynamic> json) {
    planId = json['plan_id'];
    name = json['name'];
    status = json['status'];
    active = json['active'];
    onGracePeriod = json['on_grace_period'];
    onTrial = json['on_trial'];
    cancelled = json['cancelled'];
    stripeStatus = json['stripe_status'];
    stripeId = json['stripe_id'];
    stripePlan = json['stripe_plan'];
    stripeExtraData = json['stripe_extra_data'];
    quantity = json['quantity'];
    endsAt = json['ends_at'];
    trialEndsAt = json['trial_ends_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['plan_id'] = planId;
    data['name'] = name;
    data['status'] = status;
    data['active'] = active;
    data['on_grace_period'] = onGracePeriod;
    data['on_trial'] = onTrial;
    data['cancelled'] = cancelled;
    data['stripe_status'] = stripeStatus;
    data['stripe_id'] = stripeId;
    data['stripe_plan'] = stripePlan;
    data['stripe_extra_data'] = stripeExtraData;
    data['quantity'] = quantity;
    data['ends_at'] = endsAt;
    data['trial_ends_at'] = trialEndsAt;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
