import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wepeat/helper/constants.dart';
import 'package:wepeat/model/calendar_items_model.dart';
import 'package:wepeat/model/meal_types_model.dart';

import '../di/repository.dart';
import '../model/recipes_model.dart';
import '../model/response/error_logins/LoginErrorsResponse.dart';
import '../model/user_calendar_model.dart';
import '../view/base.dart';

class PlanViewModel extends BaseViewModel {
  final GithubRepo _repo;
  bool _loading = false;
  bool isApiCalled = false;
  LoginErrorsResponse loginErrorsResponse = LoginErrorsResponse();

  MealTypesModel? mealTypesRes;
  UserCalendarModel? calendarRes;
  CalendarItemsModel? calendarItmesRes;

  List<CalendarItemsModel>? filtredCalendarItems = [];

  PlanViewModel(this._repo);

  bool get loading => _loading;

  set loading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  Future<Stream?> getMealTypes(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getMealTypes().doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);
        mealTypesRes = MealTypesModel.fromJson(r);
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getCalendar(Presenter presenter) async {
    if (await checkInternet()) {
      _repo.getCalendar().doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);
        calendarRes = UserCalendarModel.fromJson(r);
        print(calendarRes);
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> updateCalendar(
      Presenter presenter, String title, String desc) async {
    if (await checkInternet()) {
      _repo.updateCalendar(title, desc).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);
        UserCalendarModel updatedData = UserCalendarModel.fromJson(r);
        calendarRes?.calendarData = updatedData.calendarData;
        presenter.onClick(ACTION_ON_SUCCESS);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> getCalendarItems(
      Presenter presenter, startDate, endDate, int days) async {
    if (await checkInternet()) {
      isApiCalled = false;
      _repo.getCalendarIems(startDate, endDate).doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);
        filtredCalendarItems?.clear();

        calendarItmesRes = CalendarItemsModel.fromJson(r);
        calendarItmesRes?.items?.forEach((item) {
          mealTypesRes?.type?.forEach((element) {
            if (item.mealTypeId == element.id) {
              item.mealTypeName = element.key;
            }
          });

          bool isDateMatched = false;

          filtredCalendarItems?.forEach((element) {
            if (item.date == element.data) {
              isDateMatched = true;
              element.items?.add(item);
            }
          });

          if (!isDateMatched) {
            filtredCalendarItems?.add(
              CalendarItemsModel(data: item.date, items: [item]),
            );
          }
        });

        if (filtredCalendarItems?.length != days) {
          for (int i = 0; i <= days; i++) {
            var sDate = DateTime.parse(startDate);
            var newDate = sDate.add(Duration(days: i));
            String newDateSting = DateFormat('yyyy-MM-dd').format(newDate);
            print(newDateSting);
            if (filtredCalendarItems!
                .any((element) => element.data == newDateSting)) {
              //Already exist
            } else {
              filtredCalendarItems?.add(
                CalendarItemsModel(data: newDateSting, items: [CalendarItem()]),
              );
            }
          }

          filtredCalendarItems?.sort((a, b) => a.data!.compareTo(b.data ?? ""));
        }

        print(calendarItmesRes);

        isApiCalled = true;
        presenter.onClick(actionSuccess);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> addNote(Presenter presenter, String date, int mealTypeId,
      String link, String content) async {
    if (await checkInternet()) {
      _repo.addNote(date, mealTypeId, content, link, "note").doOnListen(() {
        loading = true;
        presenter.onClick(ACTION_ON_REFRESH);
      }).doOnDone(() {
        loading = false;
        presenter.onClick(ACTION_ON_REFRESH);
      }).listen((r) {
        //success
        print(r);
        presenter.onClick(actionSuccessAddNote);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
    return null;
  }

  Future<Stream?> delItem(Presenter presenter, itemId) async {
    loading = true;
    if (await checkInternet()) {
      _repo.delCalendarItem(itemId).doOnListen(() {
        loading = true;
      }).doOnDone(() {
        loading = false;
      }).listen((r) {
        //success
        print(r);
        presenter.onClick(actionSuccessDelItem);
      }, onError: (e) {
        //error
        if (e is DioError) {
          if (e.response?.statusCode == 422) {
            final body = json.decode(e.response.toString());
            print("ErrorResponse: " + body["message"]);
            loginErrorsResponse = LoginErrorsResponse.fromJson(body);
            onApiResponseError();
          }
        }
      });
    }
  }
}
